//
//  AppDelegate.swift
//  qlip
//
//  Created by Amelia Lim on 22/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import PromiseKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FirebaseDynamicLinks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
// swiftlint:disable function_body_length
    func handleNotification(userInfo: [AnyHashable: Any]) {
        NotificationCenter.default.post(
            name: .appPushNotificationPayload,
            object: nil,
            userInfo: userInfo)
    }
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
    ) {
        print("testing",userInfo)
        handleNotification(userInfo: userInfo)
        completionHandler(.newData)
    }
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        // Promise Kit
        let queue = DispatchQueue(
            label: "QLIP_QUEUE",
            qos: .userInitiated,
            attributes: .concurrent,
            autoreleaseFrequency: .workItem
        )
        PromiseKit.conf.catchPolicy = .allErrors
        PromiseKit.conf.Q.map = queue
        PromiseKit.conf.Q.return = .main
        // API Service
        let enableAPILog = Log.logTags.filter({ $0.tag == .api }).first?.enabled ?? true
        NetworkService.enableLog = enableAPILog
        APIService.enableLog = enableAPILog
        APIService.logTag =
            Log.logTags.filter({ $0.tag == .api }).first?.tag.rawValue ?? ""
        APIService.baseURL = APISetting.baseUrl
        // Request token if needed
        let token = TokenModel.Storage.getToken()?.accessToken ?? ""
        if token.isEmpty {
            if !AppHelper.isRequestingToken {
                AppHelper.isRequestingToken = true
                Log.debug("Requesting token is forcing:", tag: .token)
                APIService.Authentication.requestToken()
                    .done { data in
                        if let tokenData = data.dataModel() {
                            TokenModel.Storage.delete()
                            TokenModel.Storage.save(tokenModel: tokenData)
                            NotificationCenter.default.post(
                                name: .appTokenRefreshed,
                                object: nil, userInfo: nil
                            )
                            let token = TokenModel.Storage.getToken()?.accessToken ?? ""
                            Log.debug("Token requested with token \(token)", tag: .token)

                            // Setup windows

                            #if DEBUG
                            AppDemo.shared.show()
                            #endif

                        }
                }.ensure {
                    AppHelper.isRequestingToken = false
                }.catch { error in
                    Log.debug("Token request with error: \(error)", tag: .token)
                }
                self.setMainWindow()
            }
        } else {
            // Setup windows
            self.setMainWindow()

            #if DEBUG
            AppDemo.shared.show()
            #endif
        }
        application.registerForRemoteNotifications()
        // Firebase
        let enableFirebaseLog = Log.logTags.filter({ $0.tag == .firebase }).first?.enabled ?? false
        Firebase.shared.enableLog = enableFirebaseLog
        Firebase.shared.logTag = Log.logTags.filter({ $0.tag == .firebase }).first?.tag.rawValue ?? ""
        Firebase.shared.topicName = "qlip"
        Firebase.shared.configureFirebase(application: application, launchOptions: launchOptions)

        // Subscribe firebase
        Firebase.shared.setFirebaseSubscription(isOn: true)

        @available(iOS 9.0, *)
        func application(_ application: UIApplication,
                         open url: URL,
                         options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {

            return true
        }

        // Google Sign in
        GIDSignIn.sharedInstance().clientID =
        "411573315549-hjk6445lva18b7jtvq1pbs6doglacfpt.apps.googleusercontent.com"

        // facebook Sign in
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

        // Set audio always unmute after launch
        UserDefaults.standard.set(false, forKey: SoundDefaultKey.soundState)
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
        // Do your task here
            let userInfo =  launchOptions?[.remoteNotification] as? [AnyHashable: Any]
            handleNotification(userInfo: userInfo!)
        }
        // set first install date
        if Storage.EventTracking.getFirstInstallDate() == nil {
            setFirstInstallDate()
        }
        // set first install version
        if Storage.EventTracking.getFirstInstallVersion() == nil {
            setFirstInstallVersion()
        }
        return true
    }

    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink) {
        guard let url = dynamicLink.url else { return }
        print("handle dynamic links for this url : \(url)")
    }

    func application(
        _ application: UIApplication,
        continue userActivity: NSUserActivity,
        restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL {
            print("incoming URL is \(incomingURL)")
            let linkedHandled = DynamicLinks.dynamicLinks().handleUniversalLink(
            incomingURL
            ) { (dynamicLink, error) in
                guard error == nil else {
                    Log.debug("Found an Error ! \(String(describing: error?.localizedDescription))")
                    return
                }
                if let dynamicLink = dynamicLink {
                    self.handleIncomingDynamicLink(dynamicLink)
                }
            }
            if linkedHandled {
                return true
            } else {
                return false
            }
        }
        return false
    }

    // MARK: - Life Cycles

    open func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handleFacebook = ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        } else {
            let handleGoogle = GIDSignIn.sharedInstance().handle(url)
            return handleFacebook || handleGoogle
        }
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        Firebase.shared.setUserProperties(.currentVersion)
        Firebase.shared.setUserProperties(.kidAge)
    }

    // MARK: - Windows

    private func setMainWindow() {
        MultiLanguage.setLanguage(language: .idLang)
        Storage.SoundState.setSoundState(isActive: true)
        if ParentRealmModel.isLoggedIn() || KidRealmModel.isLoggedIn() {
            let navigation = UINavigationController(rootViewController: MainTabBarViewController())
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = navigation
            window?.makeKeyAndVisible()
        } else {
            let navigation = UINavigationController(rootViewController: CoreValuesViewController())
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = navigation
            window?.makeKeyAndVisible()
        }
    }

    // MARK: - Helpers
    private func setFirstInstallDate() {
        let urlToDocumentsFolder: URL? = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask
        ).last
        let installDate = try? FileManager.default.attributesOfItem(
            atPath: (urlToDocumentsFolder?.path)!)[.creationDate] as? Date
        Storage.EventTracking.saveFirstInstallDate(date: installDate)
    }
    private func setFirstInstallVersion() {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as? String
        if let version = version {
            Storage.EventTracking.saveFirstInstallVersion(version: version)
            Firebase.shared.setUserProperties(.installVersion)
        }
    }
}
