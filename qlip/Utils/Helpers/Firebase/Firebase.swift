//
//  Firebase.swift
//  qlip
//
//  Created by Dan Faerae on 17/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import Firebase
import Messages

final class Firebase: NSObject, UNUserNotificationCenterDelegate, MessagingDelegate {

    static let shared: Firebase = {
        let instance = Firebase()

        return instance
    }()

    var enableLog: Bool = true
    var logTag: String = "[Firebase]"
    var topicName: String = "firebase"

    // MARK: - Public Functions

    func configureFirebase(
        application: UIApplication,
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) {
        FirebaseApp.configure()

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS).
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            UNUserNotificationCenter.current().delegate = self
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

        Messaging.messaging().delegate = self
    }

    // MARK: - Private Functions

    func log(_ items: Any...) {
        if enableLog {
            print("\(logTag): \(items)")
        }
    }

    func handleNotification(userInfo: [AnyHashable: Any]) {
        guard let action = userInfo["action"] as? String else { return }

        if let url = URL(string: action) {
            DeeplinkManager.shared.handleDeeplinkFromApp(url: url)
        }
    }

    // MARK: Messaging Delegate

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let token = Messaging.messaging().fcmToken
        log("didReceiveRegistrationToken: \(token ?? "")")
        setFirebaseSubscription(isOn: true)
        AppHelper.updateFCMToken()
    }

    // Deprecated
    // func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        // log("didReceive didReceive remoteMessage userInfo: \(remoteMessage.appData)")
    // }

    // MARK: - User Notification Center Delegate

    @available(iOS 10.0, *)
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        let userInfo = response.notification.request.content.userInfo
        log("didReceive userInfo: \(userInfo)")
//        DeeplinkManager.shared.handleRemoteNotification(userInfo)

        // if the app is in foreground, execute the deeplink directly
        if UIApplication.shared.applicationState == .active {
//            DeeplinkManager.shared.checkDeepLink()
        }
        NotificationCenter.default.post(
            name: .appPushNotificationPayload,
            object: nil,
            userInfo: userInfo
        )
        completionHandler()
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) ->
        Void
    ) {
        let userInfo = notification.request.content.userInfo
        log("willPresent userInfo: \(userInfo)")
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        Messaging.messaging().appDidReceiveMessage(userInfo)
        Messaging.messaging().delegate = self

        // handleNotification(userInfo: userInfo)
        // silent notif
        NotificationCenter.default.post(
            name: .appPushNotificationPayload,
            object: nil,
            userInfo: userInfo
        )

        // Change this to your preferred presentation option
        completionHandler([.alert,.sound])
    }

    func setFirebaseSubscription(isOn: Bool) {
        let topic = "\(topicName)-all"
        let topicCustomer = "\(topicName)-customer-all"
        let topiciOS = "\(topicName)-customer-ios"
        if isOn {
            // firebase subscribe to topic
            Messaging.messaging().subscribe(toTopic: topic)
            Messaging.messaging().subscribe(toTopic: topicCustomer)
            Messaging.messaging().subscribe(toTopic: topiciOS)
        } else {
            // firebase unsubscribe to topic
            Messaging.messaging().unsubscribe(fromTopic: topic)
            Messaging.messaging().unsubscribe(fromTopic: topicCustomer)
            Messaging.messaging().unsubscribe(fromTopic: topiciOS)
         }
    }

    func getFCMToken() -> String? {
        return Messaging.messaging().fcmToken
    }

    func setEventTracking(_ eventname: EventName, params: [String: Any]) {
        let name = eventname.rawValue.camelCaseToSnakeCase()
        Analytics.logEvent(name, parameters: params)
    }

    func setUserProperties(_ name: UserPropertiesType, value: String? = nil) {
        let data = UserPropertiesManager.shared.setup(type: name, value: value)
        let forName = name.rawValue.camelCaseToSnakeCase()
        Analytics.setUserProperty(data, forName: forName)
    }
}
