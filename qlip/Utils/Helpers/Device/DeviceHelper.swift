//
//  DeviceHelper.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 12/08/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

final class DeviceHelper {
    static func isLowerThanIPhone5() -> Bool {
        // iPhones_5_5s_5c_SE
        return UIScreen.main.nativeBounds.height <= 1136
    }
}
