//
//  ChunkedArray.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 12/08/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
