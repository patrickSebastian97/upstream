//
//  NavigationHelper.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 03/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

/// Add all basic navigation functionality for easy maintenance and easy to be reuse
extension UIViewController {

    func closeAndPopAllControllers() {
        // try to pop to root view controller and dismiss
        navigationController?.popToRootViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }

    func gotoDummyHomeScreen() {
        closeAndPopAllControllers()

        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        window?.rootViewController = DummyViewController()
        window?.makeKeyAndVisible()
    }
    func gotoHomeScreen() {
        closeAndPopAllControllers()

        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        let navigation = UINavigationController(rootViewController: MainTabBarViewController())
        window?.rootViewController = navigation
        window?.makeKeyAndVisible()
    }

    func goToCoreValuesScreen() {
        closeAndPopAllControllers()

        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        let navigation = UINavigationController(rootViewController: CoreValuesViewController())
        window?.rootViewController = navigation
        window?.makeKeyAndVisible()
    }

}
