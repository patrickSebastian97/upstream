//
//  BaseNavigationHelper.swift
//  qlip
//
//  Created by Amelia Lim on 02/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

extension UIViewController {

    func presentOnNavigation(viewController: UIViewController) {
        let navigation = UINavigationController(rootViewController: viewController)
        present(navigation, animated: true, completion: nil)
    }

    func findViewControllerOfType<T: UIViewController>(controller: T.Type) -> T? {
        guard let viewControllers = navigationController?.viewControllers else {
            return nil
        }

        for viewController in viewControllers {
            if viewController.isKind(of: controller) {
                return viewController as? T
            }
        }

        return nil
    }

    func popToViewControllerOfType<T: UIViewController>(controller: T.Type, animated: Bool) {
        guard let viewControllers = navigationController?.viewControllers else {
            return
        }

        for viewController in viewControllers {
            if viewController.isKind(of: controller) {
                navigationController?.popToViewController(viewController, animated: animated)
            }
        }
    }

}
