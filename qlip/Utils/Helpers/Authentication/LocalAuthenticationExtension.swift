//
//  LocalAuthenticationExtension.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 24/09/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation
import LocalAuthentication

extension LAContext {
    enum BiometricType: String {
        case none
        case touchID
        case faceID
    }

    var biometricType: BiometricType {
        var error: NSError?

        guard self.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            // Capture these recoverable error thru Crashlytics
            return .none
        }

        if #available(iOS 11.0, *) {
            switch self.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            @unknown default:
                return .none
            }
        } else {
            return  self.canEvaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                error: nil) ? .touchID : .none
        }
    }
}
