//
//  SheetDrawerHelper.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import FloatingPanel

class SheetDrawerLayout: FloatingPanelLayout {
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .full
    var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
        return [
            .full: FloatingPanelLayoutAnchor(absoluteInset: 60.0, edge: .top, referenceGuide: .safeArea),
            .half: FloatingPanelLayoutAnchor(fractionalInset: 0.5, edge: .bottom, referenceGuide: .safeArea)
        ]
    }
}
