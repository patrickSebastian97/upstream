//
//  NSMutableAttributedString.swift
//  qlip
//
//  Created by Dan Faerae on 17/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    func normal(_ text: String, color: UIColor? = nil) -> NSMutableAttributedString {
        var attributes: [NSAttributedString.Key: Any] = [:]
        if color != nil {
            attributes[NSAttributedString.Key.foregroundColor] = color
        }
        let text = NSMutableAttributedString(string: "\(text)", attributes: attributes)
        self.append(text)

        return self
    }

    func bold(_ text: String, boldFont: UIFont, color: UIColor? = nil) -> NSMutableAttributedString {
        var attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: boldFont]
        if color != nil {
            attributes[NSAttributedString.Key.foregroundColor] = color
        }
        let boldText = NSMutableAttributedString(string: "\(text)", attributes: attributes)
        self.append(boldText)

        return self
    }

    func italic(_ text: String, italicFont: UIFont, color: UIColor? = nil) -> NSMutableAttributedString {
        var attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: italicFont]
        if color != nil {
            attributes[NSAttributedString.Key.foregroundColor] = color
        }
        let italicText = NSMutableAttributedString(string: "\(text)", attributes: attributes)
        self.append(italicText)

        return self
    }
}
