//
//  Storage.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 05/08/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

struct Storage {

    enum DeeplinkPN {
        static let deeplinkKey: String = "DeepLink_pn"
        static func setDeeplinkPN(deepLink : URL) {
            UserDefaults.standard.set(deepLink, forKey: deeplinkKey)
        }
        static func getDeeplinkPN() -> URL? {
            return UserDefaults.standard.url(forKey: deeplinkKey)
        }
        static func clearDeeplinkPN() {
            UserDefaults.standard.removeObject(forKey: deeplinkKey)
        }
    }
    enum User {
        static let userTypeKey: String = "Storage_UserType_Key"
        static func setUserType(type: String) {
            UserDefaults.standard.set(type, forKey: userTypeKey)
        }
        static func getUserType() -> String {
            UserDefaults.standard.string(forKey: userTypeKey) ?? "none"
        }
    }
    enum UserFirstLogin {
        static let userFirstLoginKey = UserDefaults.standard
        static let getUserFirstLogin = userFirstLoginKey.bool(forKey: "userFirstLoginKeyStored")
        static func setUserFirstLogin() {
            userFirstLoginKey.set(true, forKey: "userFirstLoginKeyStored")
        }
    }
    enum ParentData {
        static let parentDataKey: String = "Parent_Data_Key"
        static func save(data: ParentRealmModel) {
            if let encoded = try? JSONEncoder().encode(data) {
                UserDefaults.standard.set(encoded, forKey: parentDataKey)
            }
        }
        static func getParent() -> ParentRealmModel? {
            if let data = UserDefaults.standard.object(forKey: parentDataKey) as? Data {
                return try? JSONDecoder().decode(ParentRealmModel.self, from: data)
            }
            return nil
        }
        static func delete() {
            UserDefaults.standard.set(nil, forKey: parentDataKey)
        }
    }
    enum EventTracking {
        static let firstInstallDateKey: String = "First_Install_Date_Key"
        static func saveFirstInstallDate(date: Date?) {
            if let date = date {
                UserDefaults.standard.set(date, forKey: firstInstallDateKey)
            }
        }
        static func getFirstInstallDate() -> Date? {
            UserDefaults.standard.object(forKey: firstInstallDateKey) as? Date
        }

        static let firstParentRegisterKey: String = "First_Parent_Register_Key"
        static func saveFirstParentRegistered(date: Date) {
            UserDefaults.standard.set(date, forKey: firstParentRegisterKey)
        }
        static func getFirstParentRegistered() -> Date? {
            UserDefaults.standard.object(forKey: firstParentRegisterKey) as? Date
        }

        static let firstKidRegisterKey: String = "First_Kid_Register_Key"
        static func saveFirstKidRegistered(date: Date) {
            UserDefaults.standard.set(date, forKey: firstKidRegisterKey)
        }
        static func getFirstKidRegistered() -> Date? {
            UserDefaults.standard.object(forKey: firstKidRegisterKey) as? Date
        }

        static let eventOrderCountKey: String = "Event_Order_Counter_Key"
        static func saveEventOrder(count: Int) {
            UserDefaults.standard.set(count, forKey: eventOrderCountKey)
        }
        static func getEventOrder() -> Int {
            UserDefaults.standard.integer(forKey: eventOrderCountKey)
        }

        static let firstInstallVersionKey: String = "First_Install_Version_Key"
        static func saveFirstInstallVersion(version: String) {
            UserDefaults.standard.stringArray(forKey: firstInstallVersionKey)
        }
        static func getFirstInstallVersion() -> String? {
            UserDefaults.standard.string(forKey: firstInstallVersionKey)
        }

        static let kidDobKey: String = "Kid_Dob_Key"
        static func saveKidDob(dob: String) {
            UserDefaults.standard.set(dob, forKey: kidDobKey)
        }
        static func getKidDob() -> String? {
            UserDefaults.standard.string(forKey: kidDobKey)
        }
        static func removeKidDob() {
            UserDefaults.standard.removeObject(forKey: kidDobKey)
        }
    }
    enum PickerHomeFeed {
        static let pickerHomeFeedKey: String = "Picker_Home_Feed_Key"
        static func savePickerIndex(index: Int) {
            UserDefaults.standard.set(index, forKey: pickerHomeFeedKey)
        }
        static func getPickerIndex() -> Int {
            UserDefaults.standard.integer(forKey: pickerHomeFeedKey)
        }
    }
    enum SoundState {
        static let soundSateKey: String = "Sound_State_Key"
        static func setSoundState(isActive: Bool) {
            UserDefaults.standard.set(isActive, forKey: soundSateKey)
        }
        static func getSoundState() -> Bool {
            UserDefaults.standard.bool(forKey: soundSateKey)
        }
    }
}
