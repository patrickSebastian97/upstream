//
//  ImageLoader.swift
//  qlip
//
//  Created by Amelia Lim on 30/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import Kingfisher
import SkeletonView

extension UIImageView {

    func loadImage(
        url: String?,
        indicatorType: IndicatorType = .activity,
        placeholder: UIImage = UIImage(),
        onSuccess: ((_ image: UIImage) -> Void)? = nil,
        onFailure: ((_ error: KingfisherError) -> Void)? = nil
    ) {
        let imageUrl = URL(string: url ?? "")

        let shimmerIndicator = ShimmerIndicator()
        switch indicatorType {
        case .activity:
            self.kf.indicatorType = .activity
        default:
            self.kf.indicatorType = .custom(indicator: shimmerIndicator)
        }
        self.kf.setImage(
            with: imageUrl,
            placeholder: placeholder,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.none),
                .cacheOriginalImage
            ],
            completionHandler: { result in
                switch result {
                case .success(let value):
                    onSuccess?(value.image)
                case .failure(let error):
                    onFailure?(error)
                }
        })
    }
}

struct ShimmerIndicator: Indicator {
    let view: UIView = UIView()

    func startAnimatingView() {
        view.showAnimatedGradientSkeleton()
        view.isHidden = false
    }
    func stopAnimatingView() {
        view.hideSkeleton()
        view.isHidden = true
    }

    init() {
        view.isSkeletonable = true
    }
}
