//
//  Base64Data.swift
//  qlip
//
//  Created by Amelia Lim on 14/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

extension UIImage {
    func apiBase64(imageQuality: CGFloat) -> String {
        var base64String = ""

        let imageData = self.jpegData(compressionQuality: imageQuality)
        base64String = imageData?.base64EncodedString(options: .init(rawValue: 0)) ?? ""

        if base64String == "" {
            return ""
        }

        return "data:image/png;base64," + base64String
    }
}
