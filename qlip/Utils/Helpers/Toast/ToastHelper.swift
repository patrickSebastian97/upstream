//
//  ToastHelper.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import Loaf

extension UIViewController {

    func showSuccessToast(
        message: String,
        icon: UIImage = Asset.General.correct.image,
        iconAlignment: Loaf.Style.IconAlignment = .left,
        backgroundColor: UIColor = Asset.Color.green.color,
        textColor: UIColor = Asset.Color.white.color,
        direction: Loaf.Direction = .vertical,
        location: Loaf.Location = .top
    ) {
        let loaf = Loaf(message,
                        state: .custom(.init(
                                        backgroundColor: backgroundColor,
                                        textColor: textColor,
                                        font: .medium14,
                                        icon: icon,
                                        textAlignment: .left,
                                        iconAlignment: iconAlignment
                        )),
                        location: location,
                        presentingDirection: direction,
                        dismissingDirection: .vertical,
                        sender: self)
        loaf.show()
    }

    func showWarningToast(
        message: String,
        icon: UIImage = Asset.General.warning.image,
        iconAlignment: Loaf.Style.IconAlignment = .left,
        backgroundColor: UIColor = Asset.Color.red.color,
        textColor: UIColor = Asset.Color.white.color,
        direction: Loaf.Direction = .vertical,
        location: Loaf.Location = .top
    ) {

        let loaf = Loaf(message,
                        state: .custom(.init(
                                        backgroundColor: backgroundColor,
                                        textColor: textColor,
                                        font: .medium14,
                                        icon: icon,
                                        textAlignment: .left,
                                        iconAlignment: iconAlignment
                        )),
                        location: location,
                        presentingDirection: direction,
                        dismissingDirection: .vertical,
                        sender: self)
        loaf.show()
    }
}
