//
//  UIPageControl.swift
//  qlip
//
//  Created by Amelia Lim on 24/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

@available(iOS 11.0, tvOS 11.0, *)
extension UIPageControl {

    convenience public init(pageIndicatorTintColor: UIColor, currentPageIndicatorTintColor: UIColor) {
        self.init()
        self.pageIndicatorTintColor = pageIndicatorTintColor
        self.currentPageIndicatorTintColor = currentPageIndicatorTintColor
    }

}
