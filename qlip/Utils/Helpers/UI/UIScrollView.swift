//
//  UIScrollView.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

extension UIScrollView {
    func scrollToBottom() {
        let rect = CGRect(x: contentSize.width - 1, y: contentSize.height - 1, width: 1, height: 1)
        scrollRectToVisible(rect, animated: true)
    }

    func scrollToTop() {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        scrollRectToVisible(rect, animated: true)
    }

    /// Scroll to a specific view so that it's top is at the top our scrollview.
    func scrollToView(view: UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(
                CGRect(x: 0, y: childStartPoint.y, width: 1, height: self.frame.height),
                animated: animated
            )
        }
    }
}
