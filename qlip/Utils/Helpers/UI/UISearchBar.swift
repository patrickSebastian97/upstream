//
//  UISearchBar.swift
//  qlip
//
//  Created by Dan Faerae on 18/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

extension UISearchBar {
    var textField: UITextField? {
        if #available(iOS 13.0, *) {
            return self.searchTextField
        } else {
            // Fallback on earlier versions
            for view in (self.subviews[0]).subviews {
                if let textField = view as? UITextField {
                    return textField
                }
            }
        }
        return nil
    }
}
