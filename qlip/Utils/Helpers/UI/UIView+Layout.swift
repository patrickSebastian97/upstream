//
//  UIView+Layout.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import LBTATools

@available(iOS 11.0, tvOS 11.0, *)
extension UIView {

    fileprivate func _vstack(
        _ axis: NSLayoutConstraint.Axis = .vertical,
        views: [UIView],
        spacing: CGFloat = 0,
        alignment: UIStackView.Alignment = .fill,
        distribution: UIStackView.Distribution = .fill
    ) -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: views)
        stackView.isSkeletonable = true
        stackView.axis = axis
        stackView.spacing = spacing
        stackView.alignment = alignment
        stackView.distribution = distribution
        addSubview(stackView)
        stackView.fillSuperview()
        return stackView
    }

    @discardableResult
    open func vstack(
        _ views: UIView...,
        spacing: CGFloat = 0,
        alignment: UIStackView.Alignment = .fill,
        distribution: UIStackView.Distribution = .fill
    ) -> UIStackView {
        return _vstack(
            .vertical,
            views: views,
            spacing: spacing,
            alignment: alignment,
            distribution: distribution
        )
    }

    open func addSubViews(_ views: UIView...) {
        views.forEach({ addSubview($0) })
    }

}
