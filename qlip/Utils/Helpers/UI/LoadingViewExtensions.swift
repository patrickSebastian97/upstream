//
//  LoadingViewExtensions.swift
//  qlip
//
//  Created by Amelia Lim on 29/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import SnapKit
import PromiseKit

extension UIView {
    enum LoadingPosition {
        case top
        case center
    }

    static let loadingViewTag = 283982932
    static let loadingOverlayViewTag = 827162616

    func hideLoading() {
        let loadingIndicator = self.viewWithTag(UIView.loadingViewTag)
        let overlayView = self.viewWithTag(UIView.loadingOverlayViewTag)
        loadingIndicator?.removeFromSuperview()
        overlayView?.removeFromSuperview()
    }

    @discardableResult
    func showLoading(position: LoadingPosition = .center, withOverlay: Bool = false) -> Guarantee<Bool> {
        hideLoading()

        // overlay
        let overlay = UIView()
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        overlay.tag = UIView.loadingOverlayViewTag
        if withOverlay {
            self.addSubview(overlay)
            overlay.snp.makeConstraints { maker in
                maker.edges.equalToSuperview()
            }
        }

        let loadingContainer = UIView()
        loadingContainer.tag = UIView.loadingViewTag
        self.addSubview(loadingContainer)
        switch position {
        case .top:
            loadingContainer.snp.makeConstraints { (maker) in
                maker.centerX.equalToSuperview()
                maker.top.equalToSuperview()
            }
        case .center:
            loadingContainer.snp.makeConstraints { (maker) in
                maker.centerX.equalToSuperview()
                maker.centerY.equalToSuperview()
            }
        }

        // add default activity indicator
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.startAnimating()
        loadingContainer.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }

        return Guarantee { resolver in
            resolver(true)
        }
    }
}
