//
//  EnterCodeTextfield.swift
//  qlip
//
//  Created by Amelia Lim on 25/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation
import UIKit

@objc protocol EnterCodeTextFieldDelegate: class {
    @objc optional func enterCodeTextField(didDelete textField: EnterCodeTextField)
    @objc optional func enterCodeTextField(willDelete textField: EnterCodeTextField)
}

class EnterCodeTextField: UITextField {
    weak var enterCodeTextFieldDelegate: EnterCodeTextFieldDelegate?

    override func deleteBackward() {
        enterCodeTextFieldDelegate?.enterCodeTextField?(willDelete: self)
        super.deleteBackward()
        enterCodeTextFieldDelegate?.enterCodeTextField?(didDelete: self)
    }
}
