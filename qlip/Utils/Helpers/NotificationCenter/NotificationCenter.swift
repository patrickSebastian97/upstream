//
//  NotificationCenter.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let mainTabBarDidChange = Notification.Name("mainTabBarDidChange")
}

protocol MainTabBarNotificationCenter: class {
    // put this on viewDidLoad
    func enableMainTabBarNotification()
    // put this on deinit
    func disableMainTabBarNotification()
}

extension MainTabBarNotificationCenter where Self: UIViewController {

    func enableMainTabBarNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(mainTabBarDidChange(notification:)),
            name: .mainTabBarDidChange,
            object: nil
        )
    }

    func disableMainTabBarNotification() {
        NotificationCenter.default.removeObserver(self)
    }

}

extension UIViewController {
    @objc func mainTabBarDidChange(notification: NSNotification) {}
}
