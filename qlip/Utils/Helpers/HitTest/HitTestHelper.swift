//
//  HitTestHelper.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

extension UIViewController {

    func getIndexPathFromTableView(button: UIButton, tableView: UITableView) -> IndexPath? {
        let hitPoint = button.convert(CGPoint.zero, to: tableView)

        return tableView.indexPathForRow(at: hitPoint)
    }

    func getIndexPathFromCollectionView(button: UIButton, collectionView: UICollectionView) -> IndexPath? {
        let hitPoint = button.convert(CGPoint.zero, to: collectionView)

        return collectionView.indexPathForItem(at: hitPoint)
    }

}
