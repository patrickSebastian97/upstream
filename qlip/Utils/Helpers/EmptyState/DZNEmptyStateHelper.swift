//
//  DZNEmptyStateHelper.swift
//  qlip
//
//  Created by Dan Faerae on 17/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

import UIKit

struct EmptyStateButton {
    let title: String
    let action: (() -> Void)
}

// swiftlint:disable type_name
// swiftlint:disable nesting
extension UIButton {
    private func actionHandler(action: (() -> Void)? = nil) {
        struct __ { static var action : (() -> Void)? }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }

    @objc private func triggerActionHandler() {
        self.actionHandler()
    }

    func actionHandler(controlEvents control :UIControl.Event, forAction action:  @escaping () -> Void) {
        self.actionHandler(action: action)
        self.addTarget(self, action: #selector(triggerActionHandler), for: control)
    }
}
// swiftlint:enable type_name
// swiftlint:enable nesting

extension UIViewController {
    func getAttributedTextEmptyState(_ text: String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: text)

        attributedString.addAttribute(
            NSAttributedString.Key.font,
            value: UIFont.medium14,
            range: NSRange.init(location: 0, length: text.count)
        )
        attributedString.addAttribute(
            NSAttributedString.Key.foregroundColor,
            value: Asset.Color.black.color,
            range: NSRange.init(location: 0, length: text.count)
        )

        return attributedString
    }

    func getCustomView(
        image: UIImage,
        text: String
    ) -> UIView {
        let imageView: UIImageView = {
            let view = UIImageView(image: image, contentMode: .scaleAspectFit)

            return view
        }()
        let titleLabel: UILabel = {
            let view = UILabel(
                text: "",
                font: .heavy14,
                textColor: Asset.Color.black.color.withAlphaComponent(0.3),
                textAlignment: .center,
                numberOfLines: 0
            )
            view.text = text
            view.lineBreakMode = .byWordWrapping

            return view
        }()

        let view = UIView()
        view.vstack(
            titleLabel,
            imageView,
            spacing: 20
        )

        return view
    }

    func getCustomViewInboxAsGuess(
        image: UIImage,
        text: String,
        button: EmptyStateButton
    ) -> UIView {
        let imageView: UIImageView = {
            let view = UIImageView(image: image, contentMode: .top)

            return view
        }()
        let titleLabel: UILabel = {
            let view = UILabel(
                text: "",
                font: .heavy14,
                textColor: Asset.Color.black.color.withAlphaComponent(0.3),
                textAlignment: .center,
                numberOfLines: 0
            )
            view.text = text
            view.lineBreakMode = .byWordWrapping

            return view
        }()
        let button: Button = {
            let view = Button(title: button.title, type: .yellow)
            view.actionHandler(controlEvents: .touchUpInside, forAction: {
                button.action()
            })
            view.imageButtonSpacing = 16

            return view
        }()

        let view = UIView()
//        view.vstack(
//            titleLabel,
//            UIView().vstack(button.withHeight(UIStyle.Button.height), alignment: .center),
//            imageView,
//            spacing: 20
//        )
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { maker in
            maker.top.equalToSuperview().offset(20)
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
        }

        let buttonView = UIView()
        buttonView.vstack(button, alignment: .center)
        view.addSubview(buttonView)
        buttonView.snp.makeConstraints { maker in
            maker.top.equalTo(titleLabel.snp.bottom).offset(20)
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.height.equalTo(UIStyle.Button.height)
        }

        view.addSubview(imageView)
        imageView.snp.makeConstraints { maker in
            maker.top.equalTo(button.snp.bottom).offset(20)
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.bottom.equalToSuperview()
        }

        return view.withHeight(self.view.frame.height)
    }

    func getCustomViewWithButton(
        image: UIImage,
        text: NSAttributedString,
        button: EmptyStateButton
    ) -> UIView {
        let imageView: UIImageView = {
            let view = UIImageView(image: image, contentMode: .scaleAspectFit)

            return view
        }()
        let titleLabel: UILabel = {
            let view = UILabel(
                text: "",
                font: .heavy24,
                textColor: Asset.Color.black.color,
                textAlignment: .center,
                numberOfLines: 0
            )
            view.attributedText = text
            view.lineBreakMode = .byWordWrapping

            return view
        }()
        let textLabel: UILabel = {
            let view = UILabel(
                text: "",
                font: .medium14,
                textColor: Asset.Color.black.color,
                textAlignment: .center,
                numberOfLines: 0
            )
            view.attributedText = text
            view.lineBreakMode = .byWordWrapping

            return view
        }()
        let button: Button = {
            let view = Button(title: button.title, type: .yellow)
            view.actionHandler(controlEvents: .touchUpInside, forAction: {
                button.action()
            })

            return view
        }()

        let view = UIView()
        view.vstack(
            imageView,
            titleLabel,
            textLabel,
            UIView().vstack(button.withHeight(UIStyle.Button.height), alignment: .center),
            spacing: 20
        )

        return view
    }

    func getCustomViewMyClips(
    image: UIImage,
    text: String,
    button: EmptyStateButton
    ) -> UIView {
        let imageView: UIImageView = {
            let view = UIImageView(image: image, contentMode: .scaleAspectFit)

            return view
        }()
        let titleLabel: UILabel = {
            let view = UILabel(
                text: "",
                font: .heavy20,
                textColor: Asset.Color.blackAlpha20.color,
                textAlignment: .center,
                numberOfLines: 0
            )
            view.text = text
            view.lineBreakMode = .byWordWrapping

            return view
        }()
        let button: Button = {
            let view = Button(title: button.title, type: .yellow)
            view.actionHandler(controlEvents: .touchUpInside, forAction: {
                button.action()
            })

            return view
        }()

        let view = UIView()

        view.vstack(
            imageView,
            titleLabel,
            UIView().vstack(button.withHeight(UIStyle.Button.height), alignment: .center),
            spacing: 20
        )
        return view

    }
}
