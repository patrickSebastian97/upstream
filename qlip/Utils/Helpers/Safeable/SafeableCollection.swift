//
//  SafeableCollection.swift
//  qlip
//
//  Created by Dan Faerae on 11/09/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

extension Collection {
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
