//
//  MultiLanguage.swift
//  qlip
//
//  Created by Amelia Lim on 22/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

enum MultiLanguageEnum: String {
    case idLang = "id"
    case enLang = "en"
}

struct MultiLanguageDefaultsKey {

    // language
    static let language = "multi-language"
    static let selectedLanguage = "selected-language"
}

struct MultiLanguage {

    static func setLanguage(language: MultiLanguageEnum) {
        UserDefaults.standard.set(language.rawValue, forKey: MultiLanguageDefaultsKey.language)
    }

    static func getLanguage() -> MultiLanguageEnum {
        guard let language = UserDefaults.standard.string(forKey: MultiLanguageDefaultsKey.language) else {
            return .idLang
        }

        return MultiLanguageEnum(rawValue: language) ?? .idLang
    }

}

struct PlaySafeDefaultKey {
    static let playSafe = "play-safe"
}

struct SoundDefaultKey {
    static let soundState = "sound-state"
}
