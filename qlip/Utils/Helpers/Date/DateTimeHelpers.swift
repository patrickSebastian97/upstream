//
//  DateTimeHelpers.swift
//  qlip
//
//  Created by Amelia Lim on 26/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import DateHelper

extension TimeInterval {
    var timeString: String {
        let minutes = Int(self) / 60 % 60
        let seconds = Int(self) % 60
        let formatedTime = String(format: "%02i:%02i", minutes, seconds)

        return formatedTime
    }
}

enum DateFormat {
    static let isoDate: String = "dd/MM/yyyy"
    static let isoDateEnglish: String = "yyyy/MM/dd"
    static let isoDateStripEnglish: String = "yyyy-MM-dd"
    static let shortDate: String = "d MMMM yyyy"
    static let year4Digit: String = "yyyy"
    static let dateWithTime: String = "d MMM yyyy hh:mm"
    static let isoDateTimeFracSec: String = "yyyy-MM-dd'T'hh:mm:ss.SSS"
    static let dateWithTimeMyRewards: String = "MMM d, yyyy HH:mm"
    static let day2Digit: String = "dd"
    static let month2Digit: String = "MM"
    static let dateWithTimeClips: String = "dd MMM, HH:mm"
}

extension Locale {
    static func indonesia() -> Locale {
        return Locale(identifier: "id_ID")
    }
}

extension Int64 {
    func date() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self))
    }

    /// Convert date to fomat: d MMM yyyy. Example 13 Oct 2019.
    var shortDate: String {
        return date().shortDate
    }

    /// Convert date to fomat: dd/MM/yy. Example 13/06/2019.
    var isoDate: String {
        return date().isoDate
    }
}

extension Date {
    /// Convert date to fomat: d MMM yyyy. Example 13 Oct 2019.
    var shortDate: String {
        return self.toString(format: .custom(DateFormat.shortDate), locale: Locale.indonesia())
    }

    /// Convert date to fomat: yyyy. Example 2019.
    var year: String {
        return self.toString(format: .custom(DateFormat.year4Digit))
    }

    /// Convert date to format: MM. Example 12.
    var month: String {
        return self.toString(format: .custom(DateFormat.month2Digit))
    }

    /// Convert date to fomat: yyyy-MM-dd. Example 2019-12-01
    var isoDateStripEnglish: String {
        return self.toString(format: .custom(DateFormat.isoDateStripEnglish))
    }

    var unixTimestamp: TimeInterval {
        return timeIntervalSince1970
    }

    /// Convert date to fomat: MMM d, yyyy HH:mm. Example Jun 13, 2020 10:00
    var myRewardDateFormat: String {
        return self.toString(format: .custom(DateFormat.dateWithTimeMyRewards))
    }

    /// Convert date to format: dd/MM/yyyy. Example 01/12/2020
    var isoDate: String {
        return self.toString(format: .custom(DateFormat.isoDate))
    }

    /// Convert date to times ago
    func timeAgoSinceDate() -> String {

        // From Time
        let fromDate = self

        // To Time
        let toDate = Date()

        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents(
            [.year], from: fromDate, to: toDate).year, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
        }

        // Month
        if let interval = Calendar.current.dateComponents(
            [.month], from: fromDate, to: toDate).month, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }

        // Week
        if let interval = Calendar.current.dateComponents(
            [.weekOfYear], from: fromDate, to: toDate).weekOfYear, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "week ago" : "\(interval)" + " " + "weeks ago"
        }

        // Day
        if let interval = Calendar.current.dateComponents(
            [.day], from: fromDate, to: toDate).day, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
        }

        // Hours
        if let interval = Calendar.current.dateComponents(
            [.hour], from: fromDate, to: toDate).hour, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "hour ago" : "\(interval)" + " " + "hours ago"
        }

        // Minute
        if let interval = Calendar.current.dateComponents(
            [.minute], from: fromDate, to: toDate).minute, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "minute ago" : "\(interval)" + " " + "minutes ago"
        }

        return "a moment ago"
    }

    // Convert local time to UTC (or GMT)
     func toGlobalTime() -> Date {
         let timezone = TimeZone.current
         let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
         return Date(timeInterval: seconds, since: self)
     }

     // Convert UTC (or GMT) to local time
     func toLocalTime() -> Date {
         let timezone = TimeZone.current
         let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
         return Date(timeInterval: seconds, since: self)
     }
}

extension Int {
    /// Convert Int to Date
    var date: Date? {
        return Date(timeIntervalSince1970: TimeInterval(self))
    }
}

extension Double {
    /// Convert Double to Date
    var date: Date? {
        return Date(timeIntervalSince1970: TimeInterval(self))
    }
}

extension UIViewController {
   func getMonthNames() -> [String] {
        var monthNames: [String] = []
        let dateFormatter = DateFormatter()
        for idx in 0..<12 {
            monthNames.append("\(dateFormatter.monthSymbols[idx])")
        }

        return monthNames
    }

   func getYears() -> [String] {
        var years: [String] = []
        let currentCalendar = Calendar.current
        for idx in 0..<100 {
            if let date = currentCalendar.date(byAdding: .year, value: (idx * -1), to: Date()) {
                years.append(date.year)
            }
        }

        return years.reversed()
    }

    func daysBetween(end: Date) -> Int {
          return Calendar.current.dateComponents([.day], from: Date(), to: end).day!
      }

    func hoursBetween(end: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: Date(), to: end).hour!
    }

    func minuteBetween(end: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: Date(), to: end).minute!
    }

    func secondsBetween(end: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: Date(), to: end).second!
    }

    func daysBetween(start: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: Date()).day!
    }

    func secondsBetween(start: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: start, to: Date()).second!
    }

    func timeBetween(end: Date) -> String {
        print("\(end)")
        let different = Calendar.current.dateComponents([.hour, .minute, .second], from: Date(), to: end)
        guard let hour = different.hour, let minute = different.minute, let second = different.second else {
            return ""
        }

        return "\(hour):\(minute):\(second)"
    }
}
