//
//  Drawer.swift
//  qlip
//
//  Created by Amelia Lim on 06/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

protocol OverlaySheetDrawerProtocol: class {
    var onDismiss: ((_ controller: UIViewController, _ completion: (() -> Void)?) -> Void)! { get set }
}

final class OverlaySheetDrawerViewController: UIViewController {
    // MARK: - Properties

    fileprivate var overlayView: UIView!

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .clear

        overlayView = UIView(frame: .zero)
        overlayView.backgroundColor = .black
        overlayView.alpha = 0
        view.addSubview(overlayView)
        overlayView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }

        navigationController?.navigationBar.isHidden = true

        UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
            self.overlayView.alpha = 0.7
        })
    }

    // MARK: - Helpers

    func dismissWithAnimation(completion: (() -> Void)?) {
        UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
            self.overlayView.alpha = 0
        })
        dismiss(animated: false, completion: completion)
    }
}

class ContainerViewController: UIViewController {
    // MARK: - Properties

    typealias TapAnyWhereCallback = (() -> Void)

    lazy var grabberView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Color.blackAlpha20.color

        return view
    }()
    lazy var tapAnyWhereButton: UIButton = {
        let view = UIButton(type: .system)
        view.addTarget(self, action: #selector(anyWhereTapped), for: .touchUpInside)

        return view
    }()

    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = true

        return view
    }()

    var tapAnyWhereCallback: TapAnyWhereCallback?

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(containerView)
        containerView.snp.makeConstraints { maker in
            maker.top.greaterThanOrEqualToSuperview().inset(60)
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        self.view.insertSubview(tapAnyWhereButton, at: 0)
        tapAnyWhereButton.fillSuperview()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        containerView.roundCorners(corners: [.topLeft, .topRight], radius: 16)
        grabberView.layer.cornerRadius = grabberView.layer.frame.height / 2.0
        grabberView.layer.masksToBounds = true
    }

    // MARK: - Helpers

    func add(controller: UIViewController) {
        addChild(controller)

        // add grabble view
        containerView.addSubview(grabberView)
        grabberView.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(10)
            maker.centerX.equalToSuperview()
            maker.width.equalTo(60)
            maker.height.equalTo(4)
        }

        // add child content
        containerView.addSubview(controller.view)
        controller.view.snp.makeConstraints { maker in
            maker.top.equalTo(grabberView.snp.bottom).inset(-10)
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        controller.didMove(toParent: self)
    }

    // MARK: - Actions

    @objc private func anyWhereTapped() {
        tapAnyWhereCallback?()
    }
}

class SheetDrawer {
    static func show(
        onController: UIViewController,
        drawerController: (UIViewController & OverlaySheetDrawerProtocol),
        tapAnyWhereToClose: Bool = false,
        overNavigationController: Bool = false
    ) {
        let containerViewController = ContainerViewController()
        let containerNavigation = UINavigationController(rootViewController: containerViewController)
        containerNavigation.view.backgroundColor = .clear
        containerNavigation.modalPresentationStyle = .overCurrentContext
        containerNavigation.providesPresentationContextTransitionStyle = true
        containerNavigation.definesPresentationContext = true
        containerViewController.add(controller: drawerController)
        containerViewController.modalPresentationStyle = .overFullScreen
        containerViewController.loadViewIfNeeded()

        let overlayViewController = OverlaySheetDrawerViewController()
        let navigation = UINavigationController(rootViewController: overlayViewController)
        navigation.view.backgroundColor = .clear
        navigation.modalPresentationStyle = .overCurrentContext
        navigation.providesPresentationContextTransitionStyle = true
        navigation.definesPresentationContext = true

        if tapAnyWhereToClose {
            containerViewController.tapAnyWhereCallback = {
                drawerController.dismiss(animated: true, completion: nil)
                overlayViewController.dismissWithAnimation(completion: nil)
            }
        }

        // on swiped, close all controllers
        drawerController.onDismiss = { controller, completion in
            controller.dismiss(animated: true, completion: nil)
            overlayViewController.dismissWithAnimation(completion: completion)
        }
        onController.present(navigation, animated: false, completion: nil)
        if overNavigationController {
            navigation.present(containerNavigation, animated: true, completion: nil)
        } else {
            navigation.present(containerViewController, animated: true, completion: nil)
        }
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: radius, height: radius)
        )
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
