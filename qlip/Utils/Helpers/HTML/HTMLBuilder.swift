//
//  HTMLBuilder.swift
//  qlip
//
//  Created by Amelia Lim on 29/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(
                data: data,
                options:
                [NSAttributedString.DocumentReadingOptionKey.documentType:
                    NSAttributedString.DocumentType.html],
                documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }

    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }

    var htmlStripped : String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }

    var htmlWithMetaData: String {
        // swiftlint:disable line_length
        let html = """
            <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
                    <style>* { font-family: "Helvetica Neue"; font-size: "14px"; color: #1E1E1E; }</style>
                </head>
                <body>
                \(self)
                </body>
            </html>
        """
        // swiftlint:enable line_length

        return html
    }
}
