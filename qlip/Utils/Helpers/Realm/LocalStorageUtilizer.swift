//
//  LocalStorageUtilizer.swift
//  qlip
//
//  Created by Amelia Lim on 09/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation
import RealmSwift

protocol LocalStorageUtilizer {
    static func getAllObjects(filterStr: String) -> [Self]
    static func getFirstObject(filterStr: String) -> Self?
    static func doesExist(filterStr: String) -> Bool
    static func deleteAll()
    func save()
    func delete()
}

extension LocalStorageUtilizer where Self: Object {

    static func getRealm() -> Realm {
        guard let realm = try? Realm() else { fatalError("Can't instantiate Realm.") }

        return realm
    }

    func getRealm() -> Realm {
        guard let realm = try? Realm() else { fatalError("Can't instantiate Realm.") }

        return realm
    }

    static func getAllObjects(filterStr: String = "") -> [Self] {
        let realm = getRealm()

        var objects = realm.objects(Self.self)
        if !filterStr.isEmpty { objects = objects.filter(filterStr) }

        var newObjects: [Self] = []
        objects.forEach({
            newObjects.append($0)
        })
        return newObjects
    }

    static func getFirstObject(filterStr: String = "") -> Self? {
        return getAllObjects(filterStr: filterStr).first
    }

    static func doesExist(filterStr: String = "") -> Bool {
        return getFirstObject(filterStr: filterStr) != nil
    }

    static func deleteAll() {
        let realm = getRealm()

        do {
            let objects = realm.objects(Self.self)

            try realm.write {
                realm.delete(objects)
            }
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }

    func save() {
        let realm = getRealm()

        do {
            try realm.write {
                realm.add(self)
            }
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }

    func delete() {
        let realm = getRealm()

        do {
            try realm.write {
                realm.delete(self)
            }
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
}
