//
//  FormatableNumber.swift
//  qlip
//
//  Created by Amelia Lim on 21/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

protocol FormattableNumber {
    func formatNumber(_ number: Int, withSeparator: Bool) -> String
}

extension FormattableNumber {
    func formatNumber(_ number: Int, withSeparator: Bool = true) -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en-POSIX")
        formatter.numberStyle = withSeparator ? .decimal : .none

        guard let str = formatter.string(from: NSNumber(value: number)) else {
            fatalError("Can't format number \(number).")
        }

        return str
    }

    func formatToPoint(_ number: Int) -> String {
        return "\(formatNumber(number, withSeparator: true)) PTS"
    }

    func formatToGetPoint(_ number: Int) -> String {
        return "+\(formatNumber(number, withSeparator: true)) PTS"
    }
}
