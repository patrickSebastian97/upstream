//
//  DismissableKeyboard.swift
//  qlip
//
//  Created by Amelia Lim on 25/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

protocol DismissableKeyboard: class {
    func enableDismissableKeyboard(cancelsTouchesInView: Bool)
    func disableDismissableKeyboard()
}

extension DismissableKeyboard where Self: UIViewController {

    func enableDismissableKeyboard(cancelsTouchesInView: Bool = false) {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapGesture.cancelsTouchesInView = cancelsTouchesInView
        view.addGestureRecognizer(tapGesture)
    }

    func disableDismissableKeyboard() {
        NotificationCenter.default.removeObserver(self)
    }

}

extension UIViewController {

    @objc func viewTapped(_ sender: Any) {
        view.endEditing(true)
    }

    @objc func hideKeyboard() {
        viewTapped(self)
    }

}
