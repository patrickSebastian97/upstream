//
//  AdaptableKeyboard.swift
//  qlip
//
//  Created by Amelia Lim on 25/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

protocol AdaptableKeyboard: class {
    func enableAdaptableKeyboard()
    func disableAdaptableKeyboard()
    func enableButtonOverKeyboard(
        withSampleButton sampleButton: Button,
        onTextFields textFields: [UITextView], // Changed with TextField View
        onTextViews textviews: [UITextView],
        backgroundColor: UIColor?
    )
    func enableButtonOverKeyboard(
        withSampleButton sampleButton: Button,
        onTextFields textFields: [UITextField],
        onTextViews textviews: [UITextView],
        backgroundColor: UIColor?
    )
}

extension AdaptableKeyboard where Self: UIViewController {

    func enableAdaptableKeyboard() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillChangeFrame(notification:)),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShowNotification(notification:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHideNotification(notification:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

    func disableAdaptableKeyboard() {
        NotificationCenter.default.removeObserver(self)
    }

    func enableButtonOverKeyboard(
        withSampleButton sampleButton: Button,
        onTextFields textFields: [UITextView] = [], // Changed with TextField View
        onTextViews textviews: [UITextView] = [],
        backgroundColor: UIColor? = .clear
    ) {
        enableButtonOverKeyboard(
            withSampleButton: sampleButton,
            onTextFields: textFields.map({ $0 }), // Changed with TextField View
            onTextViews: textviews,
            backgroundColor: backgroundColor
        )
    }

    func enableButtonOverKeyboard(
        withSampleButton sampleButton: Button,
        onTextFields textFields: [UITextField] = [],
        onTextViews textviews: [UITextView] = [],
        backgroundColor: UIColor? = .clear
    ) {
        let inset: CGFloat = 20
        let wrappedView = UIView.init(
            frame: CGRect(
                x: 0,
                y: 0,
                width: sampleButton.frame.width + inset + inset,
                height: sampleButton.frame.height + inset + inset
            )
        )

        wrappedView.backgroundColor = backgroundColor

        let button = Button(type: .system)
        button.type = sampleButton.type
        button.titleLabel?.font =  sampleButton.titleLabel?.font
        button.setTitle(sampleButton.titleLabel?.text, for: .normal)
        button.isStyleEnabled = sampleButton.isStyleEnabled
        sampleButton.didChangeEnableState = { enable in
            button.isStyleEnabled = enable
        }
        if let buttonSelectorString =
            sampleButton.actions(forTarget: self, forControlEvent: .touchUpInside)?.first {
                button.addTarget(
                self,
                action: NSSelectorFromString(buttonSelectorString),
                for: .touchUpInside
            )
        }

        wrappedView.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        [
            button.topAnchor.constraint(equalTo: wrappedView.topAnchor, constant: inset),
            button.leadingAnchor.constraint(equalTo: wrappedView.leadingAnchor, constant: inset),
            button.bottomAnchor.constraint(equalTo: wrappedView.bottomAnchor, constant: -inset),
            button.trailingAnchor.constraint(equalTo: wrappedView.trailingAnchor, constant: -inset)
        ].forEach({ $0.isActive = true })

        for textField in textFields {
            textField.inputAccessoryView = wrappedView
        }
        for textView in textviews {
            textView.inputAccessoryView = wrappedView
        }
    }
}

extension UIViewController {

    @objc func keyboardWillChangeFrame(notification: NSNotification) {
        let info = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue

        if let scrollView = getScrollView(fromView: view), let keyboardFrame = info?.cgRectValue {
            let window = UIApplication.shared.windows.first { $0.isKeyWindow }!
            let keyboardRealHeightOnScreen = window.frame.height - keyboardFrame.origin.y
            var accessoryViewHeight: CGFloat = 0

            if let activeTextField = getActiveTextField(fromView: view) {
                if let accessoryView = activeTextField.inputAccessoryView, keyboardRealHeightOnScreen > 0 {
                    accessoryViewHeight = accessoryView.frame.height
                }
            } else if let activeTextView = getActiveTextView(fromView: view) {
                if let accessoryView = activeTextView.inputAccessoryView, keyboardRealHeightOnScreen > 0 {
                    accessoryViewHeight = accessoryView.frame.height
                }
            }

            scrollView.contentInset = UIEdgeInsets(
                top: scrollView.contentInset.top,
                left: scrollView.contentInset.left,
                bottom: keyboardRealHeightOnScreen - accessoryViewHeight + 5,
                right: scrollView.contentInset.right
            )

            scrollView.scrollIndicatorInsets = scrollView.contentInset
        }
    }

    @objc func keyboardWillShowNotification(notification: NSNotification) {}

    @objc func keyboardWillHideNotification(notification: NSNotification) {}

    func getScrollView(fromView view: UIView) -> UIScrollView? {
        if view is UIScrollView && !(view.superview is UITextField) {
            return view as? UIScrollView
        }

        for subview in view.subviews {
            if let scrollView = getScrollView(fromView: subview) {
                return scrollView
            }
        }

        return nil
    }

    func getActiveTextField(fromView view: UIView) -> UITextField? {
        if view is UITextField && view.isFirstResponder {
            return view as? UITextField
        }

        for subview in view.subviews {
            if let textField = getActiveTextField(fromView: subview) {
                return textField
            }
        }

        return nil
    }

    func getActiveTextView(fromView view: UIView) -> UITextView? {
        if view is UITextView && view.isFirstResponder {
            return view as? UITextView
        }

        for subview in view.subviews {
            if let textView = getActiveTextView(fromView: subview) {
                return textView
            }
        }

        return nil
    }
}
