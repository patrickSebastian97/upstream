//
//  ViewModel.swift
//  qlip
//
//  Created by Amelia Lim on 24/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

class ViewModel<T> {
    var item: T!
    init(item: T) { setItem(item) }
    private func setItem(_ item: T) { self.item = item}
}
