//
//  UserDataManager.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 23/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation

final class UserDataManager: NSObject {
    // MARK: - Properties

    struct Data {
        var username: String?
        var password: String?
        var phone: String?
        var otp: String?
        var kidName: String?
        var kidDob: String?
        var providerToken: String?
        var providerType: String?
        var providerId: String?
    }

    private var data: Data!

    static let shared: UserDataManager = {
        let instance = UserDataManager()
        instance.reset()

        return instance
    }()

    // MARK: - Helpers

    func reset() {
        data = Data()
    }

    func getData() -> Data? {
        return self.data
    }

    func setData(data: Data) {
        self.data = data
    }

    func setPhone(phone: String) {
        self.data.phone = phone
    }

    func setUsername(username: String) {
        self.data.username = username
    }

    func setPassword(password: String) {
        self.data.password = password
    }

    func setOTP(otp: String) {
        self.data.otp = otp
    }

    func setKidName(name: String) {
        self.data.kidName = name
    }

    func setKidDob(dob: String) {
        self.data.kidDob = dob
    }

    func setProviderToken(_ providerToken: String) {
        self.data.providerToken = providerToken
    }

    func setProviderType(_ providerType: String) {
        self.data.providerType = providerType
    }

    func setProviderId(_ providerId: String) {
        self.data.providerId = providerId
    }

}
