//
//  TokenModel.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

struct TokenModel: Codable {
    let tokenType: String
    let expiresIn: Int
    let accessToken: String

    enum Storage {
        static let key = "Saved_TokenModel_Storage_Key"

        static func save(tokenModel: TokenModel) {
            if let encoded = try? JSONEncoder().encode(tokenModel) {
                UserDefaults.standard.set(encoded, forKey: key)
            }
        }

        static func getToken() -> TokenModel? {
            if let token = UserDefaults.standard.object(forKey: key) as? Data {
                return try? JSONDecoder().decode(TokenModel.self, from: token)
            }

            return nil
        }

        static func delete() {
            UserDefaults.standard.set(nil, forKey: key)
        }
    }
}
