//
//  VideoChallengeModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct VideoChallengeModel: Codable {
    let id: String?
    let submissionId: String?
    let name: String?
    let descriptions: String?
    let summary: String?
    let videoSate: String?
    let image: String?
    let bannerImage: String?
    let status: String?
    let userType: String?
    let level: String?
    let duration: Int?
    let languageCode: String?
    let isFeatured: Bool?
    let isStory: Bool?
    let isActive: Bool?
    let targetAgeFrom: Int?
    let targetAgeTo: Int?
    let deadline: Double?
    let isScheduled: Bool?
    let publishDate: Double?
    let totalSubmission: Int?
    let isPromoted: Bool?
    let videoTranscoded: VideoTranscodedModel?
    let videoWatermark: VideoTranscodedModel?
    let contentCreator: ContentCreatorModel?
    let submission: [SubmissionModel]
    let isSubmitted: Bool?
    let qoin: Int?
    let totalExp: Int?
    let totalReaction: Int?
    let isLiked: Bool?
}
