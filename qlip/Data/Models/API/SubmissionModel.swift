//
//  SubmissionModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct SubmissionModel: Codable {
    let id: String?
    let challengeId: String?
    let suspendCategoryId: String?
    let descriptions: String?
    let imageThumbnail: String?
    let videoState: String?
    let userId: String?
    let type: String?
    let userType: String?
    let totalReaction: Int?
    let viewCounter: Int?
    let isSuspected: Bool?
    let isSuspended: Bool?
    let videoTranscoded: VideoTranscodedModel?
    let videoWatermark: VideoTranscodedModel?
    let user: UserModel?
    let userReaction: [UserReactionModel]?
    let isLiked: Bool?
}
