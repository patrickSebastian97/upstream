//
//  ReactionModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct ReactionModel: Codable {
    let id: String?
    let name: String?
    let image: String?
    let languageCode: String?
}
