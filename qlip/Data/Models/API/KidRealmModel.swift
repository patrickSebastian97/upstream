//
//  KidRealmModel.swift
//  qlip
//
//  Created by Amelia Lim on 13/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
final class KidRealmModel: Object, Codable, LocalStorageUtilizer {
    dynamic var id: String = ""
    dynamic var firstName: String = ""
    dynamic var lastName: String = ""
    dynamic var username: String?
    dynamic var type: String = ""
    dynamic var dob: String?
    dynamic var referralCodeUsed: String?
    dynamic var isSetupPin: Bool = false
    dynamic var isSetupUsername: Bool = false
    dynamic var isUsingAvatar: Bool = false
    dynamic var image: String?
    dynamic var isRegistered: Bool?
    dynamic var providerToken: String?
    dynamic var providerType: String?
    dynamic var providerId: String?
    dynamic var phone: String?
//    dynamic var avatar: AvatarModel?
//    var experiences = List<KidExperienceModel>()
    dynamic var family: FamilyModel?
    dynamic var refreshToken: String?
    dynamic var accessToken: String?
    dynamic var tokenType: String?
    dynamic var expiresIn: Int?
    dynamic var totalReaction: Int = 0
//    dynamic var loginRequest: LoginRequest?

    // MARK: - User Helper

    convenience init(
        id: String,
        firstName: String,
        lastName: String,
        username: String?,
        type: String,
        dob: String?,
        referralCodeUsed: String?,
        isSetupPin: Bool,
        isSetupUsername: Bool,
        isUsingAvatar: Bool,
        image: String?,
//        avatar: AvatarModel?,
//        family: FamilyModel?,
        refreshToken: String?,
        accessToken: String?,
        tokenType: String?,
        expiresIn: Int?,
        totalReaction: Int) {
        self.init()
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.username = username
        self.type = type
        self.dob = dob
        self.referralCodeUsed = referralCodeUsed
        self.isSetupPin = isSetupPin
        self.isSetupUsername = isSetupUsername
        self.isUsingAvatar = isUsingAvatar
        self.image = image
//        self.avatar = avatar
//        self.family = family
        self.refreshToken = refreshToken
        self.accessToken = accessToken
        self.tokenType = tokenType
        self.expiresIn = expiresIn
        self.totalReaction = totalReaction
    }

    func deleteAndSave() {
        KidRealmModel.deleteAll()
        self.save()
    }

    static func getUser() -> KidRealmModel {
        if let user = KidRealmModel.getFirstObject() {
            return user
        }

        return KidRealmModel()
    }

    static func getId() -> String {
        return getUser().id
    }

    static func isLoggedIn() -> Bool {
        return getUser().isLoggedIn()
    }

    func isLoggedIn() -> Bool {
        let token = TokenModel.Storage.getToken()
        let user = KidRealmModel.getUser()
        return !user.id.isEmpty && !(token?.accessToken.isEmpty ?? true)
    }

    var fullName: String {
        return "\(firstName) \(lastName)"
    }

    var isFamilySetup: Bool {
        return family != nil || referralCodeUsed == nil
    }

    static func updateAndKeepUserToken(user: KidRealmModel?) {
        guard let user = user else { return }
        // restoring all tokens
        // accessToken
        // tokenType
        let currentUser = KidRealmModel.getUser()
        user.accessToken = currentUser.accessToken
        user.tokenType = currentUser.tokenType

        // remove old user and save the new one
        KidRealmModel.deleteAll()
        user.save()
    }
}
