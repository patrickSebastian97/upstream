//
//  LegalModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation

struct LegalModel: Codable {
    let id: String?
    let title: String?
    let version: String?
    let notes: String?
    let content: String?
    let type: String?
}
