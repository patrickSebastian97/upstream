//
//  InterestSkillTagModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 02/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct InterestSkillTagModel: Codable {
    let id: String?
    let name: String?
    let descriptions: String?
    let imageWhite: String?
    let imageBlack: String?
    let slug: String?
}
