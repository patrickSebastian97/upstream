//
//  VideoTranscodedModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct VideoTranscodedModel: Codable {
    let file: String
}
