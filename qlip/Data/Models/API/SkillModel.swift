//
//  SkillModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct SkillModel: Codable {
    let id: String?
    let name: String?
    let descriptions: String?
    let image: String?
    let languageId: String?
    let isActive: Bool?
    let exp: Int?
}
