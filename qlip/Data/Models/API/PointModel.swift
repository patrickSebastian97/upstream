//
//  PointModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct PointModel: Codable {
    let id: String?
    let point: Int?
    let name: String?
    let notes: String?
    let type: String?
    let isActive: Bool?
    let languageCode: String?
}
