//
//  UserModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct UserModel: Codable {
    let id: String?
    let image: String?
    let isUsingAvatar: Bool?
    let avatar: AvatarModel?
}
