//
//  UsernameCheckModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 23/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation

struct UsernameCheckModel: Codable {
    let isUsed: Bool?
}
