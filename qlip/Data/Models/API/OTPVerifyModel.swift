//
//  OTPVerifyModel.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 28/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation

struct OTPVerifyModel: Codable {
    let status: Bool
    let token: String
}
