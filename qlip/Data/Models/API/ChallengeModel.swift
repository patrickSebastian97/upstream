//
//  ChallengeModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct ChallengeModel: Codable {
    let id: String?
    let name: String?
    let descriptions: String?
    let summary: String?
    let videoState: String?
    let image: String?
    let preparation: String?
    let bannerImage: String?
    let status: String?
    let userType: String?
    let level: String?
    let duration: Int?
    let isFeatured: Bool?
    let isStory: Bool?
    let isActive: Bool?
    let targetAgeFrom: Int?
    let targetAgeTo: Int?
    let deadline: Double?
    let isScheduled: Bool?
    let publishDate: Int64?
    let totalSubmission: Int?
    let isPromoted: Bool?
    let videoTranscoded: VideoTranscodedModel?
    let videoWatermark: VideoTranscodedModel?
    let categories: [CategoryModel]?
    let skills: [SkillModel]?
    let hints: [HintModel]?
    let contentCreator: ContentCreatorModel?
    let point: PointModel?
    let isSubmitted: Bool?
    let isLiked: Bool?
    let isBookmark: Bool?
    let totalExp: Int?
    let totalReaction: Int?
    let totalView: Int?
}
