//
//  ParentRealmModel.swift
//  qlip
//
//  Created by Amelia Lim on 13/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
final class ParentRealmModel: Object, Codable, LocalStorageUtilizer {
    dynamic var id: String = ""
    dynamic var firstName: String = ""
    dynamic var lastName: String = ""
    dynamic var username: String?
    dynamic var email: String?
    dynamic var phone: String = ""
    dynamic var type: String = ""
    dynamic var referralCode: String = ""
    dynamic var isSetupPin: Bool = false
    dynamic var isAgreeNewsletter: Bool = false
    dynamic var image: String?
    dynamic var isCompleteRegister: Bool = false
    dynamic var family: FamilyModel?
    dynamic var refreshToken: String?
    dynamic var accessToken: String?
    dynamic var tokenType: String?
    dynamic var expiresIn: Int?
    dynamic var totalReaction: Int = 0
    dynamic var isEnableBiometric: Bool = false
    dynamic var isSetupPassword: Bool = false
    // MARK: - User Helper

    func deleteAndSave() {
        ParentRealmModel.deleteAll()
        self.save()
    }

    static func getUser() -> ParentRealmModel {
        if let user = ParentRealmModel.getFirstObject() {
            return user
        }

        return ParentRealmModel()
    }

    static func getId() -> String {
        return getUser().id
    }

    static func isLoggedIn() -> Bool {
        return getUser().isLoggedIn()
    }

    func isLoggedIn() -> Bool {
        let token = TokenModel.Storage.getToken()
        let user = ParentRealmModel.getUser()
        return !user.id.isEmpty && !(token?.accessToken.isEmpty ?? true)
    }

    var fullName: String {
        return "\(firstName) \(lastName)"
    }

    static func updateAndKeepUserToken(user: ParentRealmModel?) {
        guard let user = user else { return }
        // restoring all tokens
        // accessToken
        // tokenType
        let currentUser = ParentRealmModel.getUser()
        user.accessToken = currentUser.accessToken
        user.tokenType = currentUser.tokenType

        // remove old user and save the new one
        ParentRealmModel.deleteAll()
        user.save()
    }
}
