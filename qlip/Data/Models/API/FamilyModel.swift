//
//  FamilyModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 21/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
final class FamilyModel: Object, Codable, LocalStorageUtilizer {
    // MARK: - Properties

    dynamic var isSetupFamilyKey: Bool?
    dynamic var id: String = ""
    dynamic var familyKey: String = ""
    dynamic var noOfChildren: Int = 0
    dynamic var registeredChildren: Int = 0
}
