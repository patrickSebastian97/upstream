//
//  UserReactionModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct UserReactionModel: Codable {
    let id: String?
    let submissionId: String?
    let reaction: ReactionModel?
}
