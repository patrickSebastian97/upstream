//
//  ContentCreatorModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

struct ContentCreatorModel: Codable {
    let id: String?
    let name: String?
    let description: String?
    let email: String?
    let phone: String?
    let website: String?
    let instagram: String?
    let facebook: String?
    let image: String?
    let languageCode: String?
    let isActive: Bool?
}
