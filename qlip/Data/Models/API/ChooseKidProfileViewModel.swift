//
//  ChooseKidProfileViewModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 02/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

class ChooseKidProfileViewModel: ViewModel<KidRealmModel> {
    var id: String!
    var imageUrl: String!
    var fullname: String!

    override var item: KidRealmModel! {
        didSet {
            id = item.id
            imageUrl = item.image ?? ""
            fullname = item.fullName
        }
    }
}
