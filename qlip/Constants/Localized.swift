// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
enum L10n {

  /// Update bundle if you need to change app language
  static var bundle: Bundle?

  /// Nama akun
  static var accountName: String {
    return L10n.tr("Localizable", "account_name")
  }
  /// Tulis nama akun
  static var accountNamePlaceholder: String {
    return L10n.tr("Localizable", "account_name_placeholder")
  }
  /// Yakin, akan melewatkan bagian ini?
  static var areYouSureToSkip: String {
    return L10n.tr("Localizable", "are_you_sure_to_skip")
  }
  /// Kembali
  static var back: String {
    return L10n.tr("Localizable", "back")
  }
  /// + Yuk, jadi yang pertama
  static var beTheFrist: String {
    return L10n.tr("Localizable", "be_the_frist")
  }
  /// Dengan masuk atau registrasi, %@ telah menyetujui Parental Consent, Syarat dan Ketentuan & Kebijakan Privasi
  static func byRegisterAgreeTerm(_ p1: String) -> String {
    return L10n.tr("Localizable", "by_register_agree_term", p1)
  }
  /// Batal
  static var cancel: String {
    return L10n.tr("Localizable", "cancel")
  }
  /// Pilih tanggal
  static var chooseDate: String {
    return L10n.tr("Localizable", "choose_date")
  }
  /// Pilih profilmu
  static var chooseYourProfile: String {
    return L10n.tr("Localizable", "choose_your_profile")
  }
  /// Pilih profil yang sesuai dengan identitas yang telah didaftarkan orang tuamu.
  static var chooseYourRegisteredParent: String {
    return L10n.tr("Localizable", "choose_your_registered_parent")
  }
  /// Hubungi kami, jika Anda butuh bantuan
  static var contactUsIfNeedHelp: String {
    return L10n.tr("Localizable", "contact_us_if_need_help")
  }
  /// Lanjutkan
  static var `continue`: String {
    return L10n.tr("Localizable", "continue")
  }
  /// Lanjutkan sebagai guest
  static var continueAsGuest: String {
    return L10n.tr("Localizable", "continue_as_guest")
  }
  /// Lanjutkan Menonton
  static var continueWhatching: String {
    return L10n.tr("Localizable", "continue_whatching")
  }
  /// Buat profile anak
  static var createKidProfile: String {
    return L10n.tr("Localizable", "create_kid_profile")
  }
  /// Untuk mendapatkan konten yang dipersonalisasi khusus untuk anak Anda, buat akun anak dulu yuk!
  static var createKidProfileDescription: String {
    return L10n.tr("Localizable", "create_kid_profile_description")
  }
  /// Hai Qliparents!\nAnda telah berada di tempat yang tepat
  static var createKidProfileTitle: String {
    return L10n.tr("Localizable", "create_kid_profile_title")
  }
  /// DM Instagram
  static var dmInstagram: String {
    return L10n.tr("Localizable", "dm_instagram")
  }
  /// Masuk sebagai Anak
  static var enterAsKid: String {
    return L10n.tr("Localizable", "enter_as_kid")
  }
  /// Masuk sebagai Orang Tua
  static var enterAsParent: String {
    return L10n.tr("Localizable", "enter_as_parent")
  }
  /// Masukkan no HP Anda yang terdaftar di Qlip, kami akan memandu Anda untuk proses atur ulang password
  static var forgotPassDesc: String {
    return L10n.tr("Localizable", "forgot_pass_desc")
  }
  /// Masukkan no HP-mu yang terdaftar di Qlip, kami akan memandumu untuk proses atur ulang password
  static var forgotPassDescKid: String {
    return L10n.tr("Localizable", "forgot_pass_desc_kid")
  }
  /// Atur ulang password
  static var forgotPassword: String {
    return L10n.tr("Localizable", "forgot_password")
  }
  /// Lupa Password?
  static var forgotPasswordQuestion: String {
    return L10n.tr("Localizable", "forgot_password_question")
  }
  /// Saya belum menerima OTP. Kirim ulang kode
  static var haveNotReceiveCode: String {
    return L10n.tr("Localizable", "have_not_receive_code")
  }
  /// Setelah masuk, Anda dapat menambahkan akun anak lainnya pada menu Profil
  static var infoCreateKid: String {
    return L10n.tr("Localizable", "info_create_kid")
  }
  /// Masukkan salah satu nama anak
  static var insertOneOfYourKid: String {
    return L10n.tr("Localizable", "insert_one_of_your_kid")
  }
  /// Daftar dengan nomor HP-mu, pastikan aktif dan dapat menerima SMS
  static var insertPhoneKidDescription: String {
    return L10n.tr("Localizable", "insert_phone_kid_description")
  }
  /// Masukkan nomor HP
  static var insertPhoneNumber: String {
    return L10n.tr("Localizable", "insert_phone_number")
  }
  /// Masukkan nomor HP kamu
  static var insertPhoneNumberKid: String {
    return L10n.tr("Localizable", "insert_phone_number_kid")
  }
  /// Daftar dengan nomor HP Anda, pastikan aktif dan dapat menerima SMS
  static var insertPhoneParentDescription: String {
    return L10n.tr("Localizable", "insert_phone_parent_description")
  }
  /// Masukkan nomor HP orang tua kamu yang telah terdaftar di Qlip
  static var insertYourParentNumberDescription: String {
    return L10n.tr("Localizable", "insert_your_parent_number_description")
  }
  /// Umur anak harus diantara %d - %d tahun
  static func kidAgeValidation(_ p1: Int, _ p2: Int) -> String {
    return L10n.tr("Localizable", "kid_age_validation", p1, p2)
  }
  /// Tanggal lahir anak
  static var kidDob: String {
    return L10n.tr("Localizable", "kid_dob")
  }
  /// Karena kamu belum punya nomor HP, kamu bisa isi nama akun kamu dan nomor hp orang tuamu yang terdaftar di Qlip.
  static var kidForgotPasswordDesc: String {
    return L10n.tr("Localizable", "kid_forgot_password_desc")
  }
  /// Masukkan nomor hp orang tua
  static var kidInsertParentPhone: String {
    return L10n.tr("Localizable", "kid_insert_parent_phone")
  }
  /// Nama anak
  static var kidName: String {
    return L10n.tr("Localizable", "kid_name")
  }
  /// Kamu akan melewatkan personalisasi konten khusus buatmu
  static var kidWillSkipPersonalization: String {
    return L10n.tr("Localizable", "kid_will_skip_personalization")
  }
  /// Lanjut
  static var lanjut: String {
    return L10n.tr("Localizable", "lanjut")
  }
  /// Lanjutkan
  static var lanjutkan: String {
    return L10n.tr("Localizable", "lanjutkan")
  }
  /// Masuk
  static var login: String {
    return L10n.tr("Localizable", "login")
  }
  /// Silakan masuk dengan nomor HP-mu yang telah terdaftar
  static var loginKidDescription: String {
    return L10n.tr("Localizable", "login_kid_description")
  }
  /// Silakan masuk dengan nomor HP Anda yang telah terdaftar
  static var loginParentDescription: String {
    return L10n.tr("Localizable", "login_parent_description")
  }
  /// Masuk ke akun Qlip
  static var loginTitle: String {
    return L10n.tr("Localizable", "login_title")
  }
  /// Agar akun Anda aman, buat password baru sesuai ketentuan
  static var makeNewPassDesc: String {
    return L10n.tr("Localizable", "make_new_pass_desc")
  }
  /// Buat password baru
  static var makeNewPassword: String {
    return L10n.tr("Localizable", "make_new_password")
  }
  /// Berhasil memperbarui password
  static var newPasswordSuccess: String {
    return L10n.tr("Localizable", "new_password_success")
  }
  /// Terbaru
  static var newestOption: String {
    return L10n.tr("Localizable", "newest_option")
  }
  /// Level %d
  static func numberLevel(_ p1: Int) -> String {
    return L10n.tr("Localizable", "number_level", p1)
  }
  /// + %d submisi lainnya
  static func numberOtherSubmission(_ p1: Int) -> String {
    return L10n.tr("Localizable", "number_other_submission", p1)
  }
  /// %d Qoin
  static func numberQoin(_ p1: Int) -> String {
    return L10n.tr("Localizable", "number_qoin", p1)
  }
  /// Atau masuk dengan
  static var orLoginWith: String {
    return L10n.tr("Localizable", "or_login_with")
  }
  /// Atau registrasi dengan
  static var orRegistrationWith: String {
    return L10n.tr("Localizable", "or_registration_with")
  }
  /// Aktivitas lainnya
  static var otherActivity: String {
    return L10n.tr("Localizable", "other_activity")
  }
  /// Kemampuan lainnya
  static var otherSkill: String {
    return L10n.tr("Localizable", "other_skill")
  }
  /// OTP
  static var otp: String {
    return L10n.tr("Localizable", "otp")
  }
  /// Kode OTP
  static var otpCode: String {
    return L10n.tr("Localizable", "otp_code")
  }
  /// Silakan masukkan 4 digit kode OTP yang telah kami kirimkan ke 
  static var otpDesccriptions: String {
    return L10n.tr("Localizable", "otp_desccriptions")
  }
  /// Hi, Qlip!\nBantu saya dong, saya tidak menerima OTP.\nNomor HP saya: +62%@ \nThank you.
  static func otpEmailBody(_ p1: String) -> String {
    return L10n.tr("Localizable", "otp_email_body", p1)
  }
  /// Saya Tidak Menerima OTP
  static var otpEmailSubject: String {
    return L10n.tr("Localizable", "otp_email_subject")
  }
  /// Parental Consent
  static var parentConsent: String {
    return L10n.tr("Localizable", "parent_consent")
  }
  /// Nomor HP orang tua
  static var parentPhoneNumber: String {
    return L10n.tr("Localizable", "parent_phone_number")
  }
  /// Anda akan melewatkan personalisasi konten yang dibuat khusus untuk anak Anda
  static var parentWillSkipPersonalization: String {
    return L10n.tr("Localizable", "parent_will_skip_personalization")
  }
  /// Password
  static var password: String {
    return L10n.tr("Localizable", "password")
  }
  /// 1 huruf kapital atau angka
  static var passwordCapitalValidation: String {
    return L10n.tr("Localizable", "password_capital_validation")
  }
  /// Minimal terdiri dari %d karakter
  static func passwordLengthValidation(_ p1: Int) -> String {
    return L10n.tr("Localizable", "password_length_validation", p1)
  }
  /// Masukkan password
  static var passwordLoginPlaceholder: String {
    return L10n.tr("Localizable", "password_login_placeholder")
  }
  /// Buat password
  static var passwordRegisterPlaceholder: String {
    return L10n.tr("Localizable", "password_register_placeholder")
  }
  /// Nomor HP
  static var phoneNumber: String {
    return L10n.tr("Localizable", "phone_number")
  }
  /// Nomor hp atau nama akun
  static var phoneOrUsername: String {
    return L10n.tr("Localizable", "phone_or_username")
  }
  /// Masukkan No HP atau nama akun
  static var phoneOrUsernamePlaceholder: String {
    return L10n.tr("Localizable", "phone_or_username_placeholder")
  }
  /// Terpopuler
  static var popularOption: String {
    return L10n.tr("Localizable", "popular_option")
  }
  /// Kebijakan Privasi
  static var privacyPolicy: String {
    return L10n.tr("Localizable", "privacy_policy")
  }
  /// Sebelum menikmati berbagai konten dari Qlip, registrasikan akun dulu ya
  static var registerDescription: String {
    return L10n.tr("Localizable", "register_description")
  }
  /// Buat akun dulu, yuk!
  static var registerTitle: String {
    return L10n.tr("Localizable", "register_title")
  }
  /// Registrasi
  static var registration: String {
    return L10n.tr("Localizable", "registration")
  }
  /// Kirim ulang kode
  static var resendCode: String {
    return L10n.tr("Localizable", "resend_code")
  }
  /// Kirim ulang kode OTP dalam 
  static var resendCodeIn: String {
    return L10n.tr("Localizable", "resend_code_in")
  }
  /// Simpan
  static var save: String {
    return L10n.tr("Localizable", "save")
  }
  /// Menyimpan
  static var saving: String {
    return L10n.tr("Localizable", "saving")
  }
  /// Lebih Detail
  static var seeDetail: String {
    return L10n.tr("Localizable", "see_detail")
  }
  /// Sebelum kenalan lebih jauh, beritahu minimal 3 topik yang kamu sukai.
  static var selectInterestFromKidDesc: String {
    return L10n.tr("Localizable", "select_interest_from_kid_desc")
  }
  /// Hai %@, yuk beraktivitas bareng Qlip! Pilih topik favoritmu dulu
  static func selectInterestFromKidTitle(_ p1: String) -> String {
    return L10n.tr("Localizable", "select_interest_from_kid_title", p1)
  }
  /// Anda dapat memilih lebih dari 1 aktivitas yang sering dilakukan dan disukai anak Anda.
  static var selectInterestFromParentDesc: String {
    return L10n.tr("Localizable", "select_interest_from_parent_desc")
  }
  /// Aktivitas apa yang disukai oleh %@?
  static func selectInterestFromParentTitle(_ p1: String) -> String {
    return L10n.tr("Localizable", "select_interest_from_parent_title", p1)
  }
  /// Kamu dan Qlip akan bareng-bareng mengasah kemampuan di berbagai bidang.
  static var selectSkillFromKidDesc: String {
    return L10n.tr("Localizable", "select_skill_from_kid_desc")
  }
  /// Kamu pengen lebih jago di bidang apa sih?
  static var selectSkillFromKidTitle: String {
    return L10n.tr("Localizable", "select_skill_from_kid_title")
  }
  /// Apa yang bisa Qlip bantu untuk mengoptimalkan soft skill anak Anda?
  static var selectSkillFromParentDesc: String {
    return L10n.tr("Localizable", "select_skill_from_parent_desc")
  }
  /// Kemampuan yang ingin ditingkatkan dari %@
  static func selectSkillFromParentTitle(_ p1: String) -> String {
    return L10n.tr("Localizable", "select_skill_from_parent_title", p1)
  }
  /// Kirim email
  static var sendEmail: String {
    return L10n.tr("Localizable", "send_email")
  }
  /// Video tidak dapat dibagikan
  static var shareVideoValidation: String {
    return L10n.tr("Localizable", "share_video_validation")
  }
  /// Lewati
  static var skip: String {
    return L10n.tr("Localizable", "skip")
  }
  /// Lewati jika kamu belum punya nomor HP
  static var skipDontHavePhone: String {
    return L10n.tr("Localizable", "skip_dont_have_phone")
  }
  /// Lewati, saya belum butuh
  static var skipDontNeedYet: String {
    return L10n.tr("Localizable", "skip_dont_need_yet")
  }
  /// Karakter spesial tidak diperbolehkan
  static var specialCharNotAllowrd: String {
    return L10n.tr("Localizable", "special_char_not_allowrd")
  }
  /// Submisi
  static var submission: String {
    return L10n.tr("Localizable", "submission")
  }
  /// Berhasil menyimpan video
  static var successBookmark: String {
    return L10n.tr("Localizable", "success_bookmark")
  }
  /// Akun
  static var tabAccount: String {
    return L10n.tr("Localizable", "tabAccount")
  }
  /// Eksplorasi
  static var tabExplore: String {
    return L10n.tr("Localizable", "tabExplore")
  }
  /// Home
  static var tabHome: String {
    return L10n.tr("Localizable", "tabHome")
  }
  /// Pesan
  static var tabInbox: String {
    return L10n.tr("Localizable", "tabInbox")
  }
  /// Virtual Camp
  static var tabVirtualCamp: String {
    return L10n.tr("Localizable", "tabVirtualCamp")
  }
  /// Syarat dan Ketentuan
  static var termsAndConditions: String {
    return L10n.tr("Localizable", "terms_and_conditions")
  }
  /// Upload Video Kamu
  static var uploadYourVideo: String {
    return L10n.tr("Localizable", "upload_your_video")
  }
  /// Gunakan tanda koma (,) apabila mengisi lebih dari satu.
  static var useComaDescription: String {
    return L10n.tr("Localizable", "use_coma_description")
  }
  /// Maaf, username sudah digunakan
  static var usernameCheckValidation: String {
    return L10n.tr("Localizable", "username_check_validation")
  }
  /// Video Challenge
  static var videoChallenge: String {
    return L10n.tr("Localizable", "video_challenge")
  }
  /// Berkarya dan berekspresi untuk mengembangkan berbagai soft skill. Yuk, mulai beraktivitas bareng!
  static var welcomeKidDescription: String {
    return L10n.tr("Localizable", "welcome_kid_description")
  }
  /// Berkarya dan berekspresi untuk mengembangkan berbagai soft skill anak Anda.
  static var welcomeParentDescription: String {
    return L10n.tr("Localizable", "welcome_parent_description")
  }
  /// Selamat Datang di Qlip!
  static var welcomeToQlip: String {
    return L10n.tr("Localizable", "welcome_to_qlip")
  }
  /// Spasi tidak diperbolehkan
  static var whitespaceNotAllowed: String {
    return L10n.tr("Localizable", "whitespace_not_allowed")
  }
  /// kamu
  static var youKid: String {
    return L10n.tr("Localizable", "you_kid")
  }
  /// anda
  static var youParent: String {
    return L10n.tr("Localizable", "you_parent")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
  static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let language = MultiLanguage.getLanguage().rawValue
    let searchBundle = Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj")!)!
    let format = NSLocalizedString(key, tableName: table, bundle: searchBundle, comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
