//
//  FontFamily.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

struct FontFamily {
    enum Avenir : String {
        case black              = "Avenir-Black"
        case book               = "Avenir-Book"
        case heavy              = "Avenir-Heavy"
        case medium             = "Avenir-Medium"
        case regular            = "Avenir-Regular"

        func font(size: CGFloat) -> UIFont {
            return UIFont(name: self.rawValue, size: size)!
        }
    }
}
