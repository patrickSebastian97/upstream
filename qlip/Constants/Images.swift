// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal enum Authentication {

    internal static let appleLogoBlack = ImageAsset(name: "Authentication/AppleLogoBlack")
    internal static let appleLogoSquare = ImageAsset(name: "Authentication/AppleLogoSquare")
    internal static let appleLogoWhite = ImageAsset(name: "Authentication/AppleLogoWhite")
    internal static let arrowBlue = ImageAsset(name: "Authentication/ArrowBlue")
    internal static let calendar = ImageAsset(name: "Authentication/Calendar")
    internal static let compass = ImageAsset(name: "Authentication/Compass")
    internal static let facebookLogo = ImageAsset(name: "Authentication/FacebookLogo")
    internal static let googleLogo = ImageAsset(name: "Authentication/GoogleLogo")
    internal static let passwordShow = ImageAsset(name: "Authentication/PasswordShow")
    internal static let welcome = ImageAsset(name: "Authentication/Welcome")
    internal static let passwordHide = ImageAsset(name: "Authentication/passwordHide")
  }
  internal enum Color {

    internal static let black = ColorAsset(name: "Color/Black")
    internal static let blackAlpha20 = ColorAsset(name: "Color/BlackAlpha20")
    internal static let blackAlpha30 = ColorAsset(name: "Color/BlackAlpha30")
    internal static let blackAlpha50 = ColorAsset(name: "Color/BlackAlpha50")
    internal static let blackAlpha70 = ColorAsset(name: "Color/BlackAlpha70")
    internal static let blue = ColorAsset(name: "Color/Blue")
    internal static let grayBackground = ColorAsset(name: "Color/GrayBackground")
    internal static let green = ColorAsset(name: "Color/Green")
    internal static let red = ColorAsset(name: "Color/Red")
    internal static let redAlpha3 = ColorAsset(name: "Color/RedAlpha3")
    internal static let skyBlue = ColorAsset(name: "Color/SkyBlue")
    internal static let skyBlueAlpha3 = ColorAsset(name: "Color/SkyBlueAlpha3")
    internal static let white = ColorAsset(name: "Color/White")
    internal static let whiteAlpha70 = ColorAsset(name: "Color/WhiteAlpha70")
    internal static let yellow = ColorAsset(name: "Color/Yellow")
  }
  internal enum General {

    internal static let arrowDownButton = ImageAsset(name: "General/ArrowDownButton")
    internal static let arrowUpButton = ImageAsset(name: "General/ArrowUpButton")
    internal static let backButton = ImageAsset(name: "General/BackButton")
    internal static let bookmarkActive = ImageAsset(name: "General/BookmarkActive")
    internal static let bookmarkInactive = ImageAsset(name: "General/BookmarkInactive")
    internal static let correct = ImageAsset(name: "General/Correct")
    internal static let email = ImageAsset(name: "General/Email")
    internal static let experienceIcon = ImageAsset(name: "General/ExperienceIcon")
    internal static let info = ImageAsset(name: "General/Info")
    internal static let instagram = ImageAsset(name: "General/Instagram")
    internal static let likeActiveChallenge = ImageAsset(name: "General/LikeActiveChallenge")
    internal static let likeActiveHome = ImageAsset(name: "General/LikeActiveHome")
    internal static let likeInactiveChallenge = ImageAsset(name: "General/LikeInactiveChallenge")
    internal static let likeInactiveHome = ImageAsset(name: "General/LikeInactiveHome")
    internal static let loadingSetup = ImageAsset(name: "General/LoadingSetup")
    internal static let logoWhite = ImageAsset(name: "General/LogoWhite")
    internal static let moneyIcon = ImageAsset(name: "General/MoneyIcon")
    internal static let nextButton = ImageAsset(name: "General/NextButton")
    internal static let playButton = ImageAsset(name: "General/PlayButton")
    internal static let soundDisable = ImageAsset(name: "General/SoundDisable")
    internal static let soundEnable = ImageAsset(name: "General/SoundEnable")
    internal static let warning = ImageAsset(name: "General/Warning")
    internal static let share = ImageAsset(name: "General/share")
  }
  internal enum Main {

    internal static let account = ImageAsset(name: "Main/Account")
    internal static let explore = ImageAsset(name: "Main/Explore")
    internal static let home = ImageAsset(name: "Main/Home")
    internal static let inbox = ImageAsset(name: "Main/Inbox")
    internal static let virtualCamp = ImageAsset(name: "Main/VirtualCamp")
  }
  internal enum SplashScreen {

    internal static let splashscreen = ImageAsset(name: "SplashScreen/Splashscreen")
  }
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

internal extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct DataAsset {
  internal fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(OSX)
  @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
  internal var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(OSX)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
internal extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(OSX)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: AssetImageTypeAlias {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = AssetImageTypeAlias(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal extension AssetImageTypeAlias {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
