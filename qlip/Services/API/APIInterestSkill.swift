//
//  APIInterestSkill.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 02/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import PromiseKit

extension APIService {
    enum Skill {
        static func getAll() -> Promise<ResponseDataListInterestSkillTagModel> {

            return APIService.request(
                path: "/app/skill",
                method: .get,
                parameters: nil,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
        static func assignSkill(
            kidId: String,
            skills: [String],
            customSkill: String? = nil
        ) -> Promise<ResponseMetaModel> {
            var params : [String: Any] = [
                "kid_id": kidId,
                "skill_id": skills
            ]
            if let customSkill = customSkill {
                params["skill_name"] = customSkill
            }

            return APIService.request(
                path: "/app/kids-skill",
                method: .post,
                parameters: params,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
        static func addSkill(
            name: String,
            descriptions: String? = nil,
            image: String? = nil
        ) -> Promise<ResponseMetaModel> {
            let params = [
                "name": name,
                "descriptions": descriptions ?? "",
                "image": image ?? ""
            ]

            return APIService.request(
                path: "/app/skill",
                method: .post,
                parameters: params,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
    }

    enum Interest {
        static func getAll() -> Promise<ResponseDataListInterestSkillTagModel> {

            return APIService.request(
                path: "/app/interest",
                method: .get,
                parameters: nil,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
        static func assignInterest(
            kidId: String,
            interests: [String],
            customInterest: String? = nil
        ) -> Promise<ResponseMetaModel> {
            var params: [String: Any] = [
                "kid_id": kidId,
                "interest_id": interests
            ]
            if let customInterest = customInterest {
                params["interest_name"] = customInterest
            }

            return APIService.request(
                path: "/app/kids-interest",
                method: .post,
                parameters: params,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
        static func addInterest(
            name: String,
            descriptions: String? = nil,
            image: String? = nil
        ) -> Promise<ResponseMetaModel> {
            let params = [
                "name": name,
                "descriptions": descriptions ?? "",
                "image": image ?? ""
            ]

            return APIService.request(
                path: "/app/interest",
                method: .post,
                parameters: params,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
    }
}
