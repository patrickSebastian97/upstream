//
//  APITypealias.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

typealias ResponselTokenModel = ResponseModel<TokenModel>
typealias ResponseDetailLegalModel = ResponseDataDetail<LegalModel>
typealias ResponseDetailKidRealmModel = ResponseDataDetail<KidRealmModel>
typealias ResponseDetailParentRealmModel = ResponseDataDetail<ParentRealmModel>
typealias ResponseDataListKidRealmModel = ResponseDataList<KidRealmModel>
typealias ResponseDataListInterestSkillTagModel = ResponseDataList<InterestSkillTagModel>
typealias ResponseDetailOTPVerifyModel = ResponseDataDetail<OTPVerifyModel>
typealias ResponseDataListFeedModel = ResponseDataList<VideoChallengeModel>
typealias ResponseDataDetailChallengeDetailModel = ResponseDataDetail<ChallengeDetailModel>
