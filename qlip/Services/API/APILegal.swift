//
//  APILegal.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation
import PromiseKit

extension APIService {
    enum Legal {
        static func getLegal(type: LegalType) -> Promise<ResponseDetailLegalModel> {
            var path = ""
            switch type {
            case .parentConsent:
                path = "/content/legal/parent-consent"
            case .termCondition:
                path = "/content/legal/term-condition"
            case .privacyPolicy:
                path = "/content/legal/privacy-policy"
            case .aboutUs:
                path = "/content/legal/about-us"
            }

            return APIService.request(
                path: path,
                method: .get,
                parameters: nil,
                headers: .oAuth
            )
        }
    }
}
