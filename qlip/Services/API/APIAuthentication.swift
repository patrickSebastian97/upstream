//
//  APIAuthentication.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import PromiseKit

extension APIService {
    enum Authentication {
        static func requestToken() -> Promise<ResponselTokenModel> {
            let params = [
                "grant_type": APISettingAuth.grantType,
                "username": APISettingAuth.username,
                "password": APISettingAuth.password
            ]

            return APIService.request(
                path: "/oauth/token",
                method: .post,
                parameters: params,
                headers: .custom(
                    headers: [NetworkService.HTTP.contentType: NetworkService.HTTP.applicationJSON])
            )
        }
        static func loginParent(
            id: String,
            password: String,
            token: String) -> Promise<ResponseDetailParentRealmModel> {
            let params = [
                "country_id": APIConstants.CountryCode.id,
                "id": id,
                "password": password,
                "registration_id": token
            ]

            return APIService.request(
                path: "/v2.0/parent-auth/login",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func loginKid(
            id: String,
            password: String,
            token: String) -> Promise<ResponseDetailKidRealmModel> {
            let params = [
                "country_id": APIConstants.CountryCode.id,
                "id": id,
                "password": password,
                "registration_id": token
            ]

            return APIService.request(
                path: "/v2.0/kid-auth/login",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func requestOTP(
            phone: String,
            type: String,
            username: String? = nil) -> Promise<ResponseMetaModel> {
            var params : [String:Any] = [
                "country_id": APIConstants.CountryCode.id,
                "phone": phone,
                "type": type
            ]
            if let username = username {
                            params["username"] = username
                        }

            return APIService.request(
                path: "/otp/request",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func verifyOTP(
            phone: String,
            otp: String,
            type: String,
            username: String? = nil) -> Promise<ResponseDetailOTPVerifyModel> {
            var params : [String:Any] = [
                "country_id": APIConstants.CountryCode.id,
                "phone": phone,
                "otp": otp,
                "type": type
            ]
            if let username = username {
                params["username"] = username
            }
            return APIService.request(
                path: "/otp/verify",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func registerParent(
            parentData: UserDataManager.Data?,
            token: String) -> Promise<ResponseDetailParentRealmModel> {
            let params: [String: Any] = [
                "username" : parentData?.username ?? "",
                "phone": parentData?.phone ?? "",
                "country_id": APIConstants.CountryCode.id,
                "debug": APIConstants.Settings.enableDebugging,
                "otp": parentData?.otp ?? "",
                "password": parentData?.password ?? "",
                "registration_id": token
            ]

            return APIService.request(
                path: "/v2.0/parent",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func checkUsernamePassword(
            username: String,
            password: String) -> Promise<ResponseMetaModel> {
            let params = [
                "username": username,
                "password": password
            ]

            return APIService.request(
                path: "/app/check-username-password",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func createNewPassword(
            phone: String,
            token: String,
            password: String,
            confirmPassword: String
            ) -> Promise<ResponseMetaModel> {
            let params = [
                "country_id" : APIConstants.CountryCode.id,
                "phone": phone,
                "token": token,
                "password": password,
                "confirm_password": confirmPassword
            ]

            return APIService.request(
                path: "/parent-auth/reset-password",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func parentSocialMediaSignIn(
            providerType: String,
            providerToken: String,
            registrationId: String,
            firstname: String? = nil,
            lastname: String? = nil) -> Promise<ResponseDetailParentRealmModel> {
            var params: [String:Any] = [
                "provider_type": providerType,
                "provider_token": providerToken,
                "registration_id": registrationId
            ]
            if let firstname = firstname, let lastname = lastname {
                params["first_name"] = firstname
                params["last_name"] = lastname
            }

            return APIService.request(
                path: "/v2.0/parent-auth/social-login",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func kidSocialMediaSignIn(
            providerType: String,
            providerToken: String,
            registrationId: String,
            firstname: String? = nil,
            lastname: String? = nil) -> Promise<ResponseDetailKidRealmModel> {
            var params: [String:Any] = [
                "provider_type": providerType,
                "provider_token": providerToken,
                "registration_id": registrationId
            ]
            if let firstname = firstname, let lastname = lastname {
                params["first_name"] = firstname
                params["last_name"] = lastname
            }

            return APIService.request(
                path: "/v2.0/kid-auth/social-login",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func checkParentPhoneNumber(
            parentPhone: String
        ) -> Promise<ResponseDataListKidRealmModel> {
            let params = [
                "country_id": APIConstants.CountryCode.id,
                "phone": parentPhone
            ]

            return APIService.request(
                path: "/kid/check-parent-phone",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func registerAsKid(
            userId: String,
            phone: String? = nil
        ) -> Promise<ResponseDetailKidRealmModel> {
            var params = [
                "username": UserDataManager.shared.getData()!.username!,
                "country_id": APIConstants.CountryCode.id,
                "user_id": userId,
                "password": UserDataManager.shared.getData()!.password!,
                "registration_id": Firebase.shared.getFCMToken() ?? "firebase_id"
            ]
            if let phone = phone {
                params["phone"] = phone
            }

            return APIService.request(
                path: "/kid/register",
                method: .post,
                parameters: params as [String : Any],
                headers: .oAuth
            )
        }
        static func registerAsKidBySocialMedia(
            userId: String,
            phone: String? = nil) -> Promise<ResponseDetailKidRealmModel> {
            var params = [
                "username": UserDataManager.shared.getData()!.username!,
                "country_id": APIConstants.CountryCode.id,
                "user_id": userId,
                "provider_type": UserDataManager.shared.getData()!.providerType!,
                "provider_token": UserDataManager.shared.getData()!.providerToken!,
                "provider_id": UserDataManager.shared.getData()!.providerId!,
                "registration_id": Firebase.shared.getFCMToken() ?? "firebase_id"
            ]
            if let phone = phone {
                params["phone"] = phone
            }

            return APIService.request(
                path: "/kid/register",
                method: .post,
                parameters: params as [String : Any],
                headers: .oAuth
            )
        }
        static func checkKidPhoneNumber(
            kidPhone: String
        ) -> Promise<ResponseMetaModel> {
            let params = [
                "country_id": APIConstants.CountryCode.id,
                "phone": kidPhone
            ]

            return APIService.request(
                path: "/kid/check-phone-register",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
        static func createNewPasswordKid(
            username: String? = nil,
            phone: String? = nil,
            token: String,
            password: String,
            confirmPassword: String
            ) -> Promise<ResponseMetaModel> {
            var params = [
                "country_id" : APIConstants.CountryCode.id,
                "token": token,
                "password": password,
                "confirm_password": confirmPassword
            ]
            if let username = username {
                params["username"] = username
            }
            if let phone = phone {
                params["phone"] =  phone
            }
            return APIService.request(
                path: "/kid-auth/reset-password",
                method: .post,
                parameters: params,
                headers: .oAuth
            )
        }
    }
}
