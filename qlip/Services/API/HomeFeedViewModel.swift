//
//  HomeFeedViewModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

class HomeFeedViewModel: ViewModel<VideoChallengeModel> {
    var id: String!
    var submissionId: String!
    var name: String!
    var image: String!
    var duration: Int!
    var submissionList: [SubmissionModel] = []
    var totalQoin: Int!
    var totalExp: Int!
    var totalReaction: Int!
    var isLiked: Bool!
    var creatorName: String!
    var totalSubmission: Int!

    override var item: VideoChallengeModel! {
        didSet {
            id = item.id ?? ""
            submissionId = item.submissionId ?? ""
            name = item.name ?? ""
            image = item.image ?? ""
            duration = item.duration ?? 0
            submissionList = item.submission
            totalQoin = item.qoin ?? 0
            totalExp = item.totalExp ?? 0
            totalReaction = item.totalReaction ?? 0
            isLiked = item.isLiked ?? false
            creatorName = item.contentCreator?.name ?? ""
            totalSubmission = item.totalSubmission ?? 0
        }
    }
}
