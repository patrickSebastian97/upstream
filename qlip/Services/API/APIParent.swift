//
//  APIParent.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 29/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation
import PromiseKit

extension APIService {
    enum Parent {
        static func getUser() -> Promise<ResponseDetailParentRealmModel> {

            return APIService.request(
                path: "/parent/me",
                method: .get,
                parameters: nil,
                headers: .parentAuth
            )
        }
        static func createKid(
            firstName: String,
            lastName: String,
            dob: String) -> Promise<ResponseDetailKidRealmModel> {
            let params = [
                "first_name" : firstName,
                "last_name": lastName,
                "dob": dob
            ]

            return APIService.request(
                path: "/v2.0/parent/add-kid",
                method: .post,
                parameters: params,
                headers: .parentAuth
            )
        }
        static func updatePhoneNumber(
            phone: String,
            otp: String) -> Promise<ResponseMetaModel> {
            let params = [
                "country_id" : APIConstants.CountryCode.id,
                "phone" : phone,
                "otp" : otp
            ]

            return APIService.request(
                path: "/parent/update-phone",
                method: .post,
                parameters: params,
                headers: .parentAuth
            )
        }
    }
}
