//
//  APIFirebase.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 21/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation
import PromiseKit

extension APIService {
    enum FirebaseAPI {
        static func registerFirebaseToken(
            token: String,
            isKidUseShareDevice: Bool) -> Promise<ResponseMetaModel> {
            let params: [String: Any] = [
                "registration_id": token,
                "kid_use_shared_device": isKidUseShareDevice
            ]

            return APIService.request(
                path: "/register-firebase-token",
                method: .post,
                parameters: params,
                headers: ParentRealmModel.isLoggedIn() ? .parentAuth : .kidAuth
            )
        }
    }
}
