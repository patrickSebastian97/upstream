//
//  APIReaction.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation
import PromiseKit

extension APIService {
    enum Reaction {
        static func postReaction(submissionId: String) -> Promise<ResponseMetaModel> {

            let params = [
                "submission_id": submissionId,
                "reaction_id": APIConstants.Reaction.likeId
            ]
            return APIService.request(
                path: "/app/reaction/submission",
                method: .post,
                parameters: params,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
        static func deleteReaction(submissionId: String) -> Promise<ResponseMetaModel> {

            return APIService.request(
                path: "/app/reaction/submission/\(submissionId)",
                method: .delete,
                parameters: nil,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
    }
}
