//
//  ChallengeDetailViewModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

class ChallengeDetailViewModel: ViewModel<ChallengeDetailModel> {
    var id: String!
    var submissionId: String!
    var title: String!
    var creatorImageUrl: String!
    var videoUrl: String!
    var creatorName: String!
    var dateCreate: String!
    var isBookmark: Bool!
    var totalCoin: String!
    var totalExp: String!
    var totalSubmission: String!
    var isLiked: Bool!
    var totalLike: String!

    override var item: ChallengeDetailModel! {
        didSet {
            let dateAPI = item.challenge?.publishDate?.date()
            id = item.challenge?.id ?? ""
            submissionId = item.submission?.id ?? ""
            title = item.challenge?.name ?? ""
            creatorImageUrl = item.challenge?.contentCreator?.image ?? ""
            videoUrl = item.challenge?.videoTranscoded?.file ?? ""
            creatorName = item.challenge?.contentCreator?.name ?? ""
            dateCreate = dateAPI?.shortDate ?? ""
            isBookmark = item.challenge?.isBookmark ?? false
            totalCoin = String(item.challenge?.totalExp ?? 0)
            totalExp = String(item.challenge?.totalExp ?? 0)
            totalSubmission = "\(String(item.challenge?.totalSubmission ?? 0)) \(L10n.submission)"
            isLiked = item.challenge?.isLiked ?? false
            totalLike = String(item.challenge?.totalReaction ?? 0)
        }
    }
}
