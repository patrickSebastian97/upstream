//
//  APIKid.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 31/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation
import PromiseKit

extension APIService {
    enum Kids {
        static func updatePhoneNumber(
            phone: String,
            otp: String) -> Promise<ResponseMetaModel> {
            let params = [
                "country_id" : APIConstants.CountryCode.id,
                "phone" : phone,
                "otp" : otp
            ]

            return APIService.request(
                path: "/kid/update-phone",
                method: .post,
                parameters: params,
                headers: .kidAuth
            )
        }
    }
}
