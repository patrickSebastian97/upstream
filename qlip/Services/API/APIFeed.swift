//
//  APIFeed.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation
import PromiseKit

extension APIService {

    // swiftlint:disable line_length
    enum Feed {
        static func getFeed(page: Int, sortedBy: String) -> Promise<ResponseDataListFeedModel> {

            var headers: Header = .oAuth
            if ParentRealmModel.isLoggedIn() {
                headers = .parentAuth
            } else if KidRealmModel.isLoggedIn() {
                headers = .kidAuth
            }

            return APIService.request(
                path: "/app/home/feed?limit=\(APIConstants.Pagination.limit)&page=\(page)&sort=\(sortedBy):asc",
                method: .get,
                parameters: nil,
                headers: headers
            )
        }
    }
}
