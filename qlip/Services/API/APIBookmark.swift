//
//  APIBookmark.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation
import PromiseKit

extension APIService {

    enum Bookmark {
        static func add(id: String) -> Promise<ResponseMetaModel> {
            let params = [
                "challenge_id": id
            ]

            return APIService.request(
                path: "/app/bookmark",
                method: .post,
                parameters: params,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
        static func delete(id: String) -> Promise<ResponseMetaModel> {

            return APIService.request(
                path: "/app/bookmark/\(id)",
                method: .delete,
                parameters: nil,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
    }
}
