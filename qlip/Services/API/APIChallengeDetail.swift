//
//  APIChallengeDetail.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation
import PromiseKit

extension APIService {

    enum Challenge {
        static func getDetail(id: String) -> Promise<ResponseDataDetailChallengeDetailModel> {

            return APIService.request(
                path: "/app/challenge/detail/\(id)",
                method: .get,
                parameters: nil,
                headers: KidRealmModel.isLoggedIn() ? .kidAuth : .parentAuth
            )
        }
    }
}
