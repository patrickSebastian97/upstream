//
//  ResponseModel.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

struct MetaModel: Codable {
    let code: Int
    let status: Bool
    let message: String
}

struct PaginationModel: Codable {
    let limit: Int
    let totalPage: Int
    let totalRows: Int
    let currentPage: Int
}

struct DetailModel<T: Codable>: Codable {
    let detail: T?
}

struct ListModel<T: Codable>: Codable {
    let list: [T]
    let pagination: PaginationModel?
}

struct DetailListModel<T: Codable, U: Codable>: Codable {
    let list: [T]
    let pagination: PaginationModel?
    let detail: U?
}

struct ResponseMetaModel: Codable {
    // MARK: Properties
    let meta: MetaModel?
}

/// Used specified for Response that wrapped in "detail" param
struct ResponseDataDetail<T: Codable>: Codable {
    // MARK: Properties

    let data: DetailModel<T>
    let meta: MetaModel?

    func dataModel() -> T? {
        return data.detail
    }
}

/// Used specified for Response that wrapped in "list" param
struct ResponseDataList<T: Codable>: Codable {
    // MARK: Properties

    let data: ListModel<T>
    let meta: MetaModel?

    func dataModel() -> [T] {
        return data.list
    }
}

struct ResponseDetailList<T: Codable, U: Codable>: Codable {
    let data: DetailListModel<T, U>
    let meta: MetaModel?

    func listDataModel() -> [T] {
        return data.list
    }

    func detailDataModel() -> U? {
        return data.detail
    }
}

struct ResponseModel<T: Codable>: Codable {

    // MARK: Properties

    let data: T?
    let meta: MetaModel

    func dataModel() -> T? {
        return data
    }
}
