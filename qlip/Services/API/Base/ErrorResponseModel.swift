//
//  ErrorResponseModel.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

struct GenericCodingKeys: CodingKey {
    var intValue: Int?
    var stringValue: String

    init?(intValue: Int) { self.intValue = intValue; self.stringValue = "\(intValue)" }
    init?(stringValue: String) { self.stringValue = stringValue }

    static func makeKey(name: String) -> GenericCodingKeys {
        return GenericCodingKeys(stringValue: name)!
    }
}

struct ErrorResponseModel: Codable, Error {
    // MARK: Properties
    let meta: MetaModel
}
