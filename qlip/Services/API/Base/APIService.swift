//
//  APIService.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation
import PromiseKit
import PMKFoundation
import SwiftyJSON

class APIService {

    // MARK: - Properties

    enum Header {
        case oAuth
        case parentAuth
        case kidAuth
        case custom(headers: [String: String])
    }

    static var enableLog: Bool = true
    static var logTag: String = "[API Service]"
    static var defaultErrorDomain: String = "Qlip"
    static var defaultErrorCode: Int = -1
    static var defaultErrorMessage: String = "Failed to decode response."

    static var baseURL: String = ""

    // MARK: - Private Functions

    static func log(_ items: Any) {
        if APIService.enableLog {
            print("\(APIService.logTag): \(items)")
        }
    }

    // MARK: - Helpers

    /// Create a request with path. If url is nil, then it will use a default baseUrl.
    /// Otherwise use url instead for request.
    // swiftlint:disable function_body_length
    static func request<T>(
        url: String? = nil,
        path: String,
        method: NetworkService.HTTPMethod,
        parameters: [String: Any]?,
        headers: Header
    ) -> Promise<T> where T: Decodable, T: Encodable {
        let completeUrl = createUrl(url: url, path: path)
        var headersItems: [String: String] = [:]
        switch headers {
        case .oAuth:
            headersItems = oAuthHeaders
        case .parentAuth:
            headersItems = parentHeaders
        case .kidAuth:
            headersItems = kidHeaders
        case .custom(let headers):
            headersItems = headers
        }

        APIService.log("Requesting url \(method): \(completeUrl)")
        APIService.log("Requesting params: \(parameters ?? [:])")
        APIService.log("Requesting headers: \(headersItems)")

        return Promise { seal in
            firstly {
                NetworkService.request(
                    url: completeUrl,
                    method: method,
                    parameters: parameters,
                    headers: headersItems
                )
            }.done { data in
                seal.resolve(.fulfilled(data))
            }.catch { error in
                APIService.log("Error url \(method): \(completeUrl)")
                APIService.log("Error error: \(error)")
                APIService.log("Error params: \(parameters ?? [:])")
                APIService.log("Error headers: \(headersItems)")
                if let httpError = error as? PMKHTTPError {
                    APIService.log("Error PMKHTTPError: \(httpError)")
                    // try to decode json
                    if let jsonDictionary = httpError.jsonDictionary {
                        let json = JSON(jsonDictionary)
                        APIService.log("Error JSON: \(json)")
                        switch httpError {
                        case .badStatusCode( let code, _, let response):
                            APIService.log("Error Response: \(response)")
                            APIService.log("Error code: \(code)")
                            if code == APIConstants.HTTPStatus.forbidden {
//                                AppHelper.forceLogoutKid()
//                                AppHelper.requesTokenIfNeeded(force: true)
                            }
                            let decoder = JSONDecoder()
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            do {
                                let errorResponseModel = try decoder.decode(
                                    ErrorResponseModel.self, from: json.rawData()
                                )
                                seal.reject(errorResponseModel)
                            } catch let errorDecode {
                                APIService.log("Error decode ErrorResponseModel: \(errorDecode)")
                                seal.reject(httpError)
                            }
                        }
                    } else {
                        seal.reject(httpError)
                    }
                } else {
                    // check if a decoding error
                    if let decodingError = (error as? DecodingError) {
                        APIService.log("Error DecodingError: \(decodingError)")
                        let decodeError = NSError(
                            domain: APIService.defaultErrorDomain,
                            code: APIService.defaultErrorCode,
                            userInfo: [NSLocalizedDescriptionKey: APIService.defaultErrorMessage]
                        )
                        seal.reject(decodeError)
                    } else {
                        seal.reject(error)
                    }
                }
            }
        }
    }
    // swiftlint:enable function_body_length

    // swiftlint:disable function_body_length
    // swiftlint:disable function_parameter_count
    // swiftlint:disable line_length
    /// Create a request with path. If url is nil, then it will use a default baseUrl. Otherwise use url instead for request.
    static func upload<T>(
        url: String? = nil,
        path: String,
        data: Data,
        id: String,
        descriptions: String,
        image: Data,
        paramName: String,
        headers: Header
    ) -> Promise<T> where T: Decodable, T: Encodable {
        let completeUrl = createUrl(url: url, path: path)

        return Promise { seal in
            var headersItems: [String: String] = [:]
            switch headers {
            case .oAuth:
                headersItems = oAuthHeaders
            case .parentAuth:
                headersItems = parentHeaders
            case .kidAuth:
                headersItems = kidHeaders
            case .custom(let headers):
                headersItems = headers
            }

            APIService.log("Requesting url: \(completeUrl)")
            APIService.log("Requesting headers: \(headersItems)")

            firstly {
                NetworkService.upload(
                    url: completeUrl,
                    data: data,
                    id: id,
                    descriptions: descriptions,
                    image: image,
                    paramName: paramName,
                    headers: headersItems)
            }.done { data in
                seal.resolve(.fulfilled(data))
            }.catch { error in
                APIService.log("Error upload url: \(completeUrl)")
                APIService.log("Error error: \(error)")
                APIService.log("Error headers: \(headersItems)")
                if let httpError = error as? PMKHTTPError {
                    APIService.log("Error PMKHTTPError: \(httpError)")
                    // try to decode json
                    if let jsonDictionary = httpError.jsonDictionary {
                        let json = JSON(jsonDictionary)
                        APIService.log("Error JSON: \(json)")
                        switch httpError {
                        case .badStatusCode( _, _, let response):
                            APIService.log("Error Response: \(response)")
                            let decoder = JSONDecoder()
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            do {
                                let errorResponseModel = try decoder.decode(
                                    ErrorResponseModel.self, from: json.rawData()
                                )
                                seal.reject(errorResponseModel)
                            } catch let errorDecode {
                                APIService.log("Error decode ErrorResponseModel: \(errorDecode)")
                                seal.reject(httpError)
                            }
                        }
                    } else {
                        seal.reject(httpError)
                    }
                } else {
                    // check if a decoding error
                    if let decodingError = (error as? DecodingError) {
                        APIService.log("Error DecodingError: \(decodingError)")
                        let decodeError = NSError(
                            domain: APIService.defaultErrorDomain,
                            code: APIService.defaultErrorCode,
                            userInfo: [NSLocalizedDescriptionKey: APIService.defaultErrorMessage]
                        )
                        seal.reject(decodeError)
                    } else {
                        seal.reject(error)
                    }
                }
            }
        }
    }
    // swiftlint:enable function_body_length

    static func createUrl(url: String? = nil, path: String) -> String {
        return (url == nil) ? "\(baseURL)\(path)" : "\(url ?? "")\(path)"
    }

    /// Get public oAuth Token
    private static var oAuthHeaders: [String: String] {
        let tokenType = TokenModel.Storage.getToken()?.tokenType ?? ""
        let accessToken = TokenModel.Storage.getToken()?.accessToken ?? ""
        let bearerToken = "\(tokenType) \(accessToken)"
        return [
            NetworkService.HTTP.contentType: NetworkService.HTTP.applicationJSON,
            NetworkService.HTTP.authorization: bearerToken,
            NetworkService.HTTP.acceptLanguage: MultiLanguage.getLanguage().rawValue
        ]
    }

    /// Get Parent token
    private static var parentHeaders: [String: String] {
        var bearerToken = ""

        if ParentRealmModel.getUser().isLoggedIn() {
            let user = ParentRealmModel.getUser()
            let tokenType = "\(user.tokenType ?? "")"
            let accessToken = "\(user.accessToken ?? "")"
            bearerToken = "\(tokenType) \(accessToken)"
        } else {
            let token = TokenModel.Storage.getToken()
            let tokenType = "\(token?.tokenType ?? "")"
            let accessToken = "\(token?.accessToken ?? "")"
            bearerToken = "\(tokenType) \(accessToken)"
        }

        return [
            NetworkService.HTTP.contentType: NetworkService.HTTP.applicationJSON,
            NetworkService.HTTP.authorization: bearerToken,
            NetworkService.HTTP.acceptLanguage: MultiLanguage.getLanguage().rawValue
        ]
    }

    /// Get Kid token
    private static var kidHeaders: [String: String] {
        var bearerToken = ""

        if KidRealmModel.getUser().isLoggedIn() {
            let user = KidRealmModel.getUser()
            let tokenType = "\(user.tokenType ?? "")"
            let accessToken = "\(user.accessToken ?? "")"
            bearerToken = "\(tokenType) \(accessToken)"
        } else {
            let token = TokenModel.Storage.getToken()
            let tokenType = "\(token?.tokenType ?? "")"
            let accessToken = "\(token?.accessToken ?? "")"
            bearerToken = "\(tokenType) \(accessToken)"
        }

        return [
            NetworkService.HTTP.contentType: NetworkService.HTTP.applicationJSON,
            NetworkService.HTTP.authorization: bearerToken,
            NetworkService.HTTP.acceptLanguage: MultiLanguage.getLanguage().rawValue
        ]
    }

}
