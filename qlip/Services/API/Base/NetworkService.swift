//
//  NetworkService.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation
import PromiseKit
import PMKFoundation
import SwiftyJSON

protocol NetworkServiceProtocol {
    static func request<T: Codable>(
        url: String,
        method: NetworkService.HTTPMethod,
        parameters: [String: Any]?,
        headers: [String: String]?
    ) -> Promise<T>

    //swiftlint:disable function_parameter_count
    static func upload<T: Codable>(
        url: String,
        data: Data,
        id: String,
        descriptions: String,
        image: Data,
        paramName: String,
        headers: [String : String]?
    ) -> Promise<T>
}

struct BuildUploadData {
    var data: Data
    var paramName: String
    var boundary: String
    var id: String
    var descriptions: String
    var image: Data
}

class NetworkService: NetworkServiceProtocol {
    // MARK: - Properties

    static var enableLog: Bool = true
    static var logTag: String = "[Network Service]"

    enum HTTPMethod: String {
        case post = "POST"
        case get = "GET"
        case put = "PUT"
        case delete = "DELETE"
        case upload = "UPLOAD"
    }

    enum HTTP {
        static let contentType = "Content-Type"
        static let applicationJSON = "application/json"
        static let authorization = "Authorization"
        static let formUrlEncoded = "application/x-www-form-urlencoded"
        static let connection = "Connection"
        static let keepAlive = "Keep-Alive"
        static let multipartFormData = "multipart/form-data"
        static let contentLength = "Content-Length"
        static let contenDispositionFormData = "Content-Disposition: form-data"
        static let accept = "Accept"
        static let acceptLanguage = "Accept-Language"
    }

    // MARK: - Private Functions

    static func log(_ items: Any) {
        if NetworkService.enableLog {
            print("\(NetworkService.logTag): \(items)")
        }
    }

    // MARK: - Network Service Protocol

    static func request<T>(
        url: String,
        method: NetworkService.HTTPMethod,
        parameters: [String: Any]?,
        headers: [String: String]?
    ) -> Promise<T> where T: Decodable, T: Encodable {
        return Promise { seal in
            firstly {
                URLSession.shared.dataTask(
                    .promise,
                    with: try makeUrlRequest(
                        url,
                        method: method,
                        parameters: parameters,
                        headers: headers
                    )
                ).validate()
                // ^^ we provide `.validate()` so that eg. 404s get converted to errors
            }.done { data in
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                do {
                    let objectDecoded = try decoder.decode(T.self, from: data.data)
                    seal.resolve(.fulfilled(objectDecoded))
                } catch let error {
                    seal.reject(error)
                }
                // seal.fulfill(data)
            }.catch { error in
                seal.reject(error)
            }
        }
    }

    static func upload<T>(
        url: String,
        data: Data,
        id: String,
        descriptions: String,
        image: Data,
        paramName: String,
        headers: [String : String]?
    ) -> Promise<T> where T : Decodable, T : Encodable {
        let boundary = UUID().uuidString
        let uploadData = BuildUploadData(
            data: data,
            paramName: paramName,
            boundary: boundary,
            id: id,
            descriptions: descriptions,
            image: image
        )
        return Promise { seal in
            firstly {
                URLSession.shared.uploadTask(
                    .promise,
                    with: try makeUploadUrlRequest(
                        url,
                        boundary: boundary,
                        contentLength: data.count,
                        headers: headers
                    ),
                    from: buildUploadData(multiPartData: uploadData)
                ).validate()
            }.done { data in
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                do {
                    let objectDecoded = try decoder.decode(T.self, from: data.data)
                    seal.resolve(.fulfilled(objectDecoded))
                } catch let error {
                    seal.reject(error)
                }
            }.catch { error in
                seal.reject(error)
            }
        }
    }
}

// MARK: - Helpers
extension NetworkService {
    static func makeUrlRequest(
        _ url: String,
        method: HTTPMethod,
        data: Data? = nil,
        parameters: [String: Any]?,
        headers: [String: String]?
    ) throws -> URLRequest {
        var components = URLComponents(string: url)!
        let isFormEncoded = !(headers?.filter({
            $0.key == HTTP.contentType && $0.value == HTTP.formUrlEncoded
        }) ?? []).isEmpty
        // Use query string for parameters for get method
        if let parameters = parameters {
            if method == .get || isFormEncoded {
                var queryItems: [URLQueryItem] = []
                parameters.keys.forEach { (key) in
                    if let value = parameters[key] {
                        queryItems.append(URLQueryItem(name: key, value: String(describing: value)))
                    }
                }
                components.queryItems = queryItems
                components.percentEncodedQuery =
                    components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
            }
        }
        let buildedHeaders = buildDefaultHeaders(headers: headers)

        var jsonData: Data?
        if let parameters = parameters {
            jsonData = try? JSONSerialization.data(withJSONObject: parameters as Any)
        }

        var request = URLRequest(url: components.url!)
        buildedHeaders.forEach { (headerKey, headerValue) in
            request.setValue(headerValue, forHTTPHeaderField: headerKey)
        }

        request.httpMethod = method.rawValue
        if isFormEncoded {
            request.httpBody = components.query?.data(using: .utf8)
        } else {
            if method != .get {
                request.httpBody = jsonData
            }
        }

        return request
    }

    static func makeUploadUrlRequest(
        _ url: String,
        boundary: String,
        contentLength: Int,
        headers: [String: String]?
    ) throws -> URLRequest {
        var buildedHeaders = buildDefaultHeaders(headers: headers)
        // Set Content-Type Header to multipart/form-data,
        // this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        let multipartData = "\(NetworkService.HTTP.multipartFormData); boundary=\(boundary)"
        buildedHeaders[NetworkService.HTTP.connection] = NetworkService.HTTP.keepAlive
        buildedHeaders[NetworkService.HTTP.contentType] = multipartData
        buildedHeaders[NetworkService.HTTP.contentLength] = "\(contentLength)"

        var request = URLRequest(url: URL(string: url)!)
        buildedHeaders.forEach { (headerKey, headerValue) in
            request.setValue(headerValue, forHTTPHeaderField: headerKey)
        }

        request.httpMethod = NetworkService.HTTPMethod.post.rawValue
//        request.httpBody = dataParams.map { String($0) }.joined(separator: "&").data(using: .utf8)

        return request
    }

    static func buildDefaultHeaders(headers: [String: String]?) -> [String: String] {
        if let headers = headers {
            var newHeaders: [String: String] = headers
            newHeaders[NetworkService.HTTP.contentType] = NetworkService.HTTP.applicationJSON

            return newHeaders
        } else {
            return [
                NetworkService.HTTP.contentType: NetworkService.HTTP.applicationJSON
            ]
        }
    }

    static func buildUploadData(
        multiPartData: BuildUploadData,
        filename: String = "File.mp4"
    ) -> Data {
        var httpData = Data()

        // Add the body data to the raw http request data
        let contentDisposition = NetworkService.HTTP.contenDispositionFormData
        let contentType = "\(NetworkService.HTTP.contentType): video/mp4\r\n\r\n"
        let contentTypeImage = "\(NetworkService.HTTP.contentType): image/jpg\r\n\r\n"
        let name = "name=\"\(multiPartData.paramName)\""
        let filenameData = "filename=\"\(filename)\"\r\n"
        let filenameImage = "filename=image.jpg\r\n"

        httpData.append("\r\n--\(multiPartData.boundary)\r\n".data(using: .utf8)!)
        httpData.append("\(contentDisposition); \(name); \(filenameData)".data(using: .utf8)!)
        httpData.append(contentType.data(using: .utf8)!)
        httpData.append(multiPartData.data)

        httpData.append("\r\n--\(multiPartData.boundary)\r\n".data(using: .utf8)!)
        httpData.append("\(contentDisposition); name=image; \(filenameImage)".data(using: .utf8)!)
        httpData.append(contentTypeImage.data(using: .utf8)!)
        httpData.append(multiPartData.image)

        let params: [String: Any] = ["challenge_id": multiPartData.id,
                                     "descriptions": "video"]
        for (key, value) in params {
            httpData.append("\r\n--\(multiPartData.boundary)\r\n".data(using: .utf8)!)
            httpData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
            httpData.append("\(value)".data(using: .utf8)!)
        }
        httpData.append("\r\n--\(multiPartData.boundary)--\r\n".data(using: .utf8)!)

        return httpData
    }
}
