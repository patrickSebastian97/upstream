//
//  AppNotificationCenter.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import AVFoundation

extension NSNotification.Name {
    static let appTokenRefreshed = Notification.Name("App_appTokenRefreshed")
    static let appPushNotificationPayload = Notification.Name("App_appPushNotificationPayload")
    static let appScrollToTop = Notification.Name("App_ScrollToTop")

}

protocol AppNotificationCenter: class {
    // put this on viewDidLoad
    func enableAppNotification()
    // put this on deinit
    func disableAppNotification()
}

extension AppNotificationCenter where Self: UIViewController {
    func enableAppNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appTokenRefreshed(notification:)),
            name: .appTokenRefreshed,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appPushNotificationPayload(notification:)),
            name: .appPushNotificationPayload,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appScrollToTop(notification:)),
            name: .appScrollToTop,
            object: nil
        )
    }

    func disableAppNotification() {
        NotificationCenter.default.removeObserver(self)
    }

    func notifyInternalVideoLooper(player: AVPlayer?) {
        NotificationCenter.default.addObserver(
            self, selector: #selector(CoreValuesViewController.playerItemDidReachEnd),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: player!.currentItem)
    }
}

extension UIViewController {
    @objc func appTokenRefreshed(notification: NSNotification) {}
    @objc func appPushNotificationPayload(notification: NSNotification) {}
    @objc func appScrollToTop(notification: NSNotification) {}
}
