//
//  Theme.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

struct UIStyle {
    enum Inset {
        static let all: CGFloat = 24.0
        static let top: CGFloat = 24.0
        static let left: CGFloat = 24.0
        static let bottom: CGFloat = 24.0
        static let right: CGFloat = 24.0
        static let defaultTop: CGFloat = 18.0

        static let inset4: CGFloat = 4.0
        static let inset8: CGFloat = 8.0
        static let inset12: CGFloat = 12.0
        static let inset16: CGFloat = 16.0
        static let inset20: CGFloat = 20.0
        static let inset24: CGFloat = 24.0
        static let inset32: CGFloat = 32.0
        static let inset40: CGFloat = 40.0
        static let inset48: CGFloat = 48.0
        static let inset64: CGFloat = 64.0
    }

    enum Spacing {
        static let horizontal: CGFloat = 20.0
        static let vertical: CGFloat = 20.0
    }

    enum CornerRadius {
        static let `default`: CGFloat = 10.0
        static let radius16: CGFloat = 16.0
        static let radius15: CGFloat = 15.0
        static let radius20: CGFloat = 20.0
        static let radius24: CGFloat = 24.0
        static let radius30: CGFloat = 30.0
        static let radius8: CGFloat = 8.0
        static let radius5: CGFloat = 5.0
    }

    enum Button {
        static let height: CGFloat = 48.0
        static let cornerRadius: CGFloat = 24.0
    }

    enum Form {
        static let vSpace: CGFloat = 8.0
    }

    enum Overlay {
        static let alpha: CGFloat = 0.7
    }

    enum Avatar {
        static let size: CGFloat = 120.0
        static let small: CGFloat = 80.0
    }

    enum Icon {
        static let sizeSmaller: CGFloat = 10
        static let sizeSmall: CGFloat = 16
        static let sizeMedium: CGFloat = 24
        static let sizeLarge: CGFloat = 32
    }
}

extension UIFont {
    static var heavy24: UIFont {
        return FontFamily.Avenir.heavy.font(size: 24)
    }
    static var heavy20: UIFont {
        return FontFamily.Avenir.heavy.font(size: 20)
    }
    static var heavy16: UIFont {
        return FontFamily.Avenir.heavy.font(size: 16)
    }
    static var medium16: UIFont {
        return FontFamily.Avenir.medium.font(size: 16)
    }
    static var book16: UIFont {
        return FontFamily.Avenir.book.font(size: 16)
    }
    static var heavy14: UIFont {
        return FontFamily.Avenir.heavy.font(size: 14)
    }
    static var heavy12: UIFont {
        return FontFamily.Avenir.heavy.font(size: 12)
    }
    static var heavy8: UIFont {
        return FontFamily.Avenir.heavy.font(size: 8)
    }
    static var medium14: UIFont {
        return FontFamily.Avenir.medium.font(size: 14)
    }
    static var medium12: UIFont {
        return FontFamily.Avenir.medium.font(size: 12)
    }
    static var book12: UIFont {
        return FontFamily.Avenir.book.font(size: 12)
    }
}

class Theme: NSObject {

    // MARK: - Initializer

    static let shared: Theme = {
        let theme = Theme()
        theme.initialize()

        return theme
    }()

    private func initialize() {}

    // MARK: - Configs

    func config () {}

}

extension CommonView {

    func setDefaultShadow() {
        shadowOffsetX = 0
        shadowOffsetY = 2
        shadowBlur = UIStyle.CornerRadius.default
        shadowOpacity = 0.1
        usingShadow = true
    }

    func setupDefaultBottomShadow() {
        shadowOffsetX = 0
        shadowOffsetY = 2
        shadowBlur = UIStyle.CornerRadius.default
        shadowOpacity = 0.1
        usingShadow = true
        coverTopShadow()
    }

    func setupSegmentedDefaultShadow() {
        shadowOffsetX = 0
        shadowOffsetY = 8
        shadowBlur = frame.height / 2.0
        shadowOpacity = 0.1
        usingShadow = true
        layer.cornerRadius = shadowBlur
    }

    func setupSegmentedThinDefaultShadow() {
        shadowOffsetX = 0
        shadowOffsetY = 1
        shadowBlur = frame.height / 2.0
        shadowOpacity = 0.05
        usingShadow = true
        layer.cornerRadius = shadowBlur
    }

    func coverTopShadow() {
        let tag = 123827831
        if let findCoverView = viewWithTag(tag) {
            findCoverView.removeFromSuperview()
        }

        let coverView = UIView()
        let top = shadowBlur - CGFloat(shadowOffsetX)
        coverView.backgroundColor = backgroundColor
        coverView.tag = tag
        addSubview(coverView)
        coverView.anchor(
            .top(coverView.superview!.topAnchor, constant: -top),
            .leading(coverView.superview!.leadingAnchor, constant: 0),
            .trailing(coverView.superview!.trailingAnchor, constant: 0)
        )
        coverView.withHeight(shadowBlur + top)
    }

}
