//
//  APISetting.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

enum APISetting {
    #if DEBUG
    static let baseUrl: String = "https://devapi.qlip.id"
    #else
    static let baseUrl: String = "https://api.qlip.id"
    #endif
}

enum APISettingAuth {
    static let username = "qlip"
    static let password = "%%qlip%%"
    static let grantType = "password"
}

enum APISettingPagination {
    static let page: Int = 0
    static let size: Int = 10
    static let bigLimit: Int = 999999
}
