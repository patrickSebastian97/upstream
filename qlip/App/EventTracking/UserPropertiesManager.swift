//
//  UserPropertiesManager.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 08/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

final class UserPropertiesManager {

    static let shared: UserPropertiesManager = {
        let instance = UserPropertiesManager()
        return instance
    }()

    fileprivate init() {}

    func getInstallVersion() -> String {
        return Storage.EventTracking.getFirstInstallVersion() ?? ""
    }

    func getCurrentVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as? String
        if let version = version {
            return version
        } else { return "" }
    }

    func getKidAge() -> String {
        let date = Storage.EventTracking.getKidDob() ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: date) {
            let age = Calendar.current.dateComponents(
                [.year],
                from: date,
                to: Date()).year!
            return String(age)
        } else {
            return "0"
        }
    }

    func setup(type: UserPropertiesType, value: String? = nil) -> String {
        switch type {
        case .installVersion:
            return getInstallVersion()
        case .currentVersion:
            return getCurrentVersion()
        case .kidAge:
            return getKidAge()
        case .parentAccountName:
            guard let value = value else { return "" }
            return value
        case .kidAccountName:
            guard let value = value else { return "" }
            return value
        case .userIdToDataServer:
            guard let value = value else { return "" }
            return value
        }
    }
}

enum UserPropertiesType: String {
    case installVersion
    case currentVersion
    case kidAge
    case kidAccountName
    case parentAccountName
    case userIdToDataServer
}
