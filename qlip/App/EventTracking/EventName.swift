//
//  EventName.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 06/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

enum EventName: String {

    /// FTUE
    case openOnboarding
    case parentStart
    case kidStart
    case parentClickLogin
    case parentClickRegistration
    case parentClickParentalConsent
    case parentClickPrivacyPolicy
    case parentClickTermsAndCondition
    case parentLoginFillPhoneNumber
    case parentLoginFillPassword
    case parentLoginClickLogin
    case parentLoginWithApple
    case parentLoginWithFacebook
    case parentLoginWithGoogle
    case parentLoginForgotPassword
    case parentForgotPasswordFillPhoneNumber
    case parentForgotPasswordSubmitPhoneNumber
    case parentForgotPasswordGetOtp
    case parentForgotPasswordInputOtp
    case parentForgotPasswordResendOtp
    case parentForgotPasswordSuccessOtp
    case parentForgotPasswordFillNewPassword
    case parentForgotPasswordSubmitNewPass
    case parentForgotPasswordSuccessNewPass
    case parentRegistrationFillAccountName
    case parentRegistrationFillPassword
    case parentRegistrationClickRegistration
    case parentRegistrationFillPhoneNumber
    case parentRegistrationSubmitPhoneNumber
    case parentRegistrationGetOtp
    case parentRegistrationInputOtp
    case parentRegistrationResendOtp
    case parentRegistrationSuccessVerifyOtp
    case parentRegistrationWithApple
    case parentRegistrationWithFacebook
    case parentRegistrationWithGoogle
    case parentAddKidProfOpenOnboarding1
    case parentAddKidProfFillKidName
    case parentAddKidProfFillBirthDate
    case parentAddKidProfClickCreateKidAccount
    case parentAddKidProfOpenOnboarding2
    case parentAddKidProfSubmitKidActivity
    case parentAddKidProfOpenOnboarding3
    case parentAddKidProfSubmitSkill
    case parentAddKidProfOpenOnboarding4
    case parentAddKidProfExploreContent
    case kidClickLogin
    case kidClickRegistration
    case kidClickParentalConsent
    case kidClickPrivacyPolicy
    case kidClickTermsAndCondition
    case kidLoginFillPhoneNumber
    case kidLoginFillPassword
    case kidLoginClickLogin
    case kidLoginWithApple
    case kidLoginWithFacebook
    case kidLoginWithGoogle
    case kidLoginForgotPassword
    case kidForgotPasswordFillKidPhoneNumber
    case kidForgotPasswordSubmitKidPhoneNumber
    case kidForgotPasswordFillParentPhoneNumber
    case kidForgotPasswordSubmitParentPhoneNumber
    case kidForgotPasswordGetOtp
    case kidForgotPasswordInputOtp
    case kidForgotPasswordResendOtp
    case kidForgotPasswordSuccessVerificationOtp
    case kidForgotPasswordFillNewPassword
    case kidForgotPasswordSubmitNewPassword
    case kidForgotPasswordSuccessNewPassword
    case kidRegistrationFillAccountName
    case kidRegistrationFillPassword
    case kidRegistrationClickRegistration
    case kidRegistrationFillKidPhoneNumber
    case kidRegistrationSubmitKidPhoneNumber
    case kidRegistrationKidPhoneGetOtp
    case kidRegistrationKidPhoneInputOtp
    case kidRegistrationKidPhoneResendOtp
    case kidRegistrationSuccessKidPhoneOtp
    case kidRegistrationWithApple
    case kidRegistrationWithFacebook
    case kidRegistrationWithGoogle
    case kidRegistrationFillParentPhoneNumber
    case kidRegistrationChooseProfile
    case kidAddProfileOpenOnboarding1
    case kidAddProfileSubmitTopicProfile
    case kidAddProfileOpenOnboarding2
    case kidAddProfileSubmitSkillProfile
    case kidAddProfileClickJoinChallenge
    case kidAddProfileClickExploreQlip
    case parentSuccessLogin
    case parentSuccessRegistration
    case kidSuccessLogin
    case kidSuccessRegistration
}
