//
//  EventTrackingManager.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 07/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

final class EventTrackingManager {

    static let shared: EventTrackingManager = {
        let instance = EventTrackingManager()
        return instance
    }()

    struct Property {
        let type: PropertyType
    }

    enum PropertyType {
        case propertyBasic
        case parentPropertyBasic
        case kidPropertyBasic
        case propertyForgotOTP(otp: String)
        case propertyList(title: PropertyListType, list: [String])
        case custom(metadata: [String: Any])
    }

    enum UserPropertyType {
        case parent
        case kid
        case all
    }

    enum PropertyListType: String {
        case kidActivityByParent
        case kidSkillByParent
        case kidTopicByKid
        case kidSkillByKid
    }

    private var properties: [Property] = []

    func setProperties(_ properties: [Property]) -> [String: Any] {
        self.properties = properties
        return setup()
    }

    func setup() -> [String: Any] {

        var mergedData: [String: Any] = [:]
        for index in 0..<properties.count {
            let metadata = setMetadata(type: properties[index].type)
            mergedData.merge(dict: metadata)
        }
        return mergedData
    }

    func getDuration(_ user: UserPropertyType) -> Int {
        switch user {
        case .all:
            if let date = Storage.EventTracking.getFirstInstallDate() {
                return Calendar.current.dateComponents(
                    [.second],
                    from: date,
                    to: Date()).second!
            } else { return 0 }
        case .kid:
            if let date = Storage.EventTracking.getFirstKidRegistered() {
                return Calendar.current.dateComponents(
                    [.second],
                    from: date,
                    to: Date()).second!
            } else { return 0 }
        case .parent:
            if let date = Storage.EventTracking.getFirstParentRegistered() {
                return Calendar.current.dateComponents(
                    [.second],
                    from: date,
                    to: Date()).second!
            } else { return 0 }
        }
    }

    func getLifetime(_ user: UserPropertyType) -> Int {
        switch user {
        case .all:
            if let date = Storage.EventTracking.getFirstInstallDate() {
                return Calendar.current.dateComponents(
                    [.day],
                    from: date,
                    to: Date()).day!
            } else { return 0 }
        case .kid:
            if let date = Storage.EventTracking.getFirstKidRegistered() {
                return Calendar.current.dateComponents(
                    [.day],
                    from: date,
                    to: Date()).day!
            } else { return 0 }
        case .parent:
            if let date = Storage.EventTracking.getFirstParentRegistered() {
                return Calendar.current.dateComponents(
                    [.day],
                    from: date,
                    to: Date()).day!
            } else { return 0 }
        }
    }

    func setMetadata(type: PropertyType) -> [String: Any] {
        switch type {
        case .propertyBasic:
            let duration = getDuration(.all)
            let lifetime = getLifetime(.all)
            let data = [
                "duration_in_app" : duration,
                "lifetime_in_app" : lifetime
            ]

            return data
        case .kidPropertyBasic:
            let duration = getDuration(.kid)
            let lifetime = getLifetime(.kid)
            let order = Storage.EventTracking.getEventOrder() + 1
            Storage.EventTracking.saveEventOrder(count: order)
            let data = [
                "kid_duration_in_app" : duration,
                "kid_lifetime_in_app" : lifetime,
                "kid_event_order": order
            ]

            return data
        case .parentPropertyBasic:
            let duration = getDuration(.parent)
            let lifetime = getLifetime(.parent)
            let order = Storage.EventTracking.getEventOrder() + 1
            Storage.EventTracking.saveEventOrder(count: order)
            let data = [
                "parent_duration_in_app" : duration,
                "parent_lifetime_in_app" : lifetime,
                "parent_event_order": order
            ]

            return data
        case .propertyForgotOTP(otp: let otp):
            let data = [
                "otp_source" : otp
            ]

            return data
        case .propertyList(title: let title, list: let list):
            var data: [String: Any] = [:]
            let name = title.rawValue.camelCaseToSnakeCase()
            for index in 0..<list.count {
                data["\(name)_\(index + 1)"] = list[index]
            }

            return data
        case .custom(metadata: let metadata):
            return metadata
        }
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]) {
        for (key, value) in dict {
            updateValue(value, forKey: key)
        }
    }
}
