//
//  DeeplinkParser.swift
//  qlip
//
//  Created by Dan Faerae on 10/09/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

enum DeeplinkParserType: String {
    case kid
    case parent
}

enum DeeplinkKidParentFeatureType: String {
    case home
    case inbox
    case leaderboard
    case reward
    case submission
    case kidsreport
}

final class DeeplinkParser {

//    static func parseDeepLink(_ url: URL) -> DeeplinkType? {
//        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
//            let host = DeeplinkParserType(rawValue: components.host ?? "") else {
//                return nil
//        }
//
//        var pathComponents = components.path.components(separatedBy: "/")
//        pathComponents.removeFirst()
//        let feature: String = pathComponents[safe: 0] ?? ""
//
//        switch host.rawValue {
//        case DeeplinkParserType.kid.rawValue:
//            let id = pathComponents[safe: 1] ?? ""
//            switch feature.lowercased() {
//            case DeeplinkKidParentFeatureType.submission.rawValue:
//                return .kid(type: .submission(id: id))
//            case DeeplinkKidParentFeatureType.home.rawValue:
//                return .kid(type: .home)
//            case DeeplinkKidParentFeatureType.inbox.rawValue:
//                return .kid(type: .inbox(section: id))
//            case DeeplinkKidParentFeatureType.leaderboard.rawValue:
//                return .kid(type: .leaderboard)
//            case DeeplinkKidParentFeatureType.reward.rawValue:
//                return .kid(type: .reward(status: id))
//            case DeeplinkKidParentFeatureType.kidsreport.rawValue:
//                return nil
//            default: return nil
//            }
//        case DeeplinkParserType.parent.rawValue:
//            let id = pathComponents[safe: 1] ?? ""
//            switch feature.lowercased() {
//            case DeeplinkKidParentFeatureType.submission.rawValue:
//                return .parent(type: .submission(id: id))
//            case DeeplinkKidParentFeatureType.home.rawValue:
//                return .parent(type: .home)
//            case DeeplinkKidParentFeatureType.inbox.rawValue:
//                return .parent(type: .inbox(section: id))
//            case DeeplinkKidParentFeatureType.leaderboard.rawValue:
//                return nil
//            case DeeplinkKidParentFeatureType.reward.rawValue:
//                return .parent(type: .reward(status: id))
//            case DeeplinkKidParentFeatureType.kidsreport.rawValue:
//                return .parent(type: .kidsreport)
//            default: return nil
//            }
//        default: return nil
//        }
//    }

}
