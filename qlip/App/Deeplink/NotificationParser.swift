//
//  NotificationParser.swift
//  qlip
//
//  Created by Dan Faerae on 10/09/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import Foundation

final class NotificationParser {

    static let shared: NotificationParser = {
        let instance = NotificationParser()
        return instance
    }()

    private init() {}

//    func handleNotification(_ userInfo: [AnyHashable : Any]) -> DeeplinkType? {
//        if let urlString = userInfo["action"] as? String, let url = URL(string: urlString) {
//            Storage.DeeplinkPN.setDeeplinkPN(deepLink: url)
//            return DeeplinkParser.parseDeepLink(url)
//        }
//
//        return nil
//    }

}
