//
//  DeeplinkManager.swift
//  qlip
//
//  Created by Dan Faerae on 10/09/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

final class DeeplinkManager {

    static let shared: DeeplinkManager = {
        let instance = DeeplinkManager()
        return instance
    }()

    fileprivate init() {}

//    @discardableResult
//    func handleDeeplink(url: URL) -> Bool {
//        deeplinkType = DeeplinkParser.parseDeepLink(url)
//        return deeplinkType != nil
//    }

    func handleDeeplinkFromApp(url: URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }

    }

}
