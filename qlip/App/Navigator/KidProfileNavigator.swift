//
//  KidProfileNavigator.swift
//  qlip
//
//  Created by Rahman Bramantya on 28/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

protocol KidProfileNavigator {
    func goToSelectInterest(kidName: String, kidId: String)
}

extension KidProfileNavigator where Self: UIViewController {
    func goToSelectInterest(kidName: String, kidId: String) {
        let controller = SelectInterestSkillViewController()
        controller.onboardingType = .selectInterest
        controller.kidName = kidName
        controller.kidId = kidId
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
