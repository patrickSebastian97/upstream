//
//  CreateKidProfileNavigator.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 03/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

protocol CreateKidProfileNavigator {
    func goToSelectInterest(kidName: String, kidId: String)
}

extension CreateKidProfileNavigator where Self: UIViewController {
    func goToSelectInterest(kidName: String, kidId: String) {
        let controller = SelectInterestSkillViewController()
        controller.onboardingType = .selectInterest
        controller.kidName = kidName
        controller.kidId = kidId
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
