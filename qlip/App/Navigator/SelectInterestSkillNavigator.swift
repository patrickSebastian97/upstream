//
//  SelectInterestSkillNavigator.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 03/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

protocol SelectInterestSkillNavigator {
    func goToSelectSkill(kidName: String, kidId: String)
}

extension SelectInterestSkillNavigator where Self: UIViewController {
    func goToSelectSkill(kidName: String, kidId: String) {
        let controller = SelectInterestSkillViewController()
        controller.kidName = kidName
        controller.kidId = kidId
        controller.onboardingType = .selectSkill
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
}
