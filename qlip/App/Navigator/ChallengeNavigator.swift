//
//  ChallengeNavigator.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 23/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import FloatingPanel

protocol ChallengeNavigator {
    func goToChallengeDetail(view: FloatingPanelController, viewModel: ChallengeDetailViewModel)
}

extension ChallengeNavigator where Self: UIViewController {
    func goToChallengeDetail(view: FloatingPanelController, viewModel: ChallengeDetailViewModel) {
        let controller = ChallengeDetailDrawerViewController()
        view.set(contentViewController: controller)
        self.present(view, animated: true, completion: nil)
    }
}
