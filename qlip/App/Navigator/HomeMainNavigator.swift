//
//  HomeMainNavigator.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

protocol HomeMainNavigator {
    func goToLogin()
    func goToChallenge(id: String)
}

extension HomeMainNavigator where Self: UIViewController {
    func goToLogin() {
        let controller = LoginAndRegisterViewController()
        controller.screenType = .login
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToChallenge(id: String) {
        let controller = ChallengeViewController()
        controller.challengeId = id
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
}
