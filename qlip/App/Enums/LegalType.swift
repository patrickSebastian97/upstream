//
//  LegalType.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

enum LegalType: String {
    case parentConsent = "parent_consent"
    case termCondition = "term_condition"
    case privacyPolicy = "privacy_policy"
    case aboutUs = "about_us"
}
