//
//  LegalLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 17/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension LegalViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        let view = scrollView.contentView

        view.vstack(
            view.hstack(titleLabel),
            view.hstack(notesLabel).padTop(UIStyle.Inset.inset20),
            view.hstack(webview).padTop(UIStyle.Inset.inset32)
        ).withMargins(
            .init(
                top: UIStyle.Inset.inset32,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )
    }
}
