//
//  OTPType.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

enum OTPType: String {
    case forgotPassword = "reset_password"
    case register = "register"
    case updatePhone = "update_phone"
}
