//
//  LegalViewModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

class LegalViewModel: ViewModel<LegalModel> {
    var title: String!
    var note: String!
    var content: String!

    override var item: LegalModel! {
        didSet {
            title = item.title
            note = item.notes
            content = item.content
        }
    }
}
