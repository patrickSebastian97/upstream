//
//  UserType.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 17/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

enum UserType: String {
    case kid
    case parent
}
