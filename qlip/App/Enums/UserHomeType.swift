//
//  UserHomeType.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 13/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

enum UserHomeType {
    case parentLogin
    case kidLogin
    case parentGuest
    case kidGuest
}
