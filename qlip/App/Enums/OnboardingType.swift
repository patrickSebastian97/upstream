//
//  OnboardingType.swift
//  qlip
//
//  Created by Rahman Bramantya on 29/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

enum OnboardingType: String {
    case selectInterest
    case selectSkill
}
