//
//  APIConstants.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

struct APIConstants {
    enum HTTPStatus {
        static let unauthorized: Int = 401
        static let forbidden: Int = 403
        static let existingUser: Int = 409
        static let minVideoDuration: Int = 400
    }

    enum Pagination {
        static let page = 1
        static let limit = 12
        static let bigLimit = 9999
    }

    enum Settings {
        #if DEBUG
        static let enableDebugging: Bool = true
        #else
        static let enableDebugging: Bool = false
        #endif
    }

    enum CountryCode {
        static let id: String = "Wpmbk5XezJ"
        static let code: String = "+62"
    }

    enum Reaction {
        static let likeId: String = "VolejRejNm"
    }
}
