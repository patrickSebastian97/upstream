//
//  AppSetting.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

enum SettingOTP {
    static let otpTimeInSeconds: Int = 20
}

struct SettingValidation {
    enum PhoneNumber {
        static let minLength: Int = 8
        static let maxLength: Int = 12
    }
    enum Password {
        static let minLength: Int = 6
    }
    enum Username {
        static let minLength: Int = 5
        static let maxLength: Int = 12
    }
    enum InterestSkillTag {
        static let minimum: Int = 1
        static let maximum: Int = 3
    }
}

enum QlipAccount {
    static let instagram: String = "qlip.id"
    static let emailSupport: String = "support@qlip.id"
}

enum KidAge {
    static let minimum: Int = 6
    static let maximum: Int = 17
}
