//
//  APIHelper.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import PMKFoundation
import SwiftyJSON

extension UIViewController {
    func handleError(error: Error, action: InfoPopup.ActionCallback? = nil) {
        var errorMessage: String = ""
        var errorCode: Int = -1
        if let errorResponse = error as? ErrorResponseModel {
            errorMessage = errorResponse.meta.message
            errorCode = errorResponse.meta.code
        } else {
            if let httpError = error as? PMKHTTPError {
                switch httpError {
                case .badStatusCode( _, _, let response):
                    errorCode = response.statusCode
                }
                // catch any message as possible from BE side
                if let message = JSON(httpError.jsonDictionary as Any)["message"].string {
                    errorMessage = message
                } else if let message = JSON(httpError.jsonDictionary as Any)["error_description"].string {
                    errorMessage = message
                } else if let message = JSON(httpError.jsonDictionary as Any)["error"].string {
                    errorMessage = message
                } else {
                    errorMessage = error.localizedDescription
                }
            } else {
                errorMessage = error.localizedDescription
            }
        }

        let forbidden = APIConstants.HTTPStatus.forbidden
        let exist = APIConstants.HTTPStatus.existingUser
        if errorCode == forbidden {
            showWarningToast(message: errorMessage)
        } else if errorCode == exist {
            if let actionCallback = action {
                InfoPopup.shared.show(
                    image: Asset.Authentication.welcome.image,
                    title: "Oops",
                    message: errorMessage,
                    actionButton: InfoPopup.ActionButton(title: L10n.login,
                                                         action: actionCallback, color: nil))
            } else {
                showWarningToast(message: errorMessage)
            }
        } else {
            showWarningToast(message: errorMessage)
        }
    }
    // swiftlint:enable function_body_length

    func showError(error: Error) -> String {
        var errorMessage: String = ""
//        var errorCode: Int = -1
        if let errorResponse = error as? ErrorResponseModel {
            errorMessage = errorResponse.meta.message
//            errorCode = errorResponse.meta.code
        } else {
            if let httpError = error as? PMKHTTPError {
                // catch any message as possible from BE side
                if let message = JSON(httpError.jsonDictionary as Any)["message"].string {
                    errorMessage = message
                } else if let message = JSON(httpError.jsonDictionary as Any)["error_description"].string {
                    errorMessage = message
                } else if let message = JSON(httpError.jsonDictionary as Any)["error"].string {
                    errorMessage = message
                } else {
                    errorMessage = error.localizedDescription
                }
            } else {
                errorMessage = error.localizedDescription
            }
        }

        return errorMessage
    }
}
