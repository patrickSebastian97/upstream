//
//  AppDemo.swift
//  qlip
//
//  Created by Amelia Lim on 26/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

private typealias `Self` = AppDemo

class AppDemo: BaseView {
    // MARK: - Properties

    static var shared: AppDemo = {
       return AppDemo()
    }()

    enum AnimationType {
        case open
        case close
    }

    let overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = UIStyle.Overlay.alpha

        return view
    }()
    let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true

        return view
    }()
    lazy var titleLabel: UILabel = {
        let view = UILabel(
            font: .heavy16,
            textColor: Asset.Color.black.color,
            textAlignment: .center,
            numberOfLines: 0
        )
        view.lineBreakMode = .byWordWrapping

        return view
    }()
    lazy var messageLabel: UILabel = {
        let view = UILabel(
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .center,
            numberOfLines: 0
        )
        view.lineBreakMode = .byWordWrapping

        return view
    }()
    lazy var actionButton: Button = {
        let view = Button(title: "OK", type: .yellow)
        view.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)

        return view
    }()

    // MARK: - Initializers

    override func setupView() {
        super.setupView()

        self.alpha = 0

        addSubview(overlayView)
        overlayView.fillSuperview()

        contentView.vstack(
            contentView.hstack(titleLabel).padTop(12),
            contentView.hstack(messageLabel).padTop(8),
            contentView.hstack(actionButton.withHeight(UIStyle.Button.height)).padTop(16)
        ).withMargins(.allSides(16))
        addSubview(contentView)
        contentView.snp.makeConstraints { maker in
            maker.centerY.equalToSuperview()
            maker.leading.equalToSuperview().inset(40)
            maker.trailing.equalToSuperview().inset(40)
        }
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        guard superview != nil else {
            return
        }
        self.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

// MARK: - Helpers
extension Self {
    private func getAppVersion() -> String {
        return (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
    }

    private func getBuildVersion() -> String {
        return (Bundle.main.infoDictionary?["CFBundleVersion"] as? String) ?? ""
    }

    private func addToWindowView() {
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }

        window?.addSubview(self)
    }

    private func animate(type: AnimationType, completion: (() -> Void)? = nil) {
        UIView.animate(
            withDuration: CATransaction.animationDuration(),
            animations: {
                switch type {
                case .open:
                    self.alpha = 1
                case .close:
                    self.alpha = 0
                }
        }, completion: {_ in
            completion?()
        })
    }

    func show() {
        DispatchQueue.main.async {
            let version = "\(self.getAppVersion())(\(self.getBuildVersion()))"
            self.titleLabel.text = "Information"
            self.messageLabel.text = "\nThis is a demo app.\nEnvironment: Debug.\n\nVersion: \(version)"
            self.addToWindowView()
            self.animate(type: .open)
        }
    }

    func dismiss(completion: (() -> Void)? = nil) {
        animate(type: .close, completion: {
            self.removeFromSuperview()
            completion?()
        })
    }
}

// MARK: - Actions
extension Self {
    @objc private func actionButtonTapped() {
        dismiss(completion: nil)
    }
}
