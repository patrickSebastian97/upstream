//
//  AppLoader.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 20/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import PromiseKit

class LoadingView: UIView {}

extension UIViewController {
    //swiftlint:disable function_body_length
    func showLoadingWithProgress(overWindow: Bool = true,
                                 shapeLayer: CAShapeLayer,
                                 percentageLabel: UILabel,
                                 loadingMessage: String
    ) {

        var frame = self.view.frame
        if overWindow {
            frame = UIApplication.shared.windows.first { $0.isKeyWindow }?.frame ?? .zero
        }
        let viewsBackground = LoadingView.init(frame: frame)
        viewsBackground.backgroundColor = UIColor.black.withAlphaComponent(0.5)

        percentageLabel.text = "0%"
        percentageLabel.font = .heavy14
        percentageLabel.textColor = Asset.Color.white.color
        percentageLabel.textAlignment = .center

        let messageLabel = UILabel(
            text: loadingMessage,
            font: .heavy14,
            textColor: Asset.Color.white.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        let trackLayer = CAShapeLayer()

        let circularPath = UIBezierPath(
            arcCenter: .zero,
            radius: 30,
            startAngle: 0,
            endAngle: 2 * CGFloat.pi,
            clockwise: true
        )
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.lineWidth = 5
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = CAShapeLayerLineCap.round
        trackLayer.position = view.center

        viewsBackground.layer.addSublayer(trackLayer)

        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 5
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        shapeLayer.position = view.center
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        shapeLayer.strokeEnd = 0

        viewsBackground.layer.addSublayer(shapeLayer)
        viewsBackground.addSubview(percentageLabel)
        percentageLabel.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        percentageLabel.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        viewsBackground.addSubview(messageLabel)
        messageLabel.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        messageLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(percentageLabel.snp.bottom).inset(-UIStyle.Inset.inset40)
            maker.centerX.equalToSuperview()
        }

        if overWindow {
            UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(viewsBackground)
        } else {
            self.view.addSubview(viewsBackground)
        }
    }
    //swiftlint:enable function_body_length

    func hideLoading() {
        self.view.subviews
            .filter({ $0.isKind(of: LoadingView.self)})
            .forEach({
                $0.removeFromSuperview()
            })
        UIApplication.shared.windows.first { $0.isKeyWindow }?.subviews
            .filter({ $0.isKind(of: LoadingView.self)})
            .forEach({
                $0.removeFromSuperview()
            })
    }
}
