//
//  AppLog.swift
//  qlip
//
//  Created by Amelia Lim on 08/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import SwiftyJSON

final class Log {

    // MARK: - Properties

    #if DEBUG
    static let enableLog: Bool = true
    #else
    static let enableLog: Bool = false
    #endif

    struct LogTag {
        var tag: Tag = .none
        var enabled: Bool = false
    }

    enum Tag: String {
        case none = "[QLIP]"
        case api = "[QLIP API]"
        case location = "[QLIP Location]"
        case token = "[QLIP token]"
        case firebase = "[QLIP Firebase]"
        case image = "[QLIP Image]"
    }

    static let logTags: [LogTag] = [
        LogTag(tag: .none, enabled: false),
        LogTag(tag: .api, enabled: true),
        LogTag(tag: .token, enabled: true),
        LogTag(tag: .firebase, enabled: true)
    ]

    // MARK: - Private Functions

    fileprivate static func sourceFileName(filePath: String) -> String {
        let components = filePath.components(separatedBy: "/")
        return components.isEmpty ? "" : components.last!
    }

    // MARK: - Public Functions

    static func debugFunction(
        filename: String = #file,
        line: Int = #line,
        function: String = #function
    ) {
        if enableLog {
            let tag = Tag.none.rawValue
            let fileName = "[\(sourceFileName(filePath: filename)), line \(line)]"
            print("\(tag)\(fileName): \(function)")
        }
    }

    static func debug(_ items: Any..., tag: Log.Tag = .none, useDump: Bool = true) {
        if enableLog {
            if !logTags.filter({ $0.tag == tag && $0.enabled }).isEmpty {
                if useDump {
                    dump(items, name: tag.rawValue, indent: 0, maxDepth: Int.max, maxItems: Int.max)
                } else {
                    print("\(tag.rawValue): \(items)")
                }
            }
        }
    }

}
