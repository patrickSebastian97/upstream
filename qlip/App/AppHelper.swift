//
//  AppHelper.swift
//  qlip
//
//  Created by Amelia Lim on 06/07/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import PromiseKit

struct AppHelper {
    static var isRequestingToken: Bool = false

    static func requesTokenIfNeeded(force: Bool) {
        let token = TokenModel.Storage.getToken()?.accessToken ?? ""
        if token.isEmpty || force {
            if !AppHelper.isRequestingToken {
                AppHelper.isRequestingToken = true
                Log.debug("Requesting token is forcing: \(force)", tag: .token)
                APIService.Authentication.requestToken()
                    .done { data in
                        if let tokenData = data.dataModel() {
                            TokenModel.Storage.delete()
                            TokenModel.Storage.save(tokenModel: tokenData)
                            NotificationCenter.default.post(
                                name: .appTokenRefreshed,
                                object: nil, userInfo: nil
                            )
                            let token = TokenModel.Storage.getToken()?.accessToken ?? ""
                            Log.debug("Token requested with token \(token)", tag: .token)
                        }
                }.ensure {
                    AppHelper.isRequestingToken = false
                }.catch { error in
                    Log.debug("Token request with error: \(error)", tag: .token)
                }
            }
        }
    }

    static func getRootTabBarController() -> MainTabBarViewController? {
        guard let window = (UIApplication.shared.delegate as? AppDelegate)?.window else {
            return nil
        }

        guard let rootViewController = window.rootViewController else {
            return nil
        }

        if let mainTabbarController = rootViewController as? MainTabBarViewController {
            return mainTabbarController
        }

        return nil
    }

    static func logoutKid() {
        KidRealmModel.deleteAll()
        TokenModel.Storage.delete()
        requesTokenIfNeeded(force: true)
        Storage.EventTracking.removeKidDob()
        getRootTabBarController()?.gotoHomeScreen()
    }

    static func logoutParent() {
        ParentRealmModel.deleteAll()
        TokenModel.Storage.delete()
        requesTokenIfNeeded(force: true)
        Storage.EventTracking.removeKidDob()
        getRootTabBarController()?.gotoHomeScreen()
    }

    static func updateFCMToken() {
        let token = Firebase.shared.getFCMToken() ?? ""
        let isKidUseSharedDevice = ParentRealmModel.isLoggedIn() && KidRealmModel.isLoggedIn()
        APIService.FirebaseAPI.registerFirebaseToken(token: token, isKidUseShareDevice: isKidUseSharedDevice)
        .done { _ in
            Log.debug("FCM token updated to API: \(token)", tag: .token)
        }.ensure {
        }.catch { _ in }
    }

}

extension UIViewController {
    @discardableResult
    func openUrl(_ url: String) -> Bool {
        guard let url = URL(string: url) else { return false }

        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            return true
        } else {
            return false
        }
    }

    func setKidUser(user: KidRealmModel) {
        user.deleteAndSave()
        if let dob = user.dob {
            Storage.EventTracking.saveKidDob(dob: dob)
        }

        // set User Properties
        Firebase.shared.setUserProperties(.kidAccountName, value: user.username)
        Firebase.shared.setUserProperties(.userIdToDataServer, value: user.id)
    }

    func setParentUser(user: ParentRealmModel) {
        user.deleteAndSave()

        // set User Properties
        Firebase.shared.setUserProperties(.parentAccountName, value: user.username)
        Firebase.shared.setUserProperties(.userIdToDataServer, value: user.id)
    }

    func getMainTabBarViewController() -> MainTabBarViewController? {
        return tabBarController as? MainTabBarViewController
    }

    func topMostViewController() -> UIViewController {
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }

        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }

        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }

        return self
    }

    func openInstagram(username: String) {
        let appURL = URL(string: "instagram://user?username=\(username)")!
        let application = UIApplication.shared
        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            let webURL = URL(string: "https://instagram.com/\(username)")!
            application.open(webURL)
        }
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return UIApplication.shared.windows.first {
            $0.isKeyWindow }?.rootViewController?.topMostViewController()
    }

    func getMainTabBarController() -> MainTabBarViewController? {
        return UIApplication.shared.windows.first {
            $0.isKeyWindow }?.rootViewController as? MainTabBarViewController
    }
}
