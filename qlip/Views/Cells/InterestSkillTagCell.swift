//
//  InterestSkillTagCell.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 02/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import LBTATools

class InterestSkillTagCell: LBTAListCell<InterestSkillTagViewModel> {

    // MARK: - Properties

    lazy var interestSkillView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.borderColor = Asset.Color.black.color.cgColor
        view.layer.borderWidth = 1

        return view
    }()
    lazy var imageView: UIImageView = {
        let view = UIImageView(
            image: nil,
            contentMode: .scaleAspectFit
        )

        return view
    }()
    lazy var nameLabel: UILabel = {
        let view = UILabel(
            text: "",
            font: .heavy14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 0
        )
        view.lineBreakMode = .byWordWrapping

        return view
    }()
    var isClicked: Bool = false {
        didSet {
            if isClicked {
                interestSkillView.backgroundColor = Asset.Color.blue.color
                interestSkillView.layer.borderWidth = 0
                nameLabel.textColor = Asset.Color.white.color
                imageView.loadImage(url: item.imageWhiteUrl)
            } else {
                interestSkillView.backgroundColor = Asset.Color.white.color
                interestSkillView.layer.borderWidth = 1
                nameLabel.textColor = Asset.Color.black.color
                imageView.loadImage(url: item.imageBlackUrl)
            }
        }
    }

    override var item: InterestSkillTagViewModel! {
        didSet {
            nameLabel.text = item.name
            if item.imageBlackUrl.isEmpty {
                imageView.isHidden = true
            } else {
                imageView.loadImage(url: item.imageBlackUrl)
            }
        }
    }

    // MARK: - Life Cycles
    override func setupViews() {
        super.setupViews()

        interestSkillView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        interestSkillView.addSubview(imageView)
        imageView.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.trailing.equalTo(nameLabel.snp.leading).inset(-UIStyle.Inset.inset8)
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset8)
        }

        vstack(interestSkillView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        interestSkillView.layer.cornerRadius = self.contentView.frame.height / 2
        interestSkillView.layer.masksToBounds = true
    }
}
