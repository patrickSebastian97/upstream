//
//  KidAccountProfileSetupCell.swift
//  qlip
//
//  Created by Rahman Bramantya on 22/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import LBTATools
import Kingfisher

class KidAccountProfileSetupCell: LBTAListCell<ChooseKidProfileViewModel> {

    // MARK: - Properties

    lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.withSize(.init(width: UIStyle.Avatar.small, height: UIStyle.Avatar.small))
        imageView.layer.cornerRadius = UIStyle.Avatar.small / 2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill

        return imageView
    }()
    lazy var fullnameLabel : UILabel = {
        let label = UILabel(
            text: "",
            font: .heavy16,
            textColor: Asset.Color.black.color,
            textAlignment: .center,
            numberOfLines: 2
        )

        return label
    }()
    override var item: ChooseKidProfileViewModel! {
        didSet {
            avatarImageView.loadImage(url: item.imageUrl)
            fullnameLabel.text = item.fullname
        }
    }

    // MARK: - Life Cycles

    override func setupViews() {
        super.setupViews()
        vstack(
            hstack(avatarImageView),
            hstack(fullnameLabel).padTop(UIStyle.Inset.inset8),
            UIView(),
            alignment: .center
        )
    }
}
