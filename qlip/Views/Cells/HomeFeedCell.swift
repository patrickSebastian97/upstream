//
//  HomeFeedCell.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import LBTATools
import AVKit

private typealias `Self` = HomeFeedCell

protocol HomeFeedCellDelegate: class {
    func homeFeedCell(_ homeFeedCell:HomeFeedCell, didSelectSubmissionAt index: Int)
    func didSoundButtonTapped(_ homeFeedCell:HomeFeedCell)
}
// swiftlint:disable type_body_length
// swiftlint:disable file_length
class HomeFeedCell: LBTAListCell<HomeFeedViewModel>, AVAssetResourceLoaderDelegate {

    // MARK: - Properties
    lazy var soundButton: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(soundButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var thumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = Asset.Color.white.color

        return imageView
    }()
    private lazy var player: AVPlayer = {
        let player = AVPlayer()
        return player
    }()
    private lazy var playerLayer: AVPlayerLayer = {
        let layer = AVPlayerLayer()
        layer.videoGravity = .resizeAspectFill
        layer.player = player
        return layer
    }()
    lazy var videoView: UIView = {
        let view = UIView()
        view.layer.addSublayer(playerLayer)

        return view
    }()
    lazy var movingContentView: UIView = {
        let view = UIView()
        view.addSubview(videoView)
        videoView.fillSuperview()
        view.addSubview(thumbnailImageView)
        thumbnailImageView.fillSuperview()
        view.clipsToBounds = true

        return view
    }()
    lazy var continueWatchView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Color.blackAlpha50.color
        view.isHidden = true

        return view
    }()
    lazy var continueWatchLabel: UILabel = {
        let label = UILabel(
            text: L10n.continueWhatching,
            font: .heavy12,
            textColor: Asset.Color.white.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var playImageView: UIImageView = {
        let imageview = UIImageView(
            image: Asset.General.playButton.image.withRenderingMode(.alwaysOriginal),
            contentMode: .scaleAspectFit
        )

        return imageview
    }()
    lazy var continueWatchButtonView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.borderWidth = 1
        view.layer.borderColor = Asset.Color.white.color.cgColor
        view.layer.cornerRadius = 17

        return view
    }()
    lazy var coinExpBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Color.blackAlpha30.color
        view.layer.cornerRadius = UIStyle.CornerRadius.radius16

        return view
    }()
    lazy var coinImageView: UIImageView = {
        let imageView = UIImageView(
            image: Asset.General.moneyIcon.image.withRenderingMode(.alwaysOriginal),
            contentMode: .scaleAspectFit
        )

        return imageView
    }()
    lazy var coinLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book12,
            textColor: Asset.Color.white.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var expImageView: UIImageView = {
        let imageView = UIImageView(
            image: Asset.General.experienceIcon.image.withRenderingMode(.alwaysOriginal),
            contentMode: .scaleAspectFit
        )

        return imageView
    }()
    lazy var expLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book12,
            textColor: Asset.Color.white.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 2
        )

        return label
    }()
    lazy var creatorNameLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .medium14,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var likeButton: UIButton = {
        let button = UIButton()
        button.setImage(
            Asset.General.likeInactiveHome.image.withRenderingMode(.alwaysOriginal),
            for: .normal
        )
        button.contentEdgeInsets = UIEdgeInsets(
            top: UIStyle.Inset.inset16,
            left: UIStyle.Inset.inset4,
            bottom: UIStyle.Inset.inset12,
            right: UIStyle.Inset.inset16
        )

        return button
    }()
    lazy var totalLikeLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .medium12,
            textColor: Asset.Color.black.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var shareButton: UIButton = {
        let button = UIButton()
        button.setImage(
            Asset.General.share.image.withRenderingMode(.alwaysOriginal),
            for: .normal
        )
        button.contentEdgeInsets = UIEdgeInsets(
            top: UIStyle.Inset.inset16,
            left: UIStyle.Inset.inset4,
            bottom: UIStyle.Inset.inset12,
            right: UIStyle.Inset.inset16
        )

        return button
    }()
    lazy var submissionLabel: UILabel = {
        let label = UILabel(
            text: L10n.submission,
            font: .medium12,
            textColor: Asset.Color.blackAlpha30.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var addSubmissionButton: UIButton = {
        let button = UIButton()
        button.setTitle(L10n.beTheFrist, for: .normal)
        button.titleLabel?.font = .medium14
        button.setTitleColor(Asset.Color.blue.color, for: .normal)
        button.tintColor = Asset.Color.blue.color
        button.contentHorizontalAlignment = .left

        return button
    }()
    lazy var homeSubmissionListController: HomeSubmissionListController = {
        let controller = HomeSubmissionListController()
        controller.view.isHidden = true
        controller.delegate = self

        return controller
    }()
    lazy var otherSubmissionLabelButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(Asset.Color.black.color, for: .normal)
        button.titleLabel?.font = .medium14
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.tintColor = Asset.Color.black.color
        button.contentHorizontalAlignment = .left
        button.isHidden = true

        return button
    }()
    var isLike: Bool = false
    var totalReaction: Int = 0
    private var submissions: [SubmissionModel] = []
    private var maximumSubmissionShowed: Int = 3
    private var maximumDurationVideo: Double = 5.0
    let videoToken = "a024473de51367b47ba6424e52ee2885"
    private var observer: NSKeyValueObservation?
    private var videoObserver: NSObjectProtocol?
    weak var delegate: HomeFeedCellDelegate?

    override var item: HomeFeedViewModel! {
        didSet {
            soundButton.setImage(
                Storage.SoundState.getSoundState() ?
                    Asset.General.soundEnable.image : Asset.General.soundDisable.image,
                for: .normal
            )
            thumbnailImageView.loadImage(url: item.image)
            coinLabel.text = String(item.totalQoin)
            expLabel.text = String(item.totalExp)
            titleLabel.text = item.name
            creatorNameLabel.text = item.creatorName
            totalLikeLabel.text = String(item.totalReaction)
            totalReaction = item.totalReaction
            isLike = item.isLiked
            submissions = item.submissionList
            if !submissions.isEmpty {
                addSubmissionButton.isHidden = true
                homeSubmissionListController.view.isHidden = false
                homeSubmissionListController.items =
                    self.submissions.map({ HomeSubmissionViewModel(item: $0) }
                )
            } else {
                addSubmissionButton.isHidden = false
                homeSubmissionListController.view.isHidden = true
            }
            if item.totalSubmission > maximumSubmissionShowed {
                otherSubmissionLabelButton.isHidden = false
                let otherSubmissionCount = item.totalSubmission - maximumSubmissionShowed
                otherSubmissionLabelButton.setTitle(
                    L10n.numberOtherSubmission(otherSubmissionCount),
                    for: .normal
                )
            } else {
                otherSubmissionLabelButton.isHidden = true
            }
            likeButton.setImage(
                item.isLiked ? Asset.General.likeActiveHome.image : Asset.General.likeInactiveHome.image,
                for: .normal
            )
        }
    }

    // MARK: - life Cycles
    // swiftlint:disable function_body_length
    override func setupViews() {
        super.setupViews()

        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = UIStyle.CornerRadius.radius8
        contentView.backgroundColor = Asset.Color.white.color

        contentView.addSubview(movingContentView)
        movingContentView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.width.equalTo(contentView.snp.width)
            maker.height.equalTo(contentView.snp.width)
        }
        contentView.addSubview(continueWatchView)
        continueWatchView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.width.equalTo(contentView.snp.width)
            maker.height.equalTo(contentView.snp.width)
        }
        continueWatchView.addSubview(continueWatchButtonView)
        continueWatchButtonView.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        continueWatchButtonView.addSubview(playImageView)
        playImageView.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset12)
            maker.top.bottom.equalToSuperview().inset(10)
        }
        continueWatchButtonView.addSubview(continueWatchLabel)
        continueWatchLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(playImageView.snp.trailing).inset(-UIStyle.Inset.inset8)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.centerY.equalToSuperview()
        }
        contentView.addSubview(soundButton)
        soundButton.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        movingContentView.addSubview(coinExpBackgroundView)
        coinExpBackgroundView.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.bottom.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        coinExpBackgroundView.addSubview(coinImageView)
        coinImageView.snp.makeConstraints { (maker) in
            maker.leading.bottom.top.equalToSuperview().inset(UIStyle.Inset.inset8)
        }
        coinExpBackgroundView.addSubview(coinLabel)
        coinLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(coinImageView.snp.trailing).inset(-UIStyle.Inset.inset4)
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
        }
        coinExpBackgroundView.addSubview(expImageView)
        expImageView.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.leading.equalTo(coinLabel.snp.trailing).inset(-UIStyle.Inset.inset16)
        }
        coinExpBackgroundView.addSubview(expLabel)
        expLabel.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.leading.equalTo(expImageView.snp.trailing).inset(-UIStyle.Inset.inset4)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(movingContentView.snp.bottom).inset(-UIStyle.Inset.inset16)
            maker.leading.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        contentView.addSubview(creatorNameLabel)
        creatorNameLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).inset(-UIStyle.Inset.inset8)
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        contentView.addSubview(shareButton)
        shareButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).inset(6)
            maker.trailing.equalToSuperview()
        }
        contentView.addSubview(totalLikeLabel)
        totalLikeLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(shareButton.snp.leading).inset(-UIStyle.Inset.inset12)
            maker.centerY.equalTo(shareButton.snp.centerY)
        }
        contentView.addSubview(likeButton)
        likeButton.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(totalLikeLabel.snp.leading).inset(UIStyle.Inset.inset12)
            maker.centerY.equalTo(totalLikeLabel.snp.centerY)
            maker.leading.equalTo(creatorNameLabel.snp.trailing).inset(-UIStyle.Inset.inset16)
        }
        contentView.addSubview(submissionLabel)
        submissionLabel.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.top.equalTo(creatorNameLabel.snp.bottom).inset(-UIStyle.Inset.inset8)
        }
        contentView.addSubview(addSubmissionButton)
        addSubmissionButton.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.bottom.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.top.equalTo(submissionLabel.snp.bottom).inset(-UIStyle.Inset.inset12)
        }
        contentView.addSubview(homeSubmissionListController.view)
        homeSubmissionListController.view.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.top.equalTo(submissionLabel.snp.bottom)
            maker.height.lessThanOrEqualTo(50)
            maker.width.equalTo(140)
        }
        contentView.addSubview(otherSubmissionLabelButton)
        otherSubmissionLabelButton.snp.makeConstraints { (maker) in
            maker.leading.equalTo(homeSubmissionListController.view.snp.trailing)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.centerY.equalTo(homeSubmissionListController.view.snp.centerY)
        }
        movingContentView.bringSubviewToFront(coinExpBackgroundView)
        layer.shadowColor = Asset.Color.black.color.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 8
        layer.shadowOffset = CGSize(width: 0, height: 10)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = self.bounds
    }
}

extension HomeFeedCell: HomeSubmissionListControllerDelegate {
    func homeSubmissionListController(
        _ homeSubmissionListController: HomeSubmissionListController,
        didSubmissionSelectAt index: Int
    ) {
        delegate?.homeFeedCell(self, didSelectSubmissionAt: index)
    }
}

// MARK: - Public Method
extension Self {

    func playVideo(videoUrl: String) {
        continueWatchView.isHidden = true
        if let url = URL(string: videoUrl) {
            let asset = AVURLAsset(url: url)
            asset.resourceLoader.setDelegate(self, queue: DispatchQueue(__label: "AV \(tag)", attr: nil))
            let item = AVPlayerItem(asset: asset)

            observer = item.observe(\.status, options: .new) { (item, _) in
                if item.status == .readyToPlay {
                    self.movingContentView.bringSubviewToFront(self.videoView)
                }
            }

            try? AVAudioSession.sharedInstance().setCategory(.playback)

            player.replaceCurrentItem(with: item)
            player.isMuted = !Storage.SoundState.getSoundState()
            player.play()

            if let observer = videoObserver {
                NotificationCenter.default.removeObserver(
                    observer,
                    name: .AVPlayerItemDidPlayToEndTime,
                    object: player.currentItem
                )
            }
            videoObserver = NotificationCenter.default.addObserver(
                forName: .AVPlayerItemDidPlayToEndTime,
                object: player.currentItem,
                queue: .main
            ) { _ in
                self.player.seek(to: .zero)
                self.player.play()
            }
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { [weak self] timer in
                guard let self = self else { return }
                if let second = self.player.currentItem?.currentTime().seconds,
                   second > self.maximumDurationVideo {
                    self.observer = nil
                    self.player.pause()
                    self.player.seek(to: .zero)
                    timer.invalidate()
                    UIView.transition(
                        with: self.continueWatchView,
                        duration: 0.5, options: .transitionCrossDissolve
                    ) {
                        self.continueWatchView.isHidden = false
                    }
                }
            })
        }
    }
    func stopVideo() {
        observer = nil
        player.pause()
        player.seek(to: .zero)
        movingContentView.bringSubviewToFront(thumbnailImageView)
    }
    @objc func soundButtonTapped() {
        player.isMuted = !player.isMuted
        delegate?.didSoundButtonTapped(self)
    }
}

// MARK: - Resource Loader Delegate
extension Self {
    func resourceLoader(_ resourceLoader: AVAssetResourceLoader,
                        shouldWaitForRenewalOfRequestedResource
                        renewalRequest: AVAssetResourceRenewalRequest) -> Bool {
        return shouldLoadOrRenewRequestedResource(resourceLoadingRequest: renewalRequest)
    }

    func resourceLoader(_ resourceLoader: AVAssetResourceLoader,
                        shouldWaitForLoadingOfRequestedResource
                        loadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        return shouldLoadOrRenewRequestedResource(resourceLoadingRequest: loadingRequest)
    }

    func shouldLoadOrRenewRequestedResource(resourceLoadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        guard let url = resourceLoadingRequest.request.url?.absoluteString else {
            return false
        }
        let urlWithToken = URL(string: "\(url)?token=\(videoToken)")!
        guard let urlData = try? Data(contentsOf: urlWithToken) else { return false }

        resourceLoadingRequest.dataRequest?.respond(with: urlData)
        resourceLoadingRequest.finishLoading()

        return true
    }
}
