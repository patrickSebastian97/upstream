//
//  HomeSubmissionViewModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 15/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

class HomeSubmissionViewModel: ViewModel<SubmissionModel> {
    var id: String!
    var image: String!
    var isSelected: Bool!

    override var item: SubmissionModel! {
        didSet {
            id = item.id
            image = item.user?.avatar?.image ?? ""
            isSelected = false
        }
    }
}
