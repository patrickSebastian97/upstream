//
//  HomeSubmissionCell.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 15/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import LBTATools

class HomeSubmissionCell: LBTAListCell<HomeSubmissionViewModel> {

    // MARK: - Properties

    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.withSize(.init(width: 35, height: 35))
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = Asset.Color.white.color.cgColor
        imageView.layer.cornerRadius = 33.0 / 2.0

        return imageView
    }()
    lazy var gradientBackgroundView: CommonView = {
        let view = CommonView()
        view.withSize(.init(width: 40, height: 40))
        view.usingGradient = true
        view.secondColor = Asset.Color.red.color
        view.firstColor = Asset.Color.yellow.color
        view.radii = 20
        view.affectAllCorner = true

        return view
    }()
    override var item: HomeSubmissionViewModel! {
        didSet {
            imageView.loadImage(url: item.image)
        }
    }

    // MARK: - Life Cyclese
    override func setupViews() {
        super.setupViews()

        contentView.addSubview(gradientBackgroundView)
        gradientBackgroundView.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        gradientBackgroundView.addSubview(imageView)
        imageView.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
    }
}
