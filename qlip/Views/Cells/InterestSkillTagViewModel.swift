//
//  InterestSkillTagViewModel.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 02/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

class InterestSkillTagViewModel: ViewModel<InterestSkillTagModel> {
    var id: String!
    var name: String!
    var imageWhiteUrl: String!
    var imageBlackUrl: String!
    var slug: String!
    var isSelected: Bool = false

    override var item: InterestSkillTagModel! {
        didSet {
            id = item.id ?? ""
            name = item.name ?? ""
            if let imageBlackUrl = item.imageBlack {
                self.imageBlackUrl = imageBlackUrl
            }
            if let imageWhiteUrl = item.imageWhite {
                self.imageWhiteUrl = imageWhiteUrl
            }
            slug = item.slug ?? ""
        }
    }
}
