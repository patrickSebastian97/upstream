//
//  ChooseSkillCell.swift
//  qlip
//
//  Created by Rahman Bramantya on 23/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import LBTATools
import SnapKit

class ChooseSkillCell: UICollectionViewCell {
    lazy var skillNameLabel: UILabel = {
            let label = UILabel(
                text: "",
                font: .heavy14,
                textColor: Asset.Color.black.color,
                textAlignment: .center,
                numberOfLines: 1
            )

            return label
        }()
    lazy var imageView: UIImageView = {
            let image = UIImageView()
            image.contentMode = .scaleAspectFill
            image.withSize(.init(width: 20, height: 20))
            return image
        }()

    lazy var skillView: UIView = {
            let view = UIView()
            view.layer.borderWidth = 1
            view.layer.borderColor = Asset.Color.black.color.cgColor
            view.layer.cornerRadius = self.layer.frame.height / 2
            view.vstack(
                UIView().hstack(imageView,skillNameLabel,spacing: 4),
                alignment: .center
            ).withMargins(
                .init(
                    top: 10,
                    left: 20,
                    bottom: 10,
                    right: 20
                )
            )

            return view
        }()
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
    }

    func setupViews() {
        addSubview(skillView)
        skillView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
