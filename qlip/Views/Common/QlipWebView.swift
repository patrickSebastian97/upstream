//
//  QlipWebView.swift
//  qlip
//
//  Created by Amelia Lim on 29/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import WebKit

private typealias `Self` = QlipWebView

protocol QlipWebViewDelegate: class {
    func qlipWebView (
        _ qlipWebView: QlipWebView,
        didFinishWithHeight height: CGFloat
    )
}

class QlipWebView: WKWebView {

    override init(frame: CGRect, configuration: WKWebViewConfiguration) {
        super.init(frame: frame, configuration: configuration)

        self.navigationDelegate = self
        self.backgroundColor = .clear
        self.scrollView.backgroundColor = .clear
        self.isOpaque = false
        self.scrollView.isScrollEnabled = false
    }
    weak var qlipWebDelegate: QlipWebViewDelegate?

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func loadHTMLWithMetaData(string: String) {
        self.showLoading()
        loadHTMLString(string.htmlWithMetaData, baseURL: nil)
    }

}

// MARK: - WK Navigation Delegate
extension Self: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        webView.showLoading()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        webView.hideLoading()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.hideLoading()
        webView.frame.size.height = 1
        webView.frame.size = webView.scrollView.contentSize
        webView.evaluateJavaScript("document.documentElement.scrollHeight") { (height, _) in
            webView.withHeight(height as! CGFloat)
            self.qlipWebDelegate?.qlipWebView(self, didFinishWithHeight: height as! CGFloat)
        }
    }
}
