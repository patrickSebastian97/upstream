//
//  InfoPopup.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import LBTATools
import SnapKit

private typealias `Self` = InfoPopup

class InfoPopup: BaseView {

    // MARK: - Properties

    static var shared: InfoPopup = {
       return InfoPopup()
    }()

    typealias ActionCallback = () -> Void

    enum AnimationType {
        case open
        case close
    }
    struct ActionButton {
        let title: String
        let action: ActionCallback?
        let color: UIColor?
    }

    let overlayView: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect.init(style: .systemUltraThinMaterialDark))
        return view
    }()
    let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = UIStyle.CornerRadius.radius24
        view.layer.masksToBounds = true

        return view
    }()
    lazy var iconImageView: UIImageView = {
        let view = UIImageView(image: nil, contentMode: .scaleAspectFit)

        return view
    }()
    lazy var titleLabel: UILabel = {
        let view = UILabel(
            font: .heavy20,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 0
        )
        view.lineBreakMode = .byWordWrapping

        return view
    }()
    lazy var messageLabel: UILabel = {
        let view = UILabel(
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )
        view.lineBreakMode = .byWordWrapping

        return view
    }()
    lazy var actionButton: Button = {
        let view = Button(title: "", type: .yellow)
        view.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)
        return view
    }()
    lazy var skipButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(Asset.Color.blue.color, for: .normal)
        button.titleLabel?.font = .medium14
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.numberOfLines = 0
        button.addTarget(self, action: #selector(skipButtonTapped), for: .touchUpInside)

        return button
    }()

    lazy var clickAnywhereInstructionLabel: UILabel = {
        let view = UILabel(text: "",
                           font: .medium12,
                           textColor: Asset.Color.white.color.withAlphaComponent(0.7),
                           textAlignment: .center,
                           numberOfLines: 0)

        return view
    }()

    var actionButtonCallback: ActionCallback?
    var skipButtonCallback: ActionCallback?

    // MARK: - Initializers

    override func setupView() {
        super.setupView()

        self.alpha = 0

        addSubview(overlayView)
        overlayView.fillSuperview()

        let button = UIButton()
        addSubview(button)
        button.fillSuperview()
        button.addTarget(self, action: #selector(dismissTapped), for: .touchUpInside)

        contentView.vstack(
            contentView.hstack(iconImageView).padTop(UIStyle.Inset.inset20),
            contentView.hstack(titleLabel).padTop(UIStyle.Inset.inset12),
            contentView.hstack(messageLabel).withMargins(.topBottomSides(UIStyle.Inset.inset12)),
            contentView.hstack(actionButton.withHeight(UIStyle.Button.height))
                .padTop(UIStyle.Inset.inset24),
            contentView.hstack(skipButton).padTop(UIStyle.Inset.inset16)
        ).withMargins(.allSides(UIStyle.Inset.inset24))
        addSubview(contentView)
        contentView.snp.makeConstraints { maker in
            maker.centerY.equalToSuperview()
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset24)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset24)
        }

        addSubview(clickAnywhereInstructionLabel)
        clickAnywhereInstructionLabel.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.top.equalTo(contentView.snp.bottom).inset(-UIStyle.Inset.inset24)
        }
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        guard superview != nil else {
            return
        }
        self.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

// MARK: - Helpers
extension Self {
    private func addToWindowView() {
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }

        window?.addSubview(self)
    }

    private func animate(type: AnimationType, completion: (() -> Void)? = nil) {
        UIView.animate(
            withDuration: CATransaction.animationDuration(),
            animations: {
                switch type {
                case .open:
                    self.alpha = 1
                case .close:
                    self.alpha = 0
                }
        }, completion: {_ in
            completion?()
        })
    }

    private func setupData(
        image: UIImage,
        title: String,
        message: String,
        actionButton: ActionButton,
        clickAnywhereToDismiss: Bool = false
    ) {
        iconImageView.image = image
        titleLabel.text = title
        messageLabel.text = message
        self.actionButton.setTitle(actionButton.title, for: .normal)
        actionButtonCallback = actionButton.action
        self.actionButton.isHidden = false
        if actionButton.color == Asset.Color.red.color {
            self.actionButton.backgroundColor = actionButton.color
            self.actionButton.setTitleColor(Asset.Color.white.color, for: .normal)
            self.actionButton.layer.borderColor = actionButton.color?.cgColor
        }

        self.clickAnywhereInstructionLabel.isHidden = !clickAnywhereToDismiss
    }

    private func setupData(
        image: String,
        title: String,
        message: NSAttributedString,
        actionButton: ActionButton,
        clickAnywhereToDismiss: Bool = false
    ) {
        iconImageView.loadImage(url: image)
        titleLabel.text = title
        messageLabel.attributedText = message
        self.actionButton.setTitle(actionButton.title, for: .normal)
        actionButtonCallback = actionButton.action
        self.actionButton.isHidden = false
        if actionButton.color == Asset.Color.red.color {
            self.actionButton.backgroundColor = actionButton.color
            self.actionButton.setTitleColor(Asset.Color.white.color, for: .normal)
            self.actionButton.layer.borderColor = actionButton.color?.cgColor
        }

        self.clickAnywhereInstructionLabel.isHidden = !clickAnywhereToDismiss
    }

    private func setupData(
        image: UIImage,
        title: String,
        attributedMessage: NSAttributedString,
        actionButton: ActionButton,
        clickAnywhereToDismiss: Bool = false
    ) {
        iconImageView.image = image
        titleLabel.text = title
        messageLabel.attributedText = attributedMessage
        self.actionButton.setTitle(actionButton.title, for: .normal)
        actionButtonCallback = actionButton.action
        self.actionButton.isHidden = false
        self.actionButton.backgroundColor = actionButton.color
        self.clickAnywhereInstructionLabel.isHidden = !clickAnywhereToDismiss
    }

    private func setupData(
        image: UIImage?,
        title: String,
        message: String,
        actionButton: ActionButton,
        skipButton: ActionButton,
        clickAnywhereToDismiss: Bool = false
    ) {
        if let image = image {
            iconImageView.image = image
        } else {
            iconImageView.removeFromSuperview()
        }
        if skipButton.title == "" {
            self.skipButton.removeFromSuperview()
        }
        titleLabel.text = title
        messageLabel.text = message
        self.actionButton.setTitle(actionButton.title, for: .normal)
        self.actionButton.isHidden = false
        self.actionButton.backgroundColor = actionButton.color
        actionButtonCallback = actionButton.action
        self.skipButton.setTitle(skipButton.title, for: .normal)
        self.skipButton.isHidden = false
        skipButtonCallback = skipButton.action
        self.clickAnywhereInstructionLabel.isHidden = !clickAnywhereToDismiss
    }

    func show(
        image: UIImage,
        title: String,
        message: String,
        actionButton: ActionButton,
        clickAnywhereToDismiss: Bool = false
    ) {
        setupData(
            image: image,
            title: title,
            message: message,
            actionButton: actionButton,
            clickAnywhereToDismiss: clickAnywhereToDismiss
        )
        addToWindowView()
        animate(type: .open)
    }

    func show(
        image: String,
        title: String,
        message: NSAttributedString,
        actionButton: ActionButton,
        clickAnywhereToDismiss: Bool = false
    ) {
        setupData(
            image: image,
            title: title,
            message: message,
            actionButton: actionButton,
            clickAnywhereToDismiss: clickAnywhereToDismiss)
        addToWindowView()
        animate(type: .open)
    }

    func show(
        image: UIImage,
        title: String,
        attributedMessage: NSAttributedString,
        clickAnywhereToDismiss: Bool = false,
        actionButton: ActionButton
    ) {
        setupData(
            image: image,
            title: title,
            attributedMessage: attributedMessage,
            actionButton: actionButton,
            clickAnywhereToDismiss: clickAnywhereToDismiss
        )
        addToWindowView()
        animate(type: .open)
    }

    func show(
        image: UIImage?,
        title: String,
        message: String,
        actionButton: ActionButton,
        skipButton: ActionButton,
        clickAnywhereToDismiss: Bool = false
    ) {
        setupData(
            image: image,
            title: title,
            message: message,
            actionButton: actionButton,
            skipButton: skipButton,
            clickAnywhereToDismiss: clickAnywhereToDismiss
        )
        addToWindowView()
        animate(type: .open)
    }

    func showSkillPopup(
        image: String,
        title: String,
        message: String
    ) {
        iconImageView.loadImage(url: image)
        titleLabel.text = title
        messageLabel.text = message
        self.actionButton.isHidden = true
        self.clickAnywhereInstructionLabel.isHidden = false
        addToWindowView()
        animate(type: .open)
    }

    func dismiss(completion: (() -> Void)? = nil) {
        animate(type: .close, completion: {
            self.removeFromSuperview()
            completion?()
        })
    }

}

// MARK: - Actions
extension Self {
    @objc private func actionButtonTapped() {
        dismiss(completion: {
            self.actionButtonCallback?()
        })
    }

    @objc private func dismissTapped() {
        if !clickAnywhereInstructionLabel.isHidden {
                dismiss()
        }
    }

    @objc private func skipButtonTapped() {
        dismiss(completion: {
            self.skipButtonCallback?()
        })
    }
}
