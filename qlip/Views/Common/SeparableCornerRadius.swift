//
//  SeparableCornerRadius.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

protocol SeparableCornerRadius: class {
    var radii: CGFloat { get set }
    var affectAllCorner: Bool { get set }
    var affectTopLeftCorner: Bool { get set }
    var affectTopRightCorner: Bool { get set }
    var affectBottomLeftCorner: Bool { get set }
    var affectBottomRightCorner: Bool { get set }

    func updateCornerRadius()
}

extension SeparableCornerRadius where Self: UIView {
    func updateCornerRadius() {
        var corners: UIRectCorner = []

        if affectAllCorner {
            corners = [.allCorners]
        } else {
            if affectTopLeftCorner {
                corners.insert(.topLeft)
            }

            if affectTopRightCorner {
                corners.insert(.topRight)
            }

            if affectBottomLeftCorner {
                corners.insert(.bottomLeft)
            }

            if affectBottomRightCorner {
                corners.insert(.bottomRight)
            }
        }

        guard !corners.isEmpty && radii != 0 else {
            return
        }

        let layer = CAShapeLayer()
        let path = UIBezierPath(
            roundedRect: self.bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: radii, height: radii))

        layer.frame = self.bounds
        layer.path = path.cgPath

        self.layer.mask = layer
    }
}
