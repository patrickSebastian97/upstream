//
//  LineView.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

class LineView: BaseView {
    override func setupView() {
        super.setupView()

        backgroundColor = Asset.Color.blackAlpha20.color
        self.withHeight(1)
    }
}
