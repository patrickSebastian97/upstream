//
//  Shadowable.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

protocol Shadowable: class {
    var shadowLayer: CAShapeLayer { get set }
    var usingShadow: Bool { get set }
    var shadowOffsetX: Int { get set }
    var shadowOffsetY: Int { get set }
    var shadowBlur: CGFloat { get set }
    var shadowOpacity: Float { get set }

    func updateShadow()
}

extension Shadowable where Self: UIView {
    func updateShadow() {
        shadowLayer.removeFromSuperlayer()

        guard usingShadow else {
            return
        }

        shadowLayer = {
            let layer = CAShapeLayer()
            layer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: shadowBlur).cgPath
            layer.fillColor = self.backgroundColor?.cgColor
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowPath = layer.path
            layer.shadowOffset = CGSize(width: shadowOffsetX, height: shadowOffsetY)
            layer.shadowOpacity = shadowOpacity
            layer.shadowRadius = 4
            return layer
        }()

        self.layer.insertSublayer(shadowLayer, at: 0)

        // Make sure subviews are shown properly.
        for view in self.subviews {
            self.bringSubviewToFront(view)
        }
    }
}
