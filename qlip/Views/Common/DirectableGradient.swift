//
//  DirectableGradient.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

protocol DirectableGradient: class {
    var startPoints: [(x: Double, y: Double)] { get }
    var endPoints: [(x: Double, y: Double)] { get }
    var angleIndexMap: [Int] { get }

    var gradientLayer: CAGradientLayer { get set }
    var usingGradient: Bool { get set }
    var angle: Int { get set }
    var firstColor: UIColor { get set }
    var secondColor: UIColor { get set }
    var gradientCornerRadius: CGFloat { get set }

    func updateGradient()
}

/// It only supports 0, 45, 90, 135, 180, 225, 270, 315.
/// 0   : From bottom to top.
/// 45  : From bottom left to top right.
/// 90  : From left to right.
/// and so on..
extension DirectableGradient where Self: UIView {
    var startPoints: [(x: Double, y: Double)] {
        return [
            (x: 0.5, y: 1),
            (x: 0, y: 1),
            (x: 0, y: 0.5),
            (x: 0, y: 0),
            (x: 0.5, y: 0),
            (x: 1, y: 0),
            (x: 1, y: 0.5),
            (x: 1, y: 1)
        ]
    }

    var endPoints: [(x: Double, y: Double)] {
        return [
            (x: 0.5, y: 0),
            (x: 1, y: 0),
            (x: 1, y: 0.5),
            (x: 1, y: 1),
            (x: 0.5, y: 1),
            (x: 0, y: 1),
            (x: 0, y: 0.5),
            (x: 0, y: 0)
        ]
    }

    var angleIndexMap: [Int] {
        return [0, 45, 90, 135, 180, 225, 270, 315]
    }

    func updateGradient() {
        gradientLayer.removeFromSuperlayer()

        guard usingGradient else {
            return
        }

        gradientLayer = {
            let layer = CAGradientLayer()
            layer.frame = self.bounds
            layer.colors = [firstColor.cgColor, secondColor.cgColor]
            layer.cornerRadius = gradientCornerRadius

            let desiredIndex = angleIndexMap.firstIndex(of: angle) ?? 2
            layer.startPoint = CGPoint(x: startPoints[desiredIndex].x, y: startPoints[desiredIndex].y)
            layer.endPoint = CGPoint(x: endPoints[desiredIndex].x, y: endPoints[desiredIndex].y)

            return layer
        }()

        self.layer.addSublayer(gradientLayer)

        // Make sure subviews are shown properly.
        for view in self.subviews {
            self.bringSubviewToFront(view)
        }
    }
}
