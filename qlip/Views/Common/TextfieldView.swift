//
//  TextfieldView.swift
//  qlip
//
//  Created by Rahman Bramantya on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//
//  swiftlint:disable file_length

import UIKit

@objc protocol TextFieldDelegate: class {
    @objc optional func textField(shouldReturn textField: TextFieldView) -> Bool
    @objc optional func textField(didValidate textField: TextFieldView)
    @objc optional func textField(shouldBeginEditing textField: TextFieldView) -> Bool
    @objc optional func textfield(onTapRightButton textField: TextFieldView)
    @objc optional func textfield(onFullTouch textField: TextFieldView)
}

private typealias `Self` = TextFieldView

class TextFieldView: BaseView, UITextFieldDelegate {

    // MARK: - Properties

    enum ValidationType {
        case required
        case email
        case minLength(length: Int)
        case maxLength(length: Int)
        case password
        case alphabet
        case username
        case whitespace
        case phonenumber
        case separetedByComma
    }

    struct Validation {
       let type: ValidationType
       let message: String
    }

    enum KeyboardType: Int {
        case normal = 0
        case email = 1
        case password = 2
        case phonePad = 3
        case number = 4
    }

    // swiftlint:disable identifier_name
    var _placeholder: String?
    // swiftlint:enable identifier_name

    var rightImage: UIImage? {
        didSet {
            if let rightImage = rightImage {
                let button = UIButton(type: .system)
                button.frame = CGRect(
                    x: 8, y: 0, width: rightImage.size.width, height: rightImage.size.height
                )
                button.setImage(rightImage.withRenderingMode(.alwaysOriginal), for: .normal)
                button.addTarget(self, action: #selector(rightButtonTapped), for: .touchUpInside)
                let view = CGRect(x: 0, y: 0, width: UIStyle.Inset.inset48, height: button.frame.height)
                let paddingView = UIView(frame: view)
                paddingView.addSubview(button)
                textField.rightView = paddingView
                textField.rightViewMode = .always
            } else {
                textField.rightView = nil
                textField.rightViewMode = .never
            }
        }
    }

    var leftImage: UIImage? {
        didSet {
            if let leftImage = leftImage {
                let button = UIButton(type: .system)
                button.frame = CGRect(
                    x: UIStyle.Inset.inset16,
                    y: 0,
                    width: leftImage.size.width,
                    height: leftImage.size.height
                )
                button.setImage(leftImage.withRenderingMode(.alwaysOriginal), for: .normal)
                let frame = CGRect(x: 0, y: 0, width: UIStyle.Inset.inset48, height: button.frame.height)
                let view = UIView(frame: frame)
                view.addSubview(button)
                textField.leftView = view
                textField.leftViewMode = .always
            } else {
                textField.leftView = nil
                textField.leftViewMode = .never
            }
        }
    }

    var usingPhonePad: Bool? {
        didSet {
            if usingPhonePad != nil {
                let label = UILabel(
                    text: APIConstants.CountryCode.code,
                    font: .medium16,
                    textColor: Asset.Color.black.color,
                    textAlignment: .left,
                    numberOfLines: 1
                )
                let frame = CGRect(x: 0, y: 0, width: UIStyle.Inset.inset48, height: 30)
                let view = UIView(frame: frame)
                let line = UIView()
                line.withWidth(2)
                line.backgroundColor = Asset.Color.blackAlpha20.color
                view.hstack(label,
                            line,
                            spacing: UIStyle.Inset.inset8
                ).withMargins(.leftRight(
                                left: UIStyle.Inset.inset16,
                                right: UIStyle.Inset.inset8
                ))
                textField.leftView = view
                textField.leftViewMode = .always
            } else {
                textField.leftView = nil
                textField.rightViewMode = .never
            }
        }
    }

    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()

    lazy var textField: UITextField = {
        let textField = UITextField()
        let leftPadding = CGRect(x: 0, y: 0, width: UIStyle.Inset.inset16, height: textField.frame.height)
        let rightPadding = CGRect(x: 0, y: 0, width: UIStyle.Inset.inset16, height: textField.frame.height)
        let leftPaddingView = UIView(frame: leftPadding)
        let rightPaddingView = UIView(frame: rightPadding)

        textField.layer.cornerRadius = UIStyle.CornerRadius.radius24
        textField.clipsToBounds = true
        textField.layer.borderWidth = 0.5
        textField.leftView = leftPaddingView
        textField.rightView = rightPaddingView
        textField.leftViewMode = .always
        textField.rightViewMode = .always
        textField.delegate = self
        textField.backgroundColor = Asset.Color.white.color
        textField.layer.borderColor = Asset.Color.blackAlpha20.color.cgColor
        textField.font = .medium16
        textField.textColor = Asset.Color.black.color
        textField.addTarget(self, action: #selector(editingChanged(_:)), for: .allEditingEvents)

        return textField
    }()

    lazy var errorLabel : UILabel = {
        let view = UILabel(text: "",
                           font: .book12,
                           textColor: Asset.Color.red.color,
                           textAlignment: .left,
                           numberOfLines: 2)
        view.lineBreakMode = .byWordWrapping
        return view
    }()

    lazy var minLenghtIndicatorView: UIView = {
        let view = UIView(backgroundColor: Asset.Color.blackAlpha20.color)
        view.withSize(.init(width: 10.0, height: 10.0))
        view.layer.cornerRadius = UIStyle.CornerRadius.radius5

        return view
    }()
    lazy var minLengthLabel: UILabel = {
        let label = UILabel(
            text: L10n.passwordLengthValidation(SettingValidation.Password.minLength),
            font: .medium12,
            textColor: Asset.Color.blackAlpha20.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var capitalNumberIndicatorView: UIView = {
        let view = UIView(backgroundColor: Asset.Color.blackAlpha20.color)
        view.withSize(.init(width: 10.0, height: 10.0))
        view.layer.cornerRadius = UIStyle.CornerRadius.radius5

        return view
    }()
    lazy var capitalNumberLabel: UILabel = {
        let label = UILabel(
            text: L10n.passwordCapitalValidation,
            font: .medium12,
            textColor: Asset.Color.blackAlpha20.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    var isUsingValidationView: Bool = false {
        didSet {
            setupView()
        }
    }

    private lazy var fullTouchButton: UIButton = {
        let view = UIButton(type: .system)
        view.addTarget(self, action: #selector(fullTouchButtonTapped), for: .touchUpInside)

        return view
    }()

    var returnKeyType: UIReturnKeyType {
        set { textField.returnKeyType = newValue }
        get { return textField.returnKeyType }
    }

    var isFullTouchButton: Bool = false {
        didSet {
            fullTouchButton.isHidden = !isFullTouchButton
        }
    }

    var datePicker: UIDatePicker? {
        didSet {
            if let datePicker = datePicker {
                textField.inputView = datePicker
            }
        }
    }

    var toolbar: UIToolbar? {
        didSet {
            if let toolbar = toolbar {
                textField.inputAccessoryView = toolbar
            }
        }
    }

    var text: String? {
        get { return textField.text }
        set {
            textField.text = newValue
            validate()
        }
    }

    var placeholder: String? {
        get {
            return _placeholder
        }
        set {
           _placeholder = newValue
           textField.placeholder = _placeholder
        }
    }

    var errorMessage: String? {
        get { return errorLabel.text }
        set {
            errorLabel.text = newValue
        }
    }

    var borderColor: CGColor? {
        didSet {
            if let borderColor = borderColor {
                textField.layer.borderColor = borderColor
            }
        }
    }

    var backgroundTextColor: UIColor? {
        didSet {
            if let backgroundTextColor = backgroundTextColor {
                textField.backgroundColor = backgroundTextColor
            }
        }
    }

    var keyboardType: KeyboardType = .normal {
        didSet {
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.isSecureTextEntry = false
            switch keyboardType {
            case .normal:
                textField.keyboardType = .default
            case .email:
                textField.keyboardType = .emailAddress
            case .password:
                textField.keyboardType = .asciiCapable
                textField.isSecureTextEntry = true
                rightImage = Asset.Authentication.passwordShow.image
            case .phonePad:
                textField.keyboardType = .phonePad
                usingPhonePad = true
            case .number:
                textField.keyboardType = .numberPad
            }
        }
    }

    private var validations: [Validation] = []
    weak var textFieldDelegate: TextFieldDelegate?

    override func setupView() {
        super.setupView()

        if isUsingValidationView {

            let minLengthWrapper = UIView()
            minLengthWrapper.addSubview(minLenghtIndicatorView)
            minLenghtIndicatorView.snp.makeConstraints { (maker) in
                maker.top.bottom.equalToSuperview()
                maker.trailing.leading.equalToSuperview().inset(UIStyle.Inset.inset4)
            }
            let capitalWrapper = UIView()
            capitalWrapper.addSubview(capitalNumberIndicatorView)
            capitalNumberIndicatorView.snp.makeConstraints { (maker) in
                maker.top.trailing.leading.bottom.equalToSuperview().inset(UIStyle.Inset.inset4)
            }
            vstack(
                hstack(titleLabel),
                hstack(textField.withHeight(UIStyle.Button.height)),
                hstack(
                    minLengthWrapper,
                    minLengthLabel,
                    spacing: UIStyle.Inset.inset8
                ).padTop(UIStyle.Inset.inset8),
                hstack(
                    capitalWrapper,
                    capitalNumberLabel,
                    spacing: UIStyle.Inset.inset8
                ),
                spacing: UIStyle.Inset.inset12
            )
        } else {
            vstack(
                hstack(titleLabel).padBottom(UIStyle.Inset.inset8),
                hstack(textField.withHeight(UIStyle.Button.height)),
                hstack(errorLabel).padTop(UIStyle.Inset.inset8)
            )
            addSubview(fullTouchButton)
            fullTouchButton.snp.makeConstraints { maker in
                maker.top.equalTo(textField.snp.top)
                maker.leading.equalTo(textField.snp.leading)
                maker.bottom.equalTo(textField.snp.bottom)
                maker.trailing.equalTo(textField.snp.trailing)
            }
        }
    }

    public convenience init(
        title: String?,
        placeholder: String?,
        keyboardType :KeyboardType
    ) {
        self.init()
        if let title = title {
            titleLabel.text = title
        }
        if placeholder != nil {
            textField.placeholder = placeholder
        }
        self.setKeyboard(type: keyboardType)
        setFullTouchButton(false)
    }

    // MARK: - Text Field Delegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textFieldDelegate?.textField?(shouldReturn: self) ?? true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return textFieldDelegate?.textField?(shouldBeginEditing: self) ?? true
    }

}

// MARK: - helpers
extension Self {

    private func setKeyboard(type: KeyboardType) {
        keyboardType = type
    }

    private func setFullTouchButton(_ isFullTouch: Bool) {
        isFullTouchButton = isFullTouch
    }

    func hasError() -> Bool {
        let empty = errorMessage?.isEmpty ?? true
        return !empty
    }

    func isEmpty() -> Bool {
        let text = textField.text ?? ""
        return text.isEmpty
    }

    func setValidations(validations: [Validation]) {
        self.validations = validations
    }

    func validate(type: ValidationType, textToValidate: String) -> Bool {
        switch type {
        case .required:
            return !textToValidate.isEmpty
        case .minLength(length: let length):
            return textToValidate.count >= length
        case .maxLength(length: let length):
            return textToValidate.count <= length
        case .email:
            return Validator.isValidEmail(emailAddressString: textToValidate)
        case .password:
            let pattern = "^[a-zA-Z0-9\\S+$]*[A-Z0-9]+[a-zA-Z0-9\\S+$]*$"
            return Validator.isValid(pattern: pattern, text: textToValidate)
        case .alphabet:
            let pattern = "^[a-zA-Z ]+$"
            return Validator.isValid(pattern: pattern, text: textToValidate)
        case .username:
            let pattern = "^[a-zA-Z0-9\\.\\_]+$"
            return Validator.isValid(pattern: pattern, text: textToValidate)
        case .whitespace:
            let pattern = "^[\\S+$]+$"
            return Validator.isValid(pattern: pattern, text: textToValidate)
        case .phonenumber:
            let pattern = "^[0-9]+$"
            return Validator.isValid(pattern: pattern, text: textToValidate)
        case .separetedByComma:
            let pattern = "^[a-zA-Z \\,]+$"
            return Validator.isValid(pattern: pattern, text: textToValidate)
        }
    }

    private func validate() {
        let text = self.textField.text ?? ""
        errorLabel.text = ""
        if validations.isEmpty { return }

        for index in 0..<validations.count {
            let validation = validations[index]
            let isValid: Bool = validate(type: validation.type, textToValidate: text)
            if !isValid {
                textField.layer.borderColor = Asset.Color.red.color.cgColor
                textField.layer.backgroundColor = Asset.Color.white.color.cgColor
                errorLabel.text = validation.message
                break
            }
        }
    }

    func setPasswordValidate(minLengthValid: Bool, capitalValid: Bool) {
        if minLengthValid {
            minLenghtIndicatorView.backgroundColor = Asset.Color.green.color
            minLengthLabel.textColor = Asset.Color.blackAlpha70.color
        } else {
            minLenghtIndicatorView.backgroundColor = Asset.Color.blackAlpha20.color
            minLengthLabel.textColor = Asset.Color.blackAlpha20.color
        }
        if capitalValid {
            capitalNumberIndicatorView.backgroundColor = Asset.Color.green.color
            capitalNumberLabel.textColor = Asset.Color.blackAlpha70.color
        } else {
            capitalNumberIndicatorView.backgroundColor = Asset.Color.blackAlpha20.color
            capitalNumberLabel.textColor = Asset.Color.blackAlpha20.color
        }
    }

    // MARK: - Public Function Helpers

    func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event) {
        textField.addTarget(target, action: action, for: controlEvents)
    }
}
// MARK: - Actions
extension Self {
    @objc func editingChanged(_ sender: UITextField) {
        if sender.text?.isEmpty ?? false {
            sender.layer.borderColor = Asset.Color.blackAlpha70.color.cgColor
            sender.backgroundColor = Asset.Color.white.color
            errorMessage = ""
            textFieldDelegate?.textField?(didValidate: self)
            return
        }
        if sender.isEditing {
            if sender.hasText {
                sender.layer.borderColor = Asset.Color.blue.color.cgColor
                sender.backgroundColor = Asset.Color.white.color
            } else {
                sender.layer.borderColor = Asset.Color.blackAlpha70.color.cgColor
                sender.backgroundColor = Asset.Color.white.color
                errorLabel.text = ""
            }
        } else {
            if sender.hasText {
                sender.layer.borderColor = Asset.Color.blue.color.cgColor
                sender.backgroundColor = Asset.Color.skyBlueAlpha3.color
            } else {
                sender.layer.borderColor = Asset.Color.blackAlpha70.color.cgColor
                sender.backgroundColor = Asset.Color.white.color
                errorLabel.text = ""
            }
        }
        validate()
        textFieldDelegate?.textField?(didValidate: self)
    }

    @objc func rightButtonTapped() {
        textFieldDelegate?.textfield?(onTapRightButton: self)

        let disableImage = Asset.Authentication.passwordHide.image
        let enableImage = Asset.Authentication.passwordShow.image
        if keyboardType == .password {
            textField.isSecureTextEntry = !textField.isSecureTextEntry
            rightImage = textField.isSecureTextEntry ? enableImage : disableImage
        }
    }

    @objc private func fullTouchButtonTapped() {
        textFieldDelegate?.textfield?(onFullTouch: self)
    }
}

final class Validator {

    static func isValidEmail(emailAddressString: String) -> Bool {
        var returnValue = true
        // let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailRegEx =
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
                "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"

        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString,
                                        range: NSRange(location: 0, length: nsString.length))
            if results.count < 1 {
                returnValue = false
            }
        } catch {
            returnValue = false
        }

        return  returnValue
    }

    static func isValid(pattern: String, text: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern)
            let nsString = text as NSString
            let results = regex.matches(
                in: text, range: NSRange(location: 0, length: nsString.length)
            )
            if results.count < 1 {
                return false
            }
            return true
        } catch {
            return false
        }
    }
}
