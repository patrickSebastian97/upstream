//
//  CommonView.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

class CommonView: BaseView, Shadowable, SeparableCornerRadius, DirectableGradient {
    // MARK: - Directable Gradient Properties

    lazy var gradientLayer = CAGradientLayer()

    @IBInspectable var usingGradient: Bool = false {
        didSet {
            self.updateGradient()
        }
    }

    @IBInspectable var angle: Int = 0 {
        didSet {
            self.updateGradient()
        }
    }

    @IBInspectable var firstColor: UIColor = .clear {
        didSet {
            self.updateGradient()
        }
    }

    @IBInspectable var secondColor: UIColor = .clear {
        didSet {
            self.updateGradient()
        }
    }

    @IBInspectable var gradientCornerRadius: CGFloat = 0 {
        didSet {
            self.updateGradient()
        }
    }

    // MARK: - Shadowable Properties

    lazy var shadowLayer = CAShapeLayer()

    @IBInspectable var bgColor: UIColor = .white {
        didSet {
            self.backgroundColor = bgColor
        }
    }

    @IBInspectable var usingShadow: Bool = false {
        didSet {
            self.updateShadow()
        }
    }

    @IBInspectable var shadowOffsetX: Int = 0 {
        didSet {
            self.updateShadow()
        }
    }

    @IBInspectable var shadowOffsetY: Int = 0 {
        didSet {
            self.updateShadow()
        }
    }

    @IBInspectable var shadowBlur: CGFloat = 0 {
        didSet {
            self.updateShadow()
        }
    }

    @IBInspectable var shadowOpacity: Float = 1 {
        didSet {
            self.updateShadow()
        }
    }

    // MARK: - Separable Corner Radius Properties

    @IBInspectable var radii: CGFloat = 0 {
        didSet {
            self.updateCornerRadius()
        }
    }

    @IBInspectable var affectAllCorner: Bool = false {
        didSet {
            self.updateCornerRadius()
        }
    }

    @IBInspectable var affectTopLeftCorner: Bool = false {
        didSet {
            self.updateCornerRadius()
        }
    }

    @IBInspectable var affectTopRightCorner: Bool = false {
        didSet {
            self.updateCornerRadius()
        }
    }

    @IBInspectable var affectBottomLeftCorner: Bool = false {
        didSet {
            self.updateCornerRadius()
        }
    }

    @IBInspectable var affectBottomRightCorner: Bool = false {
        didSet {
            self.updateCornerRadius()
        }
    }

    // MARK: - Life Cycles

    override func layoutSubviews() {
        super.layoutSubviews()
        self.updateGradient()
        self.updateShadow()
        self.updateCornerRadius()
    }
}
