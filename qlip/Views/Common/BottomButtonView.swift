//
//  BottomButtonView.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

class BottomButtonView: BaseView {
    // MARK: - Properties

    lazy var actionButton: Button = {
        let view = Button(title: "", type: .yellow)

        return view
    }()

    override func setupView() {
        super.setupView()

        vstack(
            actionButton.withHeight(UIStyle.Button.height)
        ).withMargins(
            .init(top: UIStyle.Inset.top,
                  left: 0,
                  bottom: UIStyle.Inset.bottom,
                  right: 0)
        )
    }

    convenience public init(title: String, type: Button.ButtonType, backgroundColor: UIColor? = .clear) {
        self.init()

        actionButton.text = title
        actionButton.type = type
        self.backgroundColor = backgroundColor
    }
}
