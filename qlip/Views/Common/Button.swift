//
//  Button.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

class Button: UIButton {
    // MARK: - Properties

    var didChangeEnableState: ((_ isEnable: Bool) -> Void)?

    enum ButtonType {
        case yellow
        case white
        case circularYellow
        case clear
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        //self.updateButtonType()
    }

    var type: ButtonType = .yellow {
        didSet {
            updateButtonType()
        }
    }

    var isStyleEnabled: Bool = false {
        didSet {
            if !isStyleEnabled {
                setButtonDisabled()
            } else {
                updateButtonType()
            }
            isEnabled = isStyleEnabled
            didChangeEnableState?(isEnabled)
        }
    }

    var text: String? {
        get { return self.title(for: .normal) }
        set { self.setTitle(newValue, for: .normal) }
    }

    var imageButtonSpacing: CGFloat = 0

    override var intrinsicContentSize: CGSize {
        let superIntrinsicContentSize = super.intrinsicContentSize
        let spacing: CGFloat = 40
        let addedSpacing = spacing + imageButtonSpacing
        let extraSpacing = addedSpacing
        let width = superIntrinsicContentSize.width + extraSpacing

        return CGSize(width: width, height: superIntrinsicContentSize.height)
    }

    // MARK: - Initializers

    convenience public init(title: String, type: ButtonType) {
        self.init(type: .system)

        self.setTitle(title, for: .normal)
        self.setType(type)
        isStyleEnabled = true
    }

    // MARK: - Private Functions

    private func setType(_ type: ButtonType) {
        self.type = type
    }

    private func setButtonDisabled() {
        self.alpha = 0.2
    }

    // swiftlint:disable function_body_length
    private func updateButtonType() {

        // MARK: - Custom Button Inner Shadow
        let innerShadow = CALayer()
        innerShadow.frame = bounds
        let radius = self.frame.size.height/2
        let path = UIBezierPath(roundedRect: innerShadow.bounds.insetBy(dx: 1, dy: 1), cornerRadius:radius)
        let cutout = UIBezierPath(roundedRect: innerShadow.bounds, cornerRadius:radius).reversing()
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        innerShadow.shadowColor = UIColor.black.cgColor
        innerShadow.shadowOffset = CGSize(width: 0, height: -2)
        innerShadow.shadowOpacity = 0.25
        innerShadow.shadowRadius = 1.5
        innerShadow.cornerRadius = self.frame.size.height/2

        switch type {
        case .yellow:
            setTitleColor(Asset.Color.black.color, for: .normal)
            layer.cornerRadius = self.frame.size.height/2
            backgroundColor = Asset.Color.yellow.color
            layer.borderWidth = 1
            layer.borderColor = Asset.Color.yellow.color.cgColor
            layer.cornerRadius = UIStyle.Button.cornerRadius
            layer.masksToBounds = false
            titleLabel?.font = .heavy16
            //self.layer.addSublayer(innerShadow)
            self.alpha = 1
        case .white:
            setTitleColor(Asset.Color.black.color, for: .normal)
            backgroundColor = Asset.Color.white.color
            layer.cornerRadius = UIStyle.Button.cornerRadius
            layer.borderWidth = 1
            layer.borderColor = Asset.Color.black.color.cgColor
            layer.masksToBounds = true
            titleLabel?.font = .heavy16
            self.alpha = 1
        case .circularYellow:
            setImage(Asset.General.nextButton.image.withRenderingMode(.alwaysOriginal), for: .normal)
            backgroundColor = Asset.Color.yellow.color
            withSize(.init(width: UIStyle.Button.height, height: UIStyle.Button.height))
            layer.borderWidth = 1
            layer.borderColor = Asset.Color.yellow.color.cgColor
            layer.cornerRadius = UIStyle.Button.cornerRadius
            layer.masksToBounds = false

            layer.shadowColor = Asset.Color.black.color.cgColor
            layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
            layer.shadowRadius = 1
            layer.shadowOpacity = 0.1
            self.alpha = 1
        case .clear:
            setTitleColor(Asset.Color.white.color, for: .normal)
            backgroundColor = UIColor.clear
            layer.cornerRadius = UIStyle.Button.cornerRadius
            layer.borderWidth = 1
            layer.borderColor = Asset.Color.white.color.cgColor
            layer.masksToBounds = true
            titleLabel?.font = .heavy16
        }
    }
    //swiftlint:enable function_body_length
}
