//
//  PopupHelper.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension UIViewController {
    func showTodoPopup(
        message: String = "TODO"
    ) {
        let alert = UIAlertController(
            title: "Information",
            message: message,
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    func showInfoPopup(
        image: UIImage,
        title: String,
        message: String,
        action: InfoPopup.ActionCallback? = nil
    ) {
        let popup = InfoPopup()
        let action = InfoPopup.ActionButton(title: "OK", action: action, color: nil)
        popup.show(image: image, title: title, message: message, actionButton: action)
    }

    func showAttributedInfoPopup(
        image: UIImage,
        title: String,
        attributedMessage: NSAttributedString,
        action: InfoPopup.ActionCallback? = nil
    ) {
        let popup = InfoPopup()
        let action = InfoPopup.ActionButton(title: "OK", action: action, color: nil)
        popup.show(image: image, title: title, attributedMessage: attributedMessage, actionButton: action)
    }

    func showWarningInfoPopup(
        image: String,
        title: String,
        message: NSAttributedString,
        buttonTitle: String,
        color: UIColor,
        action: InfoPopup.ActionCallback? = nil
    ) {
        let popup = InfoPopup()
        let action = InfoPopup.ActionButton(title: buttonTitle, action: action, color: color)
        popup.show(
            image:image,
            title: title,
            message: message,
            actionButton: action,
            clickAnywhereToDismiss: true
        )
    }
    func showWarningInfoPopup(
        title: String,
        message: String,
        action: InfoPopup.ActionCallback? = nil
    ) {
        let popup = InfoPopup()
        let action = InfoPopup.ActionButton(title: "okay", action: action, color: nil)
        popup.show(image: Asset.Authentication.welcome.image,
                   title: title, message: message, actionButton: action)
    }

    //swiftlint:disable function_parameter_count
    func showGeneralInfoPopup(
        image: UIImage?,
        title: String,
        message: String,
        actionButtonTitle: String,
        skipButtonTitle: String,
        clickAnywhereToDismiss: Bool,
        action: InfoPopup.ActionCallback? = nil,
        skip: InfoPopup.ActionCallback? = nil
    ) {
        let popup = InfoPopup()
        let action = InfoPopup.ActionButton(
            title: actionButtonTitle,
            action: action,
            color: Asset.Color.yellow.color
        )
        let skip = InfoPopup.ActionButton(
            title: skipButtonTitle,
            action: skip,
            color: nil
        )
        popup.show(
            image: image,
            title: title,
            message: message,
            actionButton: action,
            skipButton: skip,
            clickAnywhereToDismiss: clickAnywhereToDismiss
        )
    }
}
