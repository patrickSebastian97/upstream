//
//  HomeFeedListHeaderView.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 15/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

private typealias `Self` = HomeFeedListHeaderView

protocol homeFeedListHeaderViewDelegate:class {
    func homeFeedListHeaderView(_ homeFeedListHeaderView: HomeFeedListHeaderView)
}

class HomeFeedListHeaderView: UICollectionReusableView {

    // MARK: - Properties

    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: L10n.videoChallenge,
            font: .heavy20,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var pickerView: ToolbarPickerView = {
        let view = ToolbarPickerView()
        view.delegate = self
        view.dataSource = self
        view.toolbarDelegate = self
        view.selectRow(Storage.PickerHomeFeed.getPickerIndex(), inComponent: 0, animated: false)
        view.reloadAllComponents()

        return view
    }()
    lazy var sortFeedTextfield: UITextField = {
        let textfield = UITextField()
        textfield.inputView = self.pickerView
        textfield.delegate = self
        textfield.tintColor = .clear
        textfield.text = Storage.PickerHomeFeed.getPickerIndex() == 0 ? L10n.popularOption : L10n.newestOption
        textfield.font = .medium14
        textfield.textAlignment = .right
        textfield.inputAccessoryView = self.pickerView.toolbar

        textfield.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: Asset.General.arrowDownButton.name)
        imageView.image = image
        textfield.rightView = imageView

        return textfield
    }()
    private let challengeSelections: [String] = [L10n.popularOption, L10n.newestOption]
    weak var delegate: homeFeedListHeaderViewDelegate?

    // MARK: - Life Cycles
    override init(frame: CGRect) {
        super.init(frame: frame)

        vstack(
            hstack(titleLabel,
                   sortFeedTextfield,
                   distribution: .fillProportionally
            ).withMargins(.bottom(UIStyle.Inset.inset16))
        )
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension Self: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(
        _ pickerView: UIPickerView,
        numberOfRowsInComponent component: Int
    ) -> Int {
        return challengeSelections.count
    }
    func pickerView(
        _ pickerView: UIPickerView,
        titleForRow row: Int,
        forComponent component: Int
    ) -> String? {
        return challengeSelections[row]
    }
}

extension Self: UITextFieldDelegate {
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        return false
    }
}

extension Self: ToolbarPickerViewDelegate {
    func didTapDone() {
        let row = self.pickerView.selectedRow(inComponent: 0)
        self.pickerView.selectRow(row, inComponent: 0, animated: false)
        self.sortFeedTextfield.text = self.challengeSelections[row]
        Storage.PickerHomeFeed.savePickerIndex(index: row)
        delegate?.homeFeedListHeaderView(self)
        self.sortFeedTextfield.resignFirstResponder()
    }
}
