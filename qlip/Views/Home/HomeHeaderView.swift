//
//  HomeHeaderView.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 13/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

protocol HomeHeaderViewDelegate: class {
    func homeHeaderView(
        didButtonGuestTapped homeHeaderView: HomeHeaderView
    )
}

class HomeHeaderView: BaseView {

    // MARK: - Properties

    lazy var nameLabel: UILabel = {
       let label = UILabel(
        text: "Hi, Abi!",
        font: .heavy14,
        textColor: Asset.Color.black.color,
        textAlignment: .left,
        numberOfLines: 1
       )

        return label
    }()
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.withSize(.init(width: UIStyle.Inset.inset40, height: UIStyle.Inset.inset40))
        imageView.layer.cornerRadius = UIStyle.CornerRadius.radius20
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true

        //delete soon
        imageView.backgroundColor = .lightGray

        return imageView
    }()
    lazy var levelLabel: UILabel = {
        let label = UILabel(
            text: L10n.numberLevel(999),
            font: .heavy8,
            textColor: Asset.Color.white.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var levelBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Color.green.color
        view.layer.shadowColor = Asset.Color.black.color.cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 4

        return view
    }()
    lazy var coinLabel: UILabel = {
       let label = UILabel(
        text: L10n.numberQoin(999999),
        font: .heavy12,
        textColor: Asset.Color.black.color,
        textAlignment: .center,
        numberOfLines: 1
       )

        return label
    }()
    lazy var coinImageView: UIImageView = {
        let imageView = UIImageView(
            image: Asset.General.moneyIcon.image.withRenderingMode(.alwaysOriginal),
            contentMode: .scaleAspectFit
        )

        return imageView
    }()
    lazy var coinBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Color.yellow.color
        view.layer.shadowColor = Asset.Color.yellow.color.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 0, height: 6)
        view.layer.shadowRadius = 8

        return view
    }()
    lazy var loginGuestButton: UIButton = {
        let button = UIButton()
        button.setTitle(L10n.login, for: .normal)
        button.titleLabel?.font = .medium14
        button.setTitleColor(Asset.Color.blue.color, for: .normal)
        button.tintColor = Asset.Color.blue.color
        button.contentHorizontalAlignment = .left

        button.addTarget(self, action: #selector(loginGuestTapped), for: .touchUpInside)

        return button
    }()
    weak var homeHeaderDelegate: HomeHeaderViewDelegate?

    // MARK: - Life Cycles

    override func setupView() {
        super.setupView()

        backgroundColor = Asset.Color.white.color

        if ParentRealmModel.isLoggedIn() {
            setupKidLoginView()
        } else if KidRealmModel.isLoggedIn() {
            setupKidLoginView()
        } else if Storage.User.getUserType() == UserType.kid.rawValue {
            setupKidGuestView()
        } else if Storage.User.getUserType() == UserType.parent.rawValue {
            setupKidGuestView()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        levelBackgroundView.layer.cornerRadius = levelBackgroundView.frame.height / 2
        coinBackgroundView.layer.cornerRadius = coinBackgroundView.frame.height / 2
    }

    // MARK: - Helpers

    func setupKidLoginView() {
        addSubview(imageView)
        imageView.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        addSubview(levelBackgroundView)
        levelBackgroundView.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(imageView.snp.centerX)
            maker.bottom.equalTo(imageView.snp.bottom)
        }
        levelBackgroundView.addSubview(levelLabel)
        levelLabel.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(2)
            maker.leading.trailing.equalToSuperview().inset(7)
        }
        addSubview(coinBackgroundView)
        coinBackgroundView.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset12)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        coinBackgroundView.addSubview(coinImageView)
        coinImageView.snp.makeConstraints { (maker) in
            maker.centerY.equalToSuperview()
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset12)
        }
        coinBackgroundView.addSubview(coinLabel)
        coinLabel.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.leading.equalTo(coinImageView.snp.trailing).inset(-UIStyle.Inset.inset8)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (maker) in
            maker.centerY.equalToSuperview()
            maker.leading.equalTo(imageView.snp.trailing).inset(-UIStyle.Inset.inset8)
        }
    }

    func setupKidGuestView() {
        coinBackgroundView.alpha = 0.2
        coinLabel.text = L10n.numberQoin(0)
        addSubview(loginGuestButton)
        loginGuestButton.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.centerY.equalToSuperview()
        }
        addSubview(coinBackgroundView)
        coinBackgroundView.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset12)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        coinBackgroundView.addSubview(coinImageView)
        coinImageView.snp.makeConstraints { (maker) in
            maker.centerY.equalToSuperview()
            maker.leading.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        coinBackgroundView.addSubview(coinLabel)
        coinLabel.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.leading.equalTo(coinImageView.snp.trailing).inset(-UIStyle.Inset.inset8)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
    }

    // MARK: - Actions

    @objc private func loginGuestTapped() {
        homeHeaderDelegate?.homeHeaderView(didButtonGuestTapped: self)
    }
}
