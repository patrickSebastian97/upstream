//
//  BaseView.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit

class BaseView: UIView {

    // MARK: - Open Functions

    open func setupView() {}
    open func setupActions() {}

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupActions()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupView()
        setupActions()
    }

    init() {
        super.init(frame: CGRect.zero)

        setupView()
        setupActions()
    }

}
