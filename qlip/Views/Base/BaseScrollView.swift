//
//  BaseScrollView.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import SnapKit

class BaseScrollView: UIScrollView {

    // MARK: - Properties

    lazy var contentView: UIView = {
        let contentView = UIView()

        return contentView
    }()

    // MARK: - Iniliazers

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        setup()
    }

    private func setup() {
        addSubview(contentView)
        contentView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
            maker.height.equalToSuperview().priority(.low)
            maker.width.equalToSuperview()
        }
    }

    func addSubviewToContentView(_ view: UIView) {
        contentView.addSubview(view)
    }

}
