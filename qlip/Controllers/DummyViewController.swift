//
//  DummyViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 11/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

class DummyViewController: BaseViewController {

    override var backImage: UIImage? { return nil }

    // MARK: - Properties

    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: "Hello There!",
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: "You success to login",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var logoutButton: BottomButtonView = {
        let button = BottomButtonView(title: "Logout", type: .white)
        button.actionButton.addTarget(self, action: #selector(logoutTapped), for: .touchUpInside)

        return button
    }()
    lazy var informationLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .center,
            numberOfLines: 0
        )

        return label
    }()
    var userType: UserType = UserType(rawValue: Storage.User.getUserType()) ?? .kid

    override func viewDidLoad() {
        super.viewDidLoad()

        switch userType {
        case .kid:
            let kid = KidRealmModel.getUser()
            informationLabel.text = "\(kid.id) \(kid.fullName) \(String(describing: kid.username))"
        case .parent:
            let parent = ParentRealmModel.getUser()
            informationLabel.text = "\(parent.id) \(parent.fullName) \(String(describing: parent.username))"
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
}

extension DummyViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        let view = scrollView.contentView

        view.vstack(
            view.hstack(titleLabel),
            view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(informationLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(logoutButton).padTop(UIStyle.Inset.inset64)
        ).withMargins(
            .init(
                top: UIStyle.Inset.defaultTop,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )
    }

    @objc private func logoutTapped() {
        switch userType {
        case .kid:
            AppHelper.logoutKid()
        case .parent:
            AppHelper.logoutParent()
        }
        goToCoreValuesScreen()
    }
}
