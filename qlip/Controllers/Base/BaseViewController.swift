//
//  BaseViewController.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import NavKit

/// Base View Controller
class BaseViewController: UIViewController, CustomizableNavigation, UIGestureRecognizerDelegate {

    // MARK: - Properties

    lazy var scrollView: BaseScrollView = {
        let scrollView = BaseScrollView()

        return scrollView
    }()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    open var enablePreferredStatusBarColor: Bool { return true }
    open var preferredStatusBarColor: UIColor { return .clear}

    // MARK: - Navigation Configs

    var titleFont: UIFont { return .heavy20 }
    var titleColor: UIColor { return Asset.Color.black.color }
    var barBackgroundColor: UIColor { return view.backgroundColor ?? .clear }
    var backImage: UIImage? { return Asset.General.backButton.image }
    var isBarUsingBottomShadow: Bool { return false }
    var backInset: UIEdgeInsets? { return .zero }
    var isUsingInteractivePopGesture: Bool { return true }

    // MARK: - Open Variables

    open var shouldAttachScrollView: Bool { return true }

    // MARK: - Open Functions

    @objc dynamic open func initializeUI() {}
    @objc dynamic open func updateViews() {}
    @objc dynamic open func setupViews() {}
    @objc dynamic open func setupActions() {}
    @objc dynamic open func fetchData() {}
    @objc dynamic open func updateButtonState() {}

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        if shouldAttachScrollView {
            attachScrollView()
        }
        initializeUI()
        setupViews()
        setupActions()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateNavigation()

        updateViews()

        updateStatusBar()
    }

    // MARK: - View Creation

    private func attachScrollView() {
        view.addSubview(scrollView)
        scrollView.fillSuperviewSafeAreaLayoutGuide()
    }

    // MARK: - Private Functions

    fileprivate func updateStatusBar() {
        let tag: Int = 726264
        if #available(iOS 13, *) {
            // remove current status bar
            UIApplication.shared.windows.first { $0.isKeyWindow }?.viewWithTag(tag)?.removeFromSuperview()
        } else {
            UIApplication.shared.statusBarView?.backgroundColor = .clear
        }

        if enablePreferredStatusBarColor {
            if #available(iOS 13, *) {
                let statusBarManager = UIApplication.shared.windows.first {
                    $0.isKeyWindow }?.windowScene?.statusBarManager
                guard let statusBarFrame = statusBarManager?.statusBarFrame else {
                    return
                }
                let statusBar = UIView(frame: statusBarFrame)
                statusBar.tag = tag
                statusBar.backgroundColor = preferredStatusBarColor
                UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(statusBar)
            } else {
                UIApplication.shared.statusBarView?.backgroundColor = preferredStatusBarColor
            }
        }
    }
}

extension UIViewController {
    @discardableResult
    func fadeFromColor(fromColor: UIColor, toColor: UIColor, withPercentage: CGFloat) -> UIColor {
        var fromRed: CGFloat = 0.0
        var fromGreen: CGFloat = 0.0
        var fromBlue: CGFloat = 0.0
        var fromAlpha: CGFloat = 0.0

        fromColor.getRed(&fromRed, green: &fromGreen, blue: &fromBlue, alpha: &fromAlpha)

        var toRed: CGFloat = 0.0
        var toGreen: CGFloat = 0.0
        var toBlue: CGFloat = 0.0
        var toAlpha: CGFloat = 0.0

        toColor.getRed(&toRed, green: &toGreen, blue: &toBlue, alpha: &toAlpha)

        //calculate the actual RGBA values of the fade colour
        let red = (toRed - fromRed) * withPercentage + fromRed
        let green = (toGreen - fromGreen) * withPercentage + fromGreen
        let blue = (toBlue - fromBlue) * withPercentage + fromBlue
        let alpha = (toAlpha - fromAlpha) * withPercentage + fromAlpha

        // return the fade colour
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 987654321
            guard let keyWindow = UIApplication.shared.windows.first(where: { $0.isKeyWindow }),
                let statusBarFrame = keyWindow.windowScene?.statusBarManager?.statusBarFrame else {
                return nil
            }

            if let statusBarView = keyWindow.viewWithTag(tag) {
                return statusBarView
            }

            let statusBar = UIView()
            statusBar.frame = statusBarFrame
            keyWindow.addSubview(statusBar)

            return statusBar
        } else {
            return value(forKey: "statusBar") as? UIView
        }
    }
}

extension UIImage {
    func imageWithColor(_ color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()

        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)

        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}

// MARK: - Scroll View Delegate
extension BaseViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {}
}
