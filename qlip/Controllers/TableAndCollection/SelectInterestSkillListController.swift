//
//  SelectInterestSkillListController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 02/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import LBTATools

private typealias `Self` = SelectInterestSkillListController

protocol SelectInterestSkillListControllerDelegate: class {
    func selectInterestSKillListController(
        _ selectInteresSkillListController: SelectInterestSkillListController,
        didSelectAt index: Int
    )
}

class SelectInterestSkillListController:
    LBTAListController<InterestSkillTagCell, InterestSkillTagViewModel> {

    // MARK: - Properties
    lazy var layout: AlignedCollectionViewFlowLayout = {
        let layout = AlignedCollectionViewFlowLayout()
        layout.horizontalAlignment = .left
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.minimumLineSpacing = UIStyle.Inset.inset12
        layout.minimumInteritemSpacing = UIStyle.Inset.inset12

        return layout
    }()
    let cellId: String = "cellId"
    weak var selectInterestSkillDelegate: SelectInterestSkillListControllerDelegate?

    // MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .clear
        collectionView.clipsToBounds = false
        collectionView.isDirectionalLockEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isScrollEnabled = false
    }

    override func viewDidLayoutSubviews() {
        let height = collectionView.contentSize.height
        if height > 0 {
            self.view.withHeight(height)
        }
    }
}

// MARK: - Collection View Data Source
extension Self {
    override func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: cellId,
            for: indexPath
        ) as! InterestSkillTagCell
        cell.item = items[indexPath.item]

        return cell
    }
}

extension Self {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? InterestSkillTagCell else { return }
        cell.isClicked = cell.isClicked ? false : true
        selectInterestSkillDelegate?.selectInterestSKillListController(
            self, didSelectAt: indexPath.item
        )
    }
}
