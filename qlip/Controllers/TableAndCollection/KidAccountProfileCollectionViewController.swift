//
//  KidAccountProfileCollectionViewController.swift
//  qlip
//
//  Created by Rahman Bramantya on 22/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import LBTATools

private typealias `Self` = KidAccountProfileSetupCollectionViewController

protocol KidAccountProfileSetupListControllerDelegate: class {
    func didSelect(_ viewController: KidAccountProfileSetupCollectionViewController, index: Int)
}

class KidAccountProfileSetupCollectionViewController: LBTAListController<KidAccountProfileSetupCell,
ChooseKidProfileViewModel> {

    private let rows: CGFloat = 3.0
    lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical

        return layout
    }()
    weak var kidDelegate: KidAccountProfileSetupListControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .clear
        collectionView.isScrollEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return UIStyle.Inset.inset4
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        return UIStyle.Inset.inset4
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let delegate = kidDelegate else { return }
        delegate.didSelect(self, index: indexPath.row)
    }
}

// MARK: - UI Collection View Delegate Flow Layout

extension Self: UICollectionViewDelegateFlowLayout {

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let insets = collectionView.contentInset.left + collectionView.contentInset.right
        let totalSpacing = (rows - 1) * UIStyle.Inset.inset4
        let width = (collectionView.frame.width - totalSpacing - insets) / rows
        return .init(
            width: width,
            height: width * 1.5
        )
    }
}
