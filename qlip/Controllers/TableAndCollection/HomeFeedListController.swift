//
//  HomeFeedListController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import LBTATools

private typealias `Self` = HomeFeedListController

protocol HomeFeedListControllerDelegate: class {
    func homeFeedListController(_ homeFeedListController: HomeFeedListController)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                didLikeTappedAt index: Int,
                                isLikeTapped: Bool)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                didShareTappedAt index: Int)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                addSubmissionTapped index: Int)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                otherSubmissionTapped index: Int)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                didFeedsSelectAt feedsIdx: Int,
                                didSubmissionSelectAt subsIdx: Int)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                didScroll index: Int)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                willBeginDragging index: Int)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                didEndDragging index: Int)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                didEndDeclarating index: Int)
    func homeFeedListController(_ homeFeedListController: HomeFeedListController,
                                didSelectItemAt index: Int)
}

class HomeFeedListController: LBTAListHeaderController<
                                HomeFeedCell, HomeFeedViewModel, HomeFeedListHeaderView>,
                              UICollectionViewDelegateFlowLayout {

    // MARK: - Properties

    private let cellId: String = "cellId"
    private let headerView: String = "headerView"
    private var currentPage: Int = 0
    private var cellDescriptionHeight: CGFloat = 145
    var listReaction: [Int: Bool] = [:]
    var firstVideoUrl: String = ""

    let vSpace: CGFloat = 16.0

    weak var delegate: HomeFeedListControllerDelegate?

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView.isScrollEnabled = true
        collectionView.contentInset = .init(
            top: UIStyle.Inset.inset64,
            left: UIStyle.Inset.inset16,
            bottom: UIStyle.Inset.inset32,
            right: UIStyle.Inset.inset16
        )
        collectionView.collectionViewLayout = layout
        collectionView.register(HomeFeedListHeaderView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: headerView
        )
        collectionView.backgroundColor = Asset.Color.white.color
        collectionView.clipsToBounds = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        collectionView.reloadData()
    }

    // MARK: - Collection Data Source

    override func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: cellId,
            for: indexPath) as? HomeFeedCell else {
            return UICollectionViewCell()
        }
        cell.delegate = self
        cell.tag = indexPath.row
        cell.item = items[indexPath.row]
        cell.continueWatchView.isHidden = true
        for (key, value) in listReaction where key == cell.tag {
            if value == true {
                cell.likeButton.setImage(Asset.General.likeActiveHome.image, for: .normal)
                cell.totalReaction += 1
            }
            if value == false {
                cell.likeButton.setImage(Asset.General.likeInactiveHome.image, for: .normal)
                cell.totalReaction -= 1
            }
            cell.totalLikeLabel.text = String(cell.totalReaction)
        }
        let isActive = Storage.SoundState.getSoundState()
        cell.soundButton.setImage(
            isActive ? Asset.General.soundEnable.image : Asset.General.soundDisable.image,
            for: .normal
        )
        cell.likeButton.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
        cell.shareButton.addTarget(self, action: #selector(shareButtonTapped(_:)), for: .touchUpInside)
        cell.addSubmissionButton.addTarget(
            self, action: #selector(addSubmissionTapped(_:)), for: .touchUpInside)
        cell.otherSubmissionLabelButton.addTarget(
            self, action: #selector(otherSubmissionTapped(_:)), for: .touchUpInside)
        if cell.tag == 0 {
            cell.playVideo(videoUrl: firstVideoUrl)
        }
        return cell
    }
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        delegate?.homeFeedListController(self, willBeginDragging: self.currentPage)
    }
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.homeFeedListController(self, didEndDeclarating: self.currentPage)
    }
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        delegate?.homeFeedListController(self, didEndDragging: self.currentPage)
    }
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.currentPage = calculateCurrentPage(scrollView)
        delegate?.homeFeedListController(self, didScroll: self.currentPage)
    }

    override func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath) -> UICollectionReusableView {

        let headerView = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: self.headerView, for: indexPath) as! HomeFeedListHeaderView
        headerView.delegate = self

        return headerView
    }
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let leftInset = collectionView.contentInset.left
        let rightInset = collectionView.contentInset.right
        let width: CGFloat = (collectionView.frame.width - leftInset - rightInset)
        return .init(
            width: width,
            height: width + cellDescriptionHeight
        )
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return vSpace
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        return vSpace
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        return .init(width: collectionView.frame.width, height: 56)
    }
    override func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        delegate?.homeFeedListController(self, didSelectItemAt: indexPath.row)
    }
}

extension HomeFeedListController: homeFeedListHeaderViewDelegate {
    func homeFeedListHeaderView(_ homeFeedListHeaderView: HomeFeedListHeaderView) {
        delegate?.homeFeedListController(self)
        homeFeedListHeaderView.pickerView.resignFirstResponder()
    }
}

extension HomeFeedListController: HomeFeedCellDelegate {
    func homeFeedCell(_ homeFeedCell: HomeFeedCell, didSelectSubmissionAt index: Int) {
        delegate?.homeFeedListController(
            self, didFeedsSelectAt: homeFeedCell.tag, didSubmissionSelectAt: index)
    }
    func didSoundButtonTapped(_ homeFeedCell: HomeFeedCell) {
        let isActive = Storage.SoundState.getSoundState()
        Storage.SoundState.setSoundState(isActive: !isActive)
        collectionView.indexPathsForVisibleItems.forEach { indexPath in
            let cell = collectionView.cellForItem(
                at: IndexPath(row: indexPath.row, section: 0)
            ) as! HomeFeedCell
            cell.soundButton.setImage(
                !isActive ? Asset.General.soundEnable.image: Asset.General.soundDisable.image,
                for: .normal
            )
        }
    }
}

// MARK: - Helpers
extension Self {
    private func calculateCurrentPage(_ scrollView: UIScrollView) -> Int {
        let leftInset = collectionView.contentInset.left
        let rightInset = collectionView.contentInset.right
        let width: CGFloat = (collectionView.frame.width - leftInset - rightInset)
        let cellHeight = width + cellDescriptionHeight
        let index = scrollView.contentOffset.y / cellHeight
        let roundedIndex = Int(round(index))
        return roundedIndex
    }
}

// MARK: - Actions
extension Self {
    @objc private func likeButtonTapped(_ button: UIButton) {
        guard let indexPath = getIndexPathFromCollectionView(
                button: button, collectionView: collectionView
        ) else { return }
        if !ParentRealmModel.isLoggedIn() && !KidRealmModel.isLoggedIn() {
            delegate?.homeFeedListController(self, didLikeTappedAt: 0, isLikeTapped: false)
            return
        }
        let cell = collectionView.cellForItem(
            at: IndexPath(row: indexPath.row, section: 0)
        ) as! HomeFeedCell
        if cell.isLike {
            cell.likeButton.setImage(Asset.General.likeInactiveHome.image, for: .normal)
            cell.totalReaction -= 1
            cell.isLike = false
            listReaction[indexPath.row] = false
        } else {
            cell.likeButton.setImage(Asset.General.likeActiveHome.image, for: .normal)
            cell.totalReaction += 1
            cell.isLike = true
            listReaction[indexPath.row] = true
        }
        cell.totalLikeLabel.text = String(cell.totalReaction)
        delegate?.homeFeedListController(self, didLikeTappedAt: indexPath.row, isLikeTapped: cell.isLike)
    }
    @objc private func shareButtonTapped(_ button: UIButton) {
        guard let indexPath = getIndexPathFromCollectionView(
                button: button, collectionView: collectionView
        ) else { return }
        delegate?.homeFeedListController(self, didShareTappedAt: indexPath.row)
    }
    @objc private func addSubmissionTapped(_ button: UIButton) {
        guard let indexPath = getIndexPathFromCollectionView(
                button: button, collectionView: collectionView
        ) else { return }
        delegate?.homeFeedListController(self, addSubmissionTapped: indexPath.row)
    }
    @objc private func otherSubmissionTapped(_ button: UIButton) {
        guard let indexPath = getIndexPathFromCollectionView(
                button: button, collectionView: collectionView
        ) else { return }
        delegate?.homeFeedListController(self, otherSubmissionTapped: indexPath.row)
    }
}
