//
//  HomeSubmissionListController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 15/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import LBTATools

protocol HomeSubmissionListControllerDelegate: class {
    func homeSubmissionListController(
        _ homeSubmissionListController: HomeSubmissionListController,
        didSubmissionSelectAt index: Int
    )
}

class HomeSubmissionListController: LBTAListController<HomeSubmissionCell, HomeSubmissionViewModel> {

    // MARK: - Properties

    let cellId: String = "cellId"

    lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0

        return layout
    }()
    weak var delegate: HomeSubmissionListControllerDelegate?

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .clear
        collectionView.isScrollEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        collectionView.reloadData()
    }

    override func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: cellId,
            for: indexPath) as? HomeSubmissionCell else {
            return UICollectionViewCell()
        }
        cell.item = items[indexPath.row]

        return cell
    }
    override func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        delegate?.homeSubmissionListController(self, didSubmissionSelectAt: indexPath.row)
    }
}
extension HomeSubmissionListController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        return CGSize(width: 45, height: 45)
    }
}
