//
//  CoreValuesVCLayout.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 15/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import LBTATools
import UIKit

extension CoreValuesViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        view.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().inset(UIStyle.Inset.inset64+100)
            maker.centerX.equalToSuperview()
        }
        view.addSubview(asKidButton)
        asKidButton.snp.makeConstraints { (maker) in
            maker.bottom.equalToSuperview().inset(UIStyle.Inset.inset64+60)
            maker.centerX.equalToSuperview().inset(UIStyle.Inset.inset48)
        }
        view.addSubview(asParentButton)
        asParentButton.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(asKidButton.snp.bottom).inset(UIStyle.Inset.inset64)
            maker.centerX.equalToSuperview()
        }
    }
}
