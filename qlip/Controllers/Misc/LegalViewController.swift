//
//  LegalViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import PromiseKit

private typealias `Self` = LegalViewController

class LegalViewController: BaseViewController {

    // MARK: - Properties

    enum ScreenType {
        case parentConsent
        case termsConditions
        case privacyPolicy
        case aboutUs
    }

    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var notesLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var webview:QlipWebView = {
        let webView = QlipWebView()

        return webView
    }()
    var legalType: LegalType = .parentConsent

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }

    // MARK: - Helpers
    override func fetchData() {
        super.fetchData()
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Legal.getLegal(type: self.legalType)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            let legalViewModel = LegalViewModel(item: data)
            self.titleLabel.text = legalViewModel.title
            self.notesLabel.text = legalViewModel.note
            self.webview.loadHTMLWithMetaData(string: legalViewModel.content)
            self.webview.hideLoading()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
}
