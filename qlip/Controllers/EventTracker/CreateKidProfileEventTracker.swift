//
//  CreateKidProfileEventTracker.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 10/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension CreateKidProfileViewController {

    // MARK: - Event Tracker

    func parentSuccessRegistrationEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentSuccessRegistration, params: params)
    }
    func parentAddKidProfileFillKidNameEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentAddKidProfFillKidName, params: params)
    }
    func parentAddKidProfileFillBirthDateEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentAddKidProfFillBirthDate, params: params)
    }
    func parentClickCreateKidAccountEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentAddKidProfClickCreateKidAccount, params: params)
    }
    func parentAddKidProfileOpenOnboarding2EventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentAddKidProfOpenOnboarding2, params: params)
    }
}
