//
//  KidAccountProfieSetupEventTracker.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 10/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension KidAccountProfileSetupViewController {

    // MARK: - Event Tracking

    func kidSuccessRegistrationEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidSuccessRegistration, params: params)
    }
    func kidRegistrationChooseProfileEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidRegistrationChooseProfile, params: params)
    }
    func kidAddProfileOpenOnboarding1EventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidAddProfileOpenOnboarding1, params: params)
    }
}
