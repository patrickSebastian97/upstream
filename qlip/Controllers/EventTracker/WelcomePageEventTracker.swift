//
//  WelcomePageEventTracker.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 07/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension WelcomePageViewController {

    // MARK: - Event Tracking

    func clickLoginEventTracking() {
        switch userType {
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentClickLogin, params: params)
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidClickLogin, params: params)
        }
    }

    func clickRegisterEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidClickRegistration, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentClickRegistration, params: params)
        }
    }

    func clickTermsAgreementEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidClickTermsAndCondition, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentClickTermsAndCondition, params: params)
        }
    }

    func clickParentalConsentEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidClickParentalConsent, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentClickParentalConsent, params: params)
        }
    }

    func clickPrivacyPolicyEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidClickPrivacyPolicy, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentClickPrivacyPolicy, params: params)
        }
    }

}
