//
//  MakeNewPasswordEventTracking.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 10/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension MakeNewPasswordViewController {

    // MARK: - Event Trackin
    func parentForgotPasswordSuccessEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentForgotPasswordSuccessNewPass, params: params)
    }
    func kidForgotPasswordSuccessEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidForgotPasswordSuccessNewPassword, params: params)
    }
    func forgotPasswordFillNewEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidForgotPasswordFillNewPassword, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentForgotPasswordFillNewPassword, params: params)
        }
    }
    func forgotPasswordSubmitNewPasswordEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidForgotPasswordSubmitNewPassword, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentForgotPasswordSubmitNewPass, params: params)
        }
    }
}
