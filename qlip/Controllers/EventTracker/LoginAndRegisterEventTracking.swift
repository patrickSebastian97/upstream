//
//  LoginAndRegisterEventTracking.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 10/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension LoginAndRegisterViewController {

    // MARK: - Event Trackin
    func kidLoginSuccessEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidSuccessLogin, params: params)
    }
    func parentLoginSuccessEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentSuccessLogin, params: params)
    }
    // swiftlint:disable function_body_length
    func loginSocialMediaEventTracking(socialMediaType: SocialMediaType) {
        switch userType {
        case .kid:
            switch socialMediaType {
            case .apple:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .kidPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.kidLoginWithApple, params: params)
            case .facebook:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .kidPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.kidLoginWithFacebook, params: params)
            case .google:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .kidPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.kidLoginWithGoogle, params: params)
            }
        case .parent:
            switch socialMediaType {
            case .apple:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .parentPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.parentLoginWithApple, params: params)
            case .facebook:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .parentPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.parentLoginWithFacebook, params: params)
            case .google:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .parentPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.parentLoginWithGoogle, params: params)
            }
        }
    }
    func loginForgotPasswordEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidLoginForgotPassword, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentLoginForgotPassword, params: params)
        }
    }
    func registerClickRegistrationEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationClickRegistration, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationClickRegistration, params: params)
        }
    }
    func loginClickLoginEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidLoginClickLogin, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentLoginClickLogin, params: params)
        }
    }
    func regisSocialMediaEventTracking(socialMediaType: SocialMediaType) {
        switch userType {
        case .kid:
            switch socialMediaType {
            case .apple:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .kidPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.kidRegistrationWithApple, params: params)
            case .facebook:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .kidPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.kidRegistrationWithFacebook, params: params)
            case .google:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .kidPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.kidRegistrationWithGoogle, params: params)
            }
        case .parent:
            switch socialMediaType {
            case .apple:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .parentPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.parentRegistrationWithApple, params: params)
            case .facebook:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .parentPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.parentRegistrationWithFacebook, params: params)
            case .google:
                let params = EventTrackingManager.shared.setProperties(
                    [.propertyBasic,
                     .parentPropertyBasic
                    ].map(EventTrackingManager.Property.init)
                )
                Firebase.shared.setEventTracking(.parentRegistrationWithGoogle, params: params)
            }
        }
    }
    func regisFillNameEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationFillAccountName, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationFillAccountName, params: params)
        }
    }
    func regisFillPasswordEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationFillPassword, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationFillPassword, params: params)
        }
    }
    func loginFillPhoneNumberEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidLoginFillPhoneNumber, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentLoginFillPhoneNumber, params: params)
        }
    }
    func loginFillPasswordEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidLoginFillPassword, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentLoginFillPassword, params: params)
        }
    }
}
