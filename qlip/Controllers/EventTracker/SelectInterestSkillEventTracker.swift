//
//  SelectInterestSkillEventTracker.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 11/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension SelectInterestSkillViewController {

    // MARK: - Event Tracking

    func kidProfileSubmitActivityEventTracking(activity: [String]) {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic,
                 .propertyList(title: .kidTopicByKid, list: activity)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidAddProfileSubmitTopicProfile, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic,
                 .propertyList(title: .kidActivityByParent, list: activity)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentAddKidProfSubmitKidActivity, params: params)
        }
    }
    func parentAddKidProfileOpenOnboarding3EventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentAddKidProfOpenOnboarding3, params: params)
    }
    func kidAddProfileOpenOnboarding2EventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidAddProfileOpenOnboarding2, params: params)
    }
    func kidProfileSubmitSkillEventTracking(skill: [String]) {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic,
                 .propertyList(title: .kidSkillByParent, list: skill)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidAddProfileSubmitSkillProfile, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic,
                 .propertyList(title: .kidSkillByParent, list: skill)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentAddKidProfSubmitSkill, params: params)
        }
    }
}
