//
//  CoreValuesEventTracker.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 07/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension CoreValuesViewController {

    // MARK: - Event Tracking

    func openOnboardingEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.openOnboarding, params: params)
    }

    func parentStartEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentStart, params: params)
    }

    func kidStartEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidStart, params: params)
    }
}
