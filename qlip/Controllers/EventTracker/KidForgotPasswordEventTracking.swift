//
//  KidForgotPasswordEventTracker.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 10/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension KidForgotPasswordViewController {

    // MARK: - Event Trackin
    func kidForgotPasswordSubmitParentPhoneEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidForgotPasswordSubmitParentPhoneNumber, params: params)
    }
    func kidForgotPasswordFillParentPhoneEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidForgotPasswordFillParentPhoneNumber, params: params)
    }
}
