//
//  InsertPhoneNumberEventTracker.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 10/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension InsertPhoneNumberViewController {

    // MARK: - Event Tracking

    func kidSuccessRegistrationEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidSuccessRegistration, params: params)
    }
    func forgotPasswordFillPhoneNumberEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidForgotPasswordFillKidPhoneNumber, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentForgotPasswordFillPhoneNumber, params: params)
        }
    }
    func forgotPasswordSubmitPhoneEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidForgotPasswordSubmitKidPhoneNumber, params: params)
        case .parent:
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentForgotPasswordSubmitPhoneNumber, params: params)
        }
    }
    func forgotPasswordGetOTPEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidForgotPasswordGetOtp, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentForgotPasswordGetOtp, params: params)
        }
    }
    func registerFillPhoneNumberEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationFillKidPhoneNumber, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationFillPhoneNumber, params: params)
        }
    }
    func registerSubmitPhoneNumberEventTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationSubmitKidPhoneNumber, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationSubmitPhoneNumber, params: params)
        }
    }
    func registerGetOTPEvenetTracking() {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationKidPhoneGetOtp, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationGetOtp, params: params)
        }
    }
    func kidRegistrationFillParentPhoneNumberEventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .kidPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.kidRegistrationFillParentPhoneNumber, params: params)
    }
}
