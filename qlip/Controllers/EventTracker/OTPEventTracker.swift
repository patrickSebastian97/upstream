//
//  OTPEventTracker.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 11/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import Foundation

extension OTPViewController {

    // MARK: - Event Tracking

    func forgotPasswordInputOTPEventTracking(otp: String) {
        switch userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic,
                 .propertyForgotOTP(otp: otp)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidForgotPasswordInputOtp, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic,
                 .propertyForgotOTP(otp: otp)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentForgotPasswordInputOtp, params: params)
        }
    }
    func forgotPasswordResendOTPEventTracking(otp: String) {
        switch self.userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic,
                 .propertyForgotOTP(otp: otp)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidForgotPasswordResendOtp, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic,
                 .propertyForgotOTP(otp: otp)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentForgotPasswordResendOtp, params: params)
        }
    }
    func forgotPasswordSuccessVerificationOTPEventTracking(otp: String) {
        switch self.userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic,
                 .propertyForgotOTP(otp: otp)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidForgotPasswordSuccessVerificationOtp, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic,
                 .propertyForgotOTP(otp: otp)
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentForgotPasswordSuccessOtp, params: params)
        }
    }
    func registerInputOTPEventTracking() {
        switch self.userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationKidPhoneInputOtp, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationInputOtp, params: params)
        }
    }
    func registerResendOTPEventTracking() {
        switch self.userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationKidPhoneResendOtp, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationResendOtp, params: params)
        }
    }
    func registerSuccessVerificationOTPEventTracking() {
        switch self.userType {
        case .kid:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .kidPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.kidRegistrationSuccessKidPhoneOtp, params: params)
        case .parent:
            let params = EventTrackingManager.shared.setProperties(
                [.propertyBasic,
                 .parentPropertyBasic
                ].map(EventTrackingManager.Property.init)
            )
            Firebase.shared.setEventTracking(.parentRegistrationSuccessVerifyOtp, params: params)
        }
    }
    func parentAddKidProfileOpenOnboarding1EventTracking() {
        let params = EventTrackingManager.shared.setProperties(
            [.propertyBasic,
             .parentPropertyBasic
            ].map(EventTrackingManager.Property.init)
        )
        Firebase.shared.setEventTracking(.parentAddKidProfOpenOnboarding1, params: params)
    }
}
