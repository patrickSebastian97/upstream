//
//  ChallengeLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 21/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

extension ChallengeViewController {

    // MARK: - Initialize UI
    // swiftlint:disable function_body_length
    override func initializeUI() {
        super.initializeUI()

        view.addSubview(videoView)
        videoView.fillSuperview()

        view.addSubview(topShadowView)
        topShadowView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.leading.trailing.equalToSuperview()
            maker.height.equalTo(152)
        }
        view.addSubview(bottomShadow)
        bottomShadow.snp.makeConstraints { (maker) in
            maker.bottom.equalToSuperview()
            maker.leading.trailing.equalToSuperview()
            maker.height.equalTo(301)
        }
        let contentView = UIView()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { (maker) in
            maker.top.equalTo(contentView.superview!.safeAreaLayoutGuide.snp.top).inset(UIStyle.Inset.inset16)
            maker.leading.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.bottom.equalTo(
                contentView.superview!.safeAreaLayoutGuide.snp.bottom).inset(UIStyle.Inset.inset16)
        }
        contentView.addSubview(backButton)
        backButton.snp.makeConstraints { (maker) in
            maker.leading.top.equalToSuperview().inset(UIStyle.Inset.inset4)
        }
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(backButton.snp.top)
            maker.trailing.equalToSuperview()
            maker.leading.equalTo(backButton.snp.trailing).inset(-UIStyle.Inset.inset20)
        }
        contentView.addSubview(creatorImageView)
        creatorImageView.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview()
            maker.top.equalTo(titleLabel.snp.bottom).inset(-UIStyle.Inset.inset20)
        }
        contentView.addSubview(creatorNameLabel)
        creatorNameLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(creatorImageView.snp.trailing).inset(-UIStyle.Inset.inset8)
            maker.top.equalTo(creatorImageView.snp.top)
        }
        contentView.addSubview(dateLabel)
        dateLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(creatorNameLabel.snp.bottom)
            maker.leading.equalTo(creatorNameLabel.snp.leading)
        }
        contentView.addSubview(bookmarkButton)
        bookmarkButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(creatorImageView.snp.top)
            maker.trailing.equalToSuperview()
            maker.leading.greaterThanOrEqualTo(creatorNameLabel.snp.trailing).inset(-UIStyle.Inset.inset8)
        }
        contentView.addSubview(coinExpBackgroundView)
        coinExpBackgroundView.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview()
            maker.top.equalTo(creatorImageView.snp.bottom).inset(-UIStyle.Inset.inset16)
        }
        coinExpBackgroundView.addSubview(coinImageView)
        coinImageView.snp.makeConstraints { (maker) in
            maker.leading.bottom.top.equalToSuperview().inset(UIStyle.Inset.inset8)
        }
        coinExpBackgroundView.addSubview(coinLabel)
        coinLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(coinImageView.snp.trailing).inset(-UIStyle.Inset.inset4)
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
        }
        coinExpBackgroundView.addSubview(expImageView)
        expImageView.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.leading.equalTo(coinLabel.snp.trailing).inset(-UIStyle.Inset.inset16)
        }
        coinExpBackgroundView.addSubview(expLabel)
        expLabel.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.leading.equalTo(expImageView.snp.trailing).inset(-UIStyle.Inset.inset4)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.inset16)
        }
        contentView.addSubview(detailChallengeButton)
        detailChallengeButton.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview()
            maker.bottom.equalToSuperview().inset(UIStyle.Inset.inset12)
        }
        contentView.addSubview(uploadVideoButton)
        uploadVideoButton.snp.makeConstraints { (maker) in
            maker.trailing.equalToSuperview()
            maker.bottom.equalToSuperview().inset(UIStyle.Inset.inset12)
            maker.leading.greaterThanOrEqualTo(
                detailChallengeButton.snp.trailing).inset(-UIStyle.Inset.inset24)
        }
        contentView.addSubview(shareButton)
        shareButton.snp.makeConstraints { (maker) in
            maker.trailing.equalToSuperview()
            maker.bottom.equalTo(uploadVideoButton.snp.top).inset(-UIStyle.Inset.inset24)
        }
        contentView.addSubview(totalLikeLabel)
        totalLikeLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(shareButton.snp.leading).inset(-UIStyle.Inset.inset12)
            maker.centerY.equalTo(shareButton.snp.centerY)
        }
        contentView.addSubview(likeButton)
        likeButton.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(totalLikeLabel.snp.leading).inset(UIStyle.Inset.inset12)
            maker.centerY.equalTo(totalLikeLabel.snp.centerY)
            maker.leading.equalTo(creatorNameLabel.snp.trailing).inset(-UIStyle.Inset.inset16)
        }
        contentView.addSubview(totalSubmissionLabel)
        totalSubmissionLabel.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview()
            maker.centerY.equalTo(likeButton.snp.centerY)
        }
    }
}
