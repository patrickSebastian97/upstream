//
//  SelectInterestSkillLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 02/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

extension SelectInterestSkillViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        let bottomView = UIView()
        bottomView.backgroundColor = Asset.Color.white.color
        bottomView.addSubview(bottomButtonView)
        bottomButtonView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().inset(-UIStyle.Inset.inset16)
            maker.leading.equalToSuperview().inset(UIStyle.Inset.left)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.right)
        }

        let view = scrollView.contentView
        view.vstack(
            view.hstack(titleLabel),
            view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(selectInterestSkillListController.view).padTop(UIStyle.Inset.inset16),
            view.hstack(otherTextField).padTop(UIStyle.Inset.inset16),
            view.hstack(useCommaLabel).padTop(UIStyle.Inset.inset8),
            view.hstack(UIView().withHeight(100))
        ).withMargins(
            .init(
                top: UIStyle.Inset.defaultTop,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )

        self.view.addSubview(bottomView)
        bottomView.snp.makeConstraints { maker in
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.bottom.equalTo(bottomView.superview!.safeAreaLayoutGuide.snp.bottom)
        }
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { maker in
            maker.top.equalTo(scrollView.superview!.safeAreaLayoutGuide.snp.top)
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.bottom.equalTo(bottomView.snp.top).inset(100)
        }
        self.view.bringSubviewToFront(bottomView)
    }
}
