//
//  LoadingSetupLayout.swift
//  qlip
//
//  Created by Rahman Bramantya on 31/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension LoadingSetupScreenViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()
        let view = scrollView.contentView

        view.snp.makeConstraints { (maker) in
            maker.centerY.equalToSuperview()
            maker.centerX.equalToSuperview()
        }
        view.addSubview(imageView)
        view.addSubview(descriptionLabel)
        imageView.snp.makeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview()
        }
        descriptionLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(imageView.snp.bottom).offset(UIStyle.Inset.inset4)
            maker.centerX.equalToSuperview()
        }
    }
}
