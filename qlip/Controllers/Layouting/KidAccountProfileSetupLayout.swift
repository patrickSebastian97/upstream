//
//  KidAccountProfileSetupLayout.swift
//  qlip
//
//  Created by Rahman Bramantya on 21/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension KidAccountProfileSetupViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        let view = scrollView.contentView
        view.vstack(
            view.hstack(titleLabel),
            view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(kidAvatarCollectionController.view).padTop(UIStyle.Inset.inset24)
        ).withMargins(
            .init(
                top: UIStyle.Inset.defaultTop,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )
    }

}
