//
//  HomeMainLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 13/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import SnapKit

extension HomeMainViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        view.backgroundColor = Asset.Color.white.color
        let view = scrollView.contentView
        scrollView.refreshControl = refreshControl
        scrollView.showsVerticalScrollIndicator = false
        view.vstack(
            homeFeedListController.view
        )

        self.view.addSubview(homeHeaderView)
        homeHeaderView.snp.makeConstraints { maker in
            maker.top.equalTo(homeHeaderView.superview!.safeAreaLayoutGuide.snp.top)
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
        }
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { maker in
            maker.top.equalTo(homeHeaderView.snp.bottom).inset(56)
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
        }
        self.view.bringSubviewToFront(homeHeaderView)
    }
}
