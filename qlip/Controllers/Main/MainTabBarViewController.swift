//
//  MainTabBarViewController.swift
//  qlip
//
//  Created by Amelia Lim on 23/06/20.
//  Copyright © 2020 Amelia Lim. All rights reserved.
//

import UIKit
import PromiseKit

enum MainTabBarItem: Int {
    case home
    case explore
    case virtualCamp
    case inbox
    case account
}

struct TabBar {
    let type: MainTabBarItem
    let title: String
    let image: UIImage
    let selectedImage: UIImage
    let controller: UIViewController
}

final class MainTabBarViewController: UITabBarController, UITabBarControllerDelegate {

    // MARK: - Properties

     //swiftlint:disable line_length
    private let tabBars: [TabBar] = [
        (.home, L10n.tabHome, Asset.Main.home.image, Asset.Main.home.image.withRenderingMode(.alwaysTemplate).withTintColor(Asset.Color.black.color), HomeMainViewController()),
        (.explore, L10n.tabExplore, Asset.Main.explore.image, Asset.Main.explore.image.withRenderingMode(.alwaysTemplate).withTintColor(Asset.Color.black.color), DummyViewController()),
        (.virtualCamp, L10n.tabVirtualCamp, Asset.Main.virtualCamp.image, Asset.Main.virtualCamp.image.withRenderingMode(.alwaysTemplate).withTintColor(Asset.Color.black.color), DummyViewController()),
        (.inbox, L10n.tabInbox, Asset.Main.inbox.image, Asset.Main.inbox.image.withRenderingMode(.alwaysTemplate).withTintColor(Asset.Color.black.color), DummyViewController()),
        (.account, L10n.tabAccount, Asset.Main.account.image, Asset.Main.account.image.withRenderingMode(.alwaysTemplate).withTintColor(Asset.Color.black.color), DummyViewController())
    ].map(TabBar.init)
     //swiftlint:enable line_length

    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()

    // MARK: - Life Cyclyes

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTabBars()
        AppHelper.updateFCMToken()
        self.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: false)

//        if KidRealmModel.isLoggedIn() || ParentRealmModel.isLoggedIn() {
//            getAvailableNewImportantInbox()
//        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let newTabBarHeight = defaultTabBarHeight + UIStyle.Inset.inset12

        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight

        tabBar.frame = newFrame
    }

    // MARK: - Private Functions

    fileprivate func setupTabBars() {
        tabBar.barTintColor = Asset.Color.white.color.withAlphaComponent(0.9)
        tabBar.tintColor = Asset.Color.black.color
        tabBar.shadowImage = nil
        tabBar.backgroundImage = nil
        tabBar.layer.borderColor = Asset.Color.red.color.cgColor
        tabBar.layer.borderWidth = 0.3
        tabBar.layer.borderColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:0.2).cgColor
        tabBar.clipsToBounds = true

        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = tabBar.bounds
        blurView.autoresizingMask = .flexibleWidth
        tabBar.insertSubview(blurView, at: 0)

        var tagCounter = 0
        tabBars.forEach({
            let tabBarItem = UITabBarItem(title: $0.title, image: $0.image, selectedImage: $0.selectedImage)
            tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -7)
            tabBarItem.tag = tagCounter
            tabBarItem.image = tabBarItem.image?.withRenderingMode(.alwaysOriginal)
            tabBarItem.selectedImage = tabBarItem.selectedImage?.withRenderingMode(.alwaysOriginal)

            $0.controller.tabBarItem = tabBarItem
            tagCounter += 1
        })
        self.viewControllers = tabBars.map({
            return UINavigationController(rootViewController: $0.controller)
        })
    }

    // MARK: - Public Functions

    func selectTab(index: MainTabBarItem) {
        self.selectedIndex = index.rawValue
    }

    func getTabBarView(index: MainTabBarItem) -> UIView? {
        guard let tabBar = tabBar.items?[index.rawValue],
            let tabBarView = tabBar.value(forKey: "view") else { return nil }

        return tabBarView as? UIView
    }

    func getMainController(index: MainTabBarItem) -> UIViewController? {
        return viewControllers?[index.rawValue]
    }

    func updateLanguage() {
        tabBar.items?[MainTabBarItem.home.rawValue].title = L10n.tabHome
        tabBar.items?[MainTabBarItem.explore.rawValue].title = L10n.tabExplore
        tabBar.items?[MainTabBarItem.virtualCamp.rawValue].title = L10n.tabVirtualCamp
        tabBar.items?[MainTabBarItem.inbox.rawValue].title = L10n.tabInbox
        tabBar.items?[MainTabBarItem.account.rawValue].title = L10n.tabAccount
    }

    // MARK: - Tab Bar Controller Delegate

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let mainTabBarItem = MainTabBarItem(rawValue: item.tag) else {
            fatalError("tab index is not recognized!")
        }

        switch mainTabBarItem {
        case .home:
            navigationItem.title = L10n.tabHome.lowercased()
        case .explore:
            navigationItem.title = L10n.tabExplore.lowercased()
        case .virtualCamp:
            navigationItem.title = L10n.tabVirtualCamp.lowercased()
        case .inbox:
            navigationItem.title = L10n.tabInbox.lowercased()
        case .account:
            navigationItem.title = L10n.tabAccount.lowercased()
        }

        if selectedIndex == item.tag {
            if let navigationController = self.viewControllers?[selectedIndex] as? UINavigationController,
                let firstController = navigationController.viewControllers.first,
                let firstScrollView = firstController.view.subviews
                    .filter({ $0.isKind(of: UIScrollView.self) }).first,
                let scrollView = firstScrollView as? UIScrollView {
                scrollView.scrollToTop()
            }
            NotificationCenter.default.post(.init(name: .appScrollToTop))
        }

        if selectedIndex != item.tag {
            NotificationCenter.default.post(name: .mainTabBarDidChange, object: mainTabBarItem, userInfo: nil)
        }
    }

}
