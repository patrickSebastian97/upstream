//
//  HomeMainViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 13/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import PromiseKit
import UIScrollView_InfiniteScroll

private typealias `Self` = HomeMainViewController

class HomeMainViewController: BaseViewController, AppNotificationCenter, HomeMainNavigator {

    // MARK: - Navigation Configs
    override var backImage: UIImage? { return nil }

    // MARK: - Properties

    enum FeedSortedBy: String {
        case popular = "popularity"
        case newest = "created_at"
    }
    lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.tintColor = Asset.Color.black.color
        control.backgroundColor = Asset.Color.white.color
        control.addTarget(self, action: #selector(refreshControlPulled), for: .valueChanged)

        return control
    }()
    lazy var homeHeaderView: HomeHeaderView = {
        let view = HomeHeaderView()
        view.homeHeaderDelegate = self

        return view
    }()
    lazy var homeFeedListController: HomeFeedListController = {
        let controller = HomeFeedListController()
        controller.delegate = self

        return controller
    }()
    lazy var loadingShapeLayer: CAShapeLayer = {
        let layer = CAShapeLayer()

        return layer
    }()
    lazy var percentageLabel: UILabel = {
        let label = UILabel()

        return label
    }()
    private var currentPage: Int = APIConstants.Pagination.page
    private var currentIndex: Int = 0
    private var count: Int = 0
    private var feeds: [VideoChallengeModel] = []
    private var feedsSortedBy: FeedSortedBy =
        Storage.PickerHomeFeed.getPickerIndex() == 0 ? .popular : .newest
    private var isUserLoggedIn = ParentRealmModel.isLoggedIn() || KidRealmModel.isLoggedIn()

    // MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        enableAppNotification()
        setupInfiniteScroll()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .default
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        playCurrentVideo(index: currentIndex)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        stopCurrentVideo(index: currentIndex)
        navigationController?.navigationBar.isHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        stopCurrentVideo(index: currentIndex)
    }
    deinit {
        disableAppNotification()
    }

    // MARK: - Helpers

    override func fetchData() {
        super.fetchData()

        getFeed(page: currentPage, sortedBy: feedsSortedBy)
    }

    private func getFeed(page: Int, sortedBy: FeedSortedBy, isPullRefresh: Bool? = false) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Feed.getFeed(page: page, sortedBy: sortedBy.rawValue)
        }.compactMap {
            return $0
        }.done { [weak self] data in
            guard let self = self else { return }
            if data.data.pagination?.currentPage == 1 {
                self.feeds = data.dataModel()
            } else {
                data.dataModel().forEach { (feed) in
                    self.feeds.append(feed)
                }
            }
            self.count = data.data.pagination?.totalPage ?? 0
            self.currentPage = data.data.pagination!.currentPage + 1
            self.homeFeedListController.items = self.feeds.map({ HomeFeedViewModel(item: $0) })
            guard let url = self.feeds[0].videoTranscoded?.file else { return }
            self.homeFeedListController.firstVideoUrl = url
        }.ensure { [weak self] in
            guard let self = self else { return }
            if isPullRefresh != nil {
                self.refreshControl.endRefreshing()
                self.homeFeedListController.listReaction.removeAll()
            }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func addLikeOnSubmission(submissionId: String) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Reaction.postReaction(submissionId: submissionId)
        }.compactMap {
            return $0
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func deleteLikeOnSubmission(submissionId: String) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Reaction.deleteReaction(submissionId: submissionId)
        }.compactMap {
            return $0
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func playCurrentVideo(index: Int) {
        if feeds.isEmpty { return }
        guard let cell = homeFeedListController.collectionView.cellForItem(
                at: IndexPath(row: index, section: 0)) as? HomeFeedCell else { return }
        if let url = feeds[index].videoTranscoded?.file {
            cell.playVideo(videoUrl: url)
        }
    }
    private func stopCurrentVideo(index: Int) {
        if feeds.isEmpty { return }
        guard let cell = homeFeedListController.collectionView.cellForItem(
                at: IndexPath(row: index, section: 0)) as? HomeFeedCell else { return }
        cell.stopVideo()
    }
}

// MARK: - App Notification Center
extension Self {
    override func appScrollToTop(notification: NSNotification) {
        homeFeedListController.collectionView.scrollToTop()
    }
}

// MARK: Home Header View Delegate
extension Self: HomeHeaderViewDelegate {
    func homeHeaderView(didButtonGuestTapped homeHeaderView: HomeHeaderView) {
        goToLogin()
    }
}

// MARK: Home Feed List Controller Delegate
extension Self: HomeFeedListControllerDelegate {
    func homeFeedListController(_ homeFeedListController: HomeFeedListController) {
        feedsSortedBy = Storage.PickerHomeFeed.getPickerIndex() == 0 ? .popular : .newest
        getFeed(page: APIConstants.Pagination.page, sortedBy: feedsSortedBy)
        homeFeedListController.listReaction.removeAll()
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController,
        didLikeTappedAt index: Int,
        isLikeTapped: Bool
    ) {
        if isUserLoggedIn {
            guard let submissionId = feeds[index].submissionId else { return }
            if isLikeTapped {
                addLikeOnSubmission(submissionId: submissionId)
            } else {
                deleteLikeOnSubmission(submissionId: submissionId)
            }
        } else {
            goToLogin()
        }
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController,
        didShareTappedAt index: Int
    ) {
        if isUserLoggedIn {
            stopCurrentVideo(index: index)
            if let url = self.feeds[index].videoWatermark?.file {
                loadingShapeLayer.strokeEnd = 0
                let videoUrl = URL(string: url)!

                let configuration = URLSessionConfiguration.default
                let operationQueue = OperationQueue()
                let urlSession = URLSession(
                    configuration: configuration,
                    delegate: self,
                    delegateQueue: operationQueue
                )

                let task = urlSession.downloadTask(with: videoUrl)

                showLoadingWithProgress(
                    shapeLayer: loadingShapeLayer,
                    percentageLabel: percentageLabel,
                    loadingMessage: L10n.saving
                )
                task.resume()
            } else {
                showWarningToast(message: L10n.shareVideoValidation)
            }
        } else {
            goToLogin()
        }
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController,
        addSubmissionTapped index: Int
    ) {
        if isUserLoggedIn {
            showTodoPopup(message: "add submission \(index)")
        } else {
            goToLogin()
        }
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController,
        otherSubmissionTapped index: Int
    ) {
        if isUserLoggedIn {
            showTodoPopup(message: "other submission \(index)")
        } else {
            goToLogin()
        }
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController,
        didFeedsSelectAt feedsIdx: Int,
        didSubmissionSelectAt subsIdx: Int
    ) {
        if isUserLoggedIn {
            showTodoPopup(message: "go to snack byte \(feedsIdx) \(subsIdx)")
        } else {
            goToLogin()
        }
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController, didScroll index: Int) {
        currentIndex = index
        stopCurrentVideo(index: index)
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController, willBeginDragging index: Int) {
        currentIndex = index
        stopCurrentVideo(index: index)
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController, didEndDeclarating index: Int) {
        currentIndex = index
        playCurrentVideo(index: index)
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController, didEndDragging index: Int) {
        currentIndex = index
        playCurrentVideo(index: index)
    }
    func homeFeedListController(
        _ homeFeedListController: HomeFeedListController, didSelectItemAt index: Int) {
        if isUserLoggedIn {
            guard let id = feeds[index].id else { return }
            goToChallenge(id: id)
        } else {
            goToLogin()
        }
    }
}

// MARK: - Actions
extension Self {

    @objc private func refreshControlPulled(_ sender: Any) {
        getFeed(page: APIConstants.Pagination.page, sortedBy: feedsSortedBy, isPullRefresh: true)
        currentIndex = 0
    }
}

extension Self {
    func setupInfiniteScroll() {
        homeFeedListController.collectionView.beginInfiniteScroll(true)
        homeFeedListController.collectionView.setShouldShowInfiniteScrollHandler { [weak self] _ -> Bool in
            guard let self = self else { return false }
            return self.currentPage <= self.count
        }
        homeFeedListController.collectionView.addInfiniteScroll { [weak self] (collectionView) in
            guard let self = self else { return }
            collectionView.performBatchUpdates({
                self.getFeed(page: self.currentPage, sortedBy: self.feedsSortedBy)
            }, completion: { [weak self] _ in
                guard self != nil else { return }
                collectionView.finishInfiniteScroll()
            })
        }
    }
}

// MARK: - URL Session Download Delegate
extension Self: URLSessionDownloadDelegate {
    func urlSession(
        _ session: URLSession,
        downloadTask: URLSessionDownloadTask,
        didFinishDownloadingTo location: URL
    ) {
        let dateTime = Date().toString(format: .custom("ddMMyyyyhhmmss"))
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let destinationFileUrl = documentsUrl.appendingPathComponent(
            "\(self.feeds[currentIndex].id ?? "")\(dateTime).mp4"
        )
        do {
            try FileManager.default.copyItem(at: location, to: destinationFileUrl)
        } catch (let writeError) {
            DispatchQueue.main.async {
                self.hideLoading()
                self.handleError(error: writeError)
            }
        }
        DispatchQueue.main.async {
            self.hideLoading()
            self.loadingShapeLayer.strokeEnd = 0
            self.percentageLabel.text = "0%"
            let activityController = UIActivityViewController(
                activityItems: [destinationFileUrl],
                applicationActivities: nil
            )
            self.present(activityController, animated: true)
        }
    }

    func urlSession(
        _ session: URLSession,
        downloadTask: URLSessionDownloadTask,
        didWriteData bytesWritten: Int64,
        totalBytesWritten: Int64,
        totalBytesExpectedToWrite: Int64
    ) {
        let percentage = CGFloat(totalBytesWritten) / CGFloat(totalBytesExpectedToWrite)
        DispatchQueue.main.async {
            self.percentageLabel.text = "\(Int(percentage * 100))%"
            self.loadingShapeLayer.strokeEnd = percentage
            self.playCurrentVideo(index: self.currentIndex)
        }
    }
}
