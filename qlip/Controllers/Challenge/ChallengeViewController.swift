//
//  ChallengeViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 21/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import PromiseKit
import AVKit
import FloatingPanel

private typealias `Self` = ChallengeViewController

// swiftlint:disable type_body_length
// swiftlint:disable file_length
class ChallengeViewController: BaseViewController, ChallengeNavigator, AVAssetResourceLoaderDelegate {

    // MARK: - Configs

    override var backImage: UIImage? { return nil }
    override var shouldAttachScrollView: Bool { return false }
    override var preferredStatusBarColor: UIColor { return .clear }

    // MARK: - Properties

    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(Asset.General.backButton.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.contentMode = .scaleAspectFit
        button.tintColor = Asset.Color.white.color
        button.withWidth(UIStyle.Inset.inset16)
        button.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .heavy16,
            textColor: Asset.Color.white.color,
            textAlignment: .left,
            numberOfLines: 2
        )

        return label
    }()
    lazy var creatorImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.withSize(.init(width: UIStyle.Icon.sizeLarge, height: UIStyle.Icon.sizeLarge))
        imageView.layer.cornerRadius = UIStyle.CornerRadius.radius16
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = Asset.Color.white.color.cgColor
        imageView.clipsToBounds = true

        return imageView
    }()
    lazy var creatorNameLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .heavy14,
            textColor: Asset.Color.white.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var dateLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .medium12,
            textColor: Asset.Color.white.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var bookmarkButton: UIButton = {
        let button = UIButton()
        button.setImage(Asset.General.bookmarkInactive.image, for: .normal)
        button.addTarget(self, action: #selector(bookmarkButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var coinExpBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Color.blackAlpha30.color
        view.layer.cornerRadius = UIStyle.CornerRadius.radius16

        return view
    }()
    lazy var coinImageView: UIImageView = {
        let imageView = UIImageView(
            image: Asset.General.moneyIcon.image.withRenderingMode(.alwaysOriginal),
            contentMode: .scaleAspectFit
        )

        return imageView
    }()
    lazy var coinLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book12,
            textColor: Asset.Color.white.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var expImageView: UIImageView = {
        let imageView = UIImageView(
            image: Asset.General.experienceIcon.image.withRenderingMode(.alwaysOriginal),
            contentMode: .scaleAspectFit
        )

        return imageView
    }()
    lazy var expLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book12,
            textColor: Asset.Color.white.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var totalSubmissionLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .heavy14,
            textColor: Asset.Color.white.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var likeButton: UIButton = {
        let button = UIButton()
        button.setImage(
            Asset.General.likeInactiveChallenge.image.withRenderingMode(.alwaysTemplate),
            for: .normal
        )
        button.tintColor = Asset.Color.white.color
        button.contentEdgeInsets = UIEdgeInsets(
            top: UIStyle.Inset.inset16,
            left: UIStyle.Inset.inset4,
            bottom: UIStyle.Inset.inset12,
            right: UIStyle.Inset.inset16
        )
        button.addTarget(self, action: #selector(likeButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var totalLikeLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .medium12,
            textColor: Asset.Color.white.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var shareButton: UIButton = {
        let button = UIButton()
        button.setImage(
            Asset.General.share.image.withRenderingMode(.alwaysTemplate),
            for: .normal
        )
        button.tintColor = Asset.Color.white.color
        button.contentEdgeInsets = UIEdgeInsets(
            top: UIStyle.Inset.inset16,
            left: UIStyle.Inset.inset4,
            bottom: UIStyle.Inset.inset12,
            right: UIStyle.Inset.inset16
        )
        button.addTarget(self, action: #selector(shareButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var uploadVideoButton: UIButton = {
        let button = UIButton()
        button.setTitle(L10n.uploadYourVideo, for: .normal)
        button.setTitleColor(Asset.Color.black.color, for: .normal)
        button.titleLabel?.font = .heavy14
        button.titleLabel?.numberOfLines = 2
        button.backgroundColor = Asset.Color.yellow.color
        button.layer.cornerRadius = 20
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 23, bottom: 10, right: 23)

        return button
    }()
    lazy var detailChallengeButton: UIButton = {
        let button = UIButton()
        button.setTitle(L10n.seeDetail, for: .normal)
        button.titleLabel?.font = .heavy14
        button.setTitleColor(Asset.Color.white.color, for: .normal)
        button.setImage(Asset.General.arrowUpButton.image, for: .normal)
        button.layer.cornerRadius = 18
        button.layer.borderWidth = 1
        button.layer.borderColor = Asset.Color.white.color.cgColor
        button.semanticContentAttribute = .forceRightToLeft
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        button.contentEdgeInsets = UIEdgeInsets(top: 7, left: 16, bottom: 7, right: 16)
        button.addTarget(self, action: #selector(detailTapped), for: .touchUpInside)

        return button
    }()
    private lazy var player: AVPlayer = {
        let player = AVPlayer()
        return player
    }()
    private lazy var playerLayer: AVPlayerLayer = {
        let layer = AVPlayerLayer()
        layer.videoGravity = .resizeAspectFill
        layer.player = player
        return layer
    }()
    lazy var videoView: UIView = {
        let view = UIView()
        view.layer.addSublayer(playerLayer)

        return view
    }()
    lazy var topShadowView: CommonView = {
        let view = CommonView()
        view.usingGradient =  true
        view.secondColor = Asset.Color.blackAlpha50.color
        view.firstColor = Asset.Color.white.color.withAlphaComponent(0)

        return view
    }()
    lazy var bottomShadow: CommonView = {
        let view = CommonView()
        view.usingGradient =  true
        view.firstColor = Asset.Color.blackAlpha50.color
        view.secondColor = Asset.Color.white.color.withAlphaComponent(0)

        return view
    }()
    lazy var challengeDetailFloatingPanel: FloatingPanelController = {
        let view = FloatingPanelController()
        let appearrance = SurfaceAppearance()
        view.layout = SheetDrawerLayout()
        appearrance.cornerRadius = UIStyle.CornerRadius.radius16
        appearrance.borderWidth = UIStyle.Inset.inset16
        appearrance.borderColor = .white
        view.surfaceView.appearance = appearrance
        view.backdropView.dismissalTapGestureRecognizer.isEnabled = true
        view.isRemovalInteractionEnabled = true
        view.delegate = self

        return view
    }()
    lazy var loadingShapeLayer: CAShapeLayer = {
        let layer = CAShapeLayer()

        return layer
    }()
    lazy var percentageLabel: UILabel = {
        let label = UILabel()

        return label
    }()
    var challengeId: String = ""
    private var challengeDetail: ChallengeDetailModel?
    private var challengeDetailViewModel: ChallengeDetailViewModel?
    private var isLiked: Bool = false
    private var isBookmark: Bool = false
    private var totalLike: Int = 0
    let videoToken = "a024473de51367b47ba6424e52ee2885"
    private var observer: NSKeyValueObservation?
    private var videoObserver: NSObjectProtocol?

    // MARK: - LifeCycles

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.tabBarController?.tabBar.isHidden = true
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame = self.view.bounds
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopVideo()
    }

    // MARK: - Helpers
    override func fetchData() {
        super.fetchData()

        getChallenge()
    }

    private func getChallenge() {
        let id = self.challengeId
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Challenge.getDetail(id: id)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.challengeDetail = data
            self.challengeDetailViewModel = ChallengeDetailViewModel(item: data)
            self.updateViews()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func addLikeOnSubmission() {
        guard let viewModel = challengeDetailViewModel else { return }
        let id = viewModel.submissionId ?? ""
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Reaction.postReaction(submissionId: id)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            self.likeButton.setImage(Asset.General.likeActiveChallenge.image, for: .normal)
            self.totalLikeLabel.text = String(self.totalLike + 1)
            self.isLiked = true
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func deleteLikeOnSubmission() {
        guard let viewModel = challengeDetailViewModel else { return }
        let id = viewModel.submissionId ?? ""
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Reaction.deleteReaction(submissionId: id)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            self.likeButton.setImage(Asset.General.likeInactiveChallenge.image, for: .normal)
            self.totalLikeLabel.text = String(self.totalLike - 1)
            self.isLiked = false
        }
        .ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func addBookmark() {
        let id = self.challengeId
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Bookmark.add(id: id)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            self.bookmarkButton.setImage(Asset.General.bookmarkActive.image, for: .normal)
            self.isBookmark = true
            self.showSuccessToast(message: L10n.successBookmark)
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func deleteBookmark() {
        let id = self.challengeId
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Bookmark.delete(id: id)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            self.bookmarkButton.setImage(Asset.General.bookmarkInactive.image, for: .normal)
            self.isBookmark = false
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    func playVideo(videoUrl: String) {
        self.view.showLoading()
        if let url = URL(string: videoUrl) {
            let asset = AVURLAsset(url: url)
            asset.resourceLoader.setDelegate(self, queue: DispatchQueue(__label: "AV \(videoUrl)", attr: nil))
            let item = AVPlayerItem(asset: asset)

            observer = item.observe(\.status, options: .new) { (item, _) in
                if item.status == .readyToPlay {
                    self.view.hideLoading()
                }
            }

            try? AVAudioSession.sharedInstance().setCategory(.playback)

            player.replaceCurrentItem(with: item)
            player.isMuted = UserDefaults.standard.bool(forKey: SoundDefaultKey.soundState)
            player.play()

            if let observer = videoObserver {
                NotificationCenter.default.removeObserver(
                    observer,
                    name: .AVPlayerItemDidPlayToEndTime,
                    object: player.currentItem
                )
            }
            videoObserver = NotificationCenter.default.addObserver(
                forName: .AVPlayerItemDidPlayToEndTime,
                object: player.currentItem,
                queue: .main
            ) { _ in
                self.player.seek(to: .zero)
                self.player.play()
            }
        }
    }
    func stopVideo() {
        observer = nil
        player.pause()
        player.seek(to: .zero)
    }

    override func updateViews() {
        super.updateViews()

        guard let viewModel = challengeDetailViewModel else { return }
        titleLabel.text = viewModel.title
        creatorImageView.loadImage(url: viewModel.creatorImageUrl)
        creatorNameLabel.text = viewModel.creatorName
        dateLabel.text = viewModel.dateCreate
        bookmarkButton.setImage(
            viewModel.isBookmark ? Asset.General.bookmarkActive.image : Asset.General.bookmarkInactive.image,
            for: .normal)
        coinLabel.text = viewModel.totalCoin
        expLabel.text = viewModel.totalExp
        totalSubmissionLabel.text = viewModel.totalSubmission
        likeButton.setImage(
            viewModel.isLiked ?
                Asset.General.likeActiveChallenge.image : Asset.General.likeInactiveChallenge.image,
            for: .normal)
        totalLikeLabel.text = viewModel.totalLike
        isLiked = viewModel.isLiked
        isBookmark = viewModel.isBookmark
        totalLike = Int(viewModel.totalLike) ?? 0
        playVideo(videoUrl: viewModel.videoUrl)
    }

}

// MARK: - Actions
extension Self {

    @objc private func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc private func bookmarkButtonTapped() {
        isBookmark ? deleteBookmark() : addBookmark()
    }
    @objc private func likeButtonTapped() {
        isLiked ? deleteLikeOnSubmission() : addLikeOnSubmission()
    }
    @objc private func shareButtonTapped() {
        if let url = self.challengeDetail?.challenge?.videoWatermark?.file {
            loadingShapeLayer.strokeEnd = 0
            let videoUrl = URL(string: url)!

            let configuration = URLSessionConfiguration.default
            let operationQueue = OperationQueue()
            let urlSession = URLSession(
                configuration: configuration,
                delegate: self,
                delegateQueue: operationQueue
            )

            let task = urlSession.downloadTask(with: videoUrl)

            showLoadingWithProgress(
                shapeLayer: loadingShapeLayer,
                percentageLabel: percentageLabel,
                loadingMessage: L10n.saving
            )
            task.resume()
        } else {
            showWarningToast(message: L10n.shareVideoValidation)
        }
    }
    @objc private func detailTapped() {
        guard let viewModel = challengeDetailViewModel else { return }
        goToChallengeDetail(view: challengeDetailFloatingPanel, viewModel: viewModel)
    }
}

// MARK: - URL Session Download Delegate
extension Self: URLSessionDownloadDelegate {
    func urlSession(
        _ session: URLSession,
        downloadTask: URLSessionDownloadTask,
        didFinishDownloadingTo location: URL
    ) {
        let dateTime = Date().toString(format: .custom("ddMMyyyyhhmmss"))
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let destinationFileUrl = documentsUrl.appendingPathComponent(
            "\(self.challengeId )\(dateTime).mp4"
        )
        do {
            try FileManager.default.copyItem(at: location, to: destinationFileUrl)
        } catch (let writeError) {
            DispatchQueue.main.async {
                self.hideLoading()
                self.handleError(error: writeError)
            }
        }
        DispatchQueue.main.async {
            self.hideLoading()
            self.loadingShapeLayer.strokeEnd = 0
            self.percentageLabel.text = "0%"
            let activityController = UIActivityViewController(
                activityItems: [destinationFileUrl],
                applicationActivities: nil
            )
            self.present(activityController, animated: true)
        }
    }

    func urlSession(
        _ session: URLSession,
        downloadTask: URLSessionDownloadTask,
        didWriteData bytesWritten: Int64,
        totalBytesWritten: Int64,
        totalBytesExpectedToWrite: Int64
    ) {
        let percentage = CGFloat(totalBytesWritten) / CGFloat(totalBytesExpectedToWrite)
        DispatchQueue.main.async {
            self.percentageLabel.text = "\(Int(percentage * 100))%"
            self.loadingShapeLayer.strokeEnd = percentage
        }
    }
}

// MARK: - Resource Loader Delegate
extension Self {
    func resourceLoader(_ resourceLoader: AVAssetResourceLoader,
                        shouldWaitForRenewalOfRequestedResource
                        renewalRequest: AVAssetResourceRenewalRequest) -> Bool {
        return shouldLoadOrRenewRequestedResource(resourceLoadingRequest: renewalRequest)
    }

    func resourceLoader(_ resourceLoader: AVAssetResourceLoader,
                        shouldWaitForLoadingOfRequestedResource
                        loadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        return shouldLoadOrRenewRequestedResource(resourceLoadingRequest: loadingRequest)
    }

    func shouldLoadOrRenewRequestedResource(resourceLoadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        guard let url = resourceLoadingRequest.request.url?.absoluteString else {
            return false
        }
        let urlWithToken = URL(string: "\(url)?token=\(videoToken)")!
        guard let urlData = try? Data(contentsOf: urlWithToken) else { return false }

        resourceLoadingRequest.dataRequest?.respond(with: urlData)
        resourceLoadingRequest.finishLoading()

        return true
    }
}

// MARK: - FLoating Panel Controller delegate
extension Self: FloatingPanelControllerDelegate {
    func floatingPanel(
        _ fpc: FloatingPanelController,
        shouldRemoveAt location: CGPoint,
        with velocity: CGVector
    ) -> Bool {
        let threshold: CGFloat = 2.0
        switch fpc.layout.position {
        case .top:
            return (velocity.dy <= -threshold)
        case .left:
            return (velocity.dx <= -threshold)
        case .bottom:
            return (velocity.dy >= threshold)
        case .right:
            return (velocity.dx >= threshold)
        }
    }
}
