//
//  ChallengeDetailDrawerViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 23/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

private typealias `Self` = ChallengeDetailDrawerViewController

class ChallengeDetailDrawerViewController: BaseViewController {

    // MARK: - Properties
    // MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
    }
    // MARK: - Helpers
}

// MARK: - Actions
extension Self {

}
