//
//  WelcomePageNavigator.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import FloatingPanel

protocol WelcomePageNavigator {
    func goToLogin()
    func goToRegister()
    func goToParentConsent(view: FloatingPanelController)
    func goToTermsConditions(view: FloatingPanelController)
    func goToPrivacyPolicy(view: FloatingPanelController)
}

extension WelcomePageNavigator where Self: UIViewController {
    func goToLogin() {
        let controller = LoginAndRegisterViewController()
        controller.screenType = .login
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToRegister() {
        let controller = LoginAndRegisterViewController()
        controller.screenType = .register
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToParentConsent(view: FloatingPanelController) {
        let controller = LegalViewController()
        controller.legalType = .parentConsent
        view.set(contentViewController: controller)
        self.present(view, animated: true, completion: nil)
    }
    func goToTermsConditions(view: FloatingPanelController) {
        let controller = LegalViewController()
        controller.legalType = .termCondition
        view.set(contentViewController: controller)
        self.present(view, animated: true, completion: nil)
    }
    func goToPrivacyPolicy(view: FloatingPanelController) {
        let controller = LegalViewController()
        controller.legalType = .privacyPolicy
        view.set(contentViewController: controller)
        self.present(view, animated: true, completion: nil)
    }
}
