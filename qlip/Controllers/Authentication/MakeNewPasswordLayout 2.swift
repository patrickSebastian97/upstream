//
//  MakeNewPasswordLayout.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension MakeNewPasswordViewController {

    override func initializeUI() {
        super.initializeUI()
        let view = scrollView.contentView
        view.vstack(
        view.hstack(popupSuccess,popupFail).padTop(UIStyle.Inset.inset4),
        view.hstack(titleLabel).padTop(UIStyle.Inset.inset24),
        view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset24),
        view.hstack(insertNewPassword).padTop(UIStyle.Inset.inset24),
        view.hstack(newPasswordField).padTop(UIStyle.Inset.inset24),
        view.hstack(radioButton,passwordMinLength).padTop(UIStyle.Inset.inset24),
        view.hstack(passwordNeedCapAndNumber).padTop(UIStyle.Inset.inset8),
        view.vstack(createPasswordBtn).padTop(UIStyle.Inset.inset24)
        ).withMargins(.init(
                    top: UIStyle.Inset.defaultTop,
                    left: UIStyle.Inset.left,
                    bottom: UIStyle.Inset.bottom,
                    right: UIStyle.Inset.right
                )
            )
    }
}
