//
//  WelcomePageViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import ActiveLabel
import FloatingPanel

private typealias `Self` = WelcomePageViewController

class WelcomePageViewController: BaseViewController, WelcomePageNavigator {

    // MARK: - Properties

    lazy var imageView: UIImageView = {
        let view = UIImageView(
            image: Asset.Authentication.welcome.image.withRenderingMode(.alwaysOriginal),
            contentMode: .scaleAspectFit
        )

        return view
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: L10n.welcomeToQlip,
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var loginButton: Button = {
        let button = Button(title: L10n.login, type: .yellow)
        button.withHeight(UIStyle.Button.height)
        button.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)

        return button
    }()
    lazy var registerButton: Button = {
        let button = Button(title: L10n.registration, type: .yellow)
        button.withHeight(UIStyle.Button.height)
        button.addTarget(self, action: #selector(registerTapped), for: .touchUpInside)

        return button
    }()
    lazy var guestButton: UIButton = {
        let button = UIButton()
        button.setTitle(L10n.continueAsGuest, for: .normal)
        button.titleLabel?.font = .medium14
        button.setTitleColor(Asset.Color.blue.color, for: .normal)
        button.setImage(Asset.Authentication.compass.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Asset.Color.blue.color
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: UIStyle.Inset.inset8,
            bottom: 0,
            right: 0
        )
        button.addTarget(self, action: #selector(guestButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var termPrivacyLabel: ActiveLabel = {
        let parentConsentType = L10n.parentConsent
        let termsAndConditionType = L10n.termsAndConditions
        let policyType = L10n.privacyPolicy
        let view = ActiveLabel(
            text: "",
            font: .book12,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 0
        )
        view.lineBreakMode = .byWordWrapping

        let consent = ActiveType.custom(pattern: parentConsentType)
        let terms = ActiveType.custom(pattern: termsAndConditionType)
        let policy = ActiveType.custom(pattern: policyType)

        view.enabledTypes.append(consent)
        view.enabledTypes.append(terms)
        view.enabledTypes.append(policy)

        view.customize({ label in
            switch userType {
            case .kid: label.text = L10n.byRegisterAgreeTerm(L10n.youKid)
            case .parent: label.text = L10n.byRegisterAgreeTerm(L10n.youParent)
            }
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping

            label.configureLinkAttribute = { (type, attributes, isSelected) in
                var atts = attributes
                switch type {
                case consent, terms, policy:
                    atts[NSAttributedString.Key.font] = UIFont.book12
                    atts[NSAttributedString.Key.foregroundColor] = Asset.Color.blue.color
                default: ()
                }

                return atts
            }
            label.handleCustomTap(for: consent, handler: { [weak self] _ in
                guard let self = self else { return }
                self.parentConsentTapped()
            })
            label.handleCustomTap(for: terms, handler: { [weak self] _ in
                guard let self = self else { return }
                self.termsAndConditionsTapped()
            })
            label.handleCustomTap(for: policy, handler: { [weak self] _ in
                guard let self = self else { return }
                self.privacyPolicyTapped()
            })
        })

        return view
    }()
    lazy var agreementFloatingPanel: FloatingPanelController = {
        let view = FloatingPanelController()
        let appearrance = SurfaceAppearance()
        view.layout = SheetDrawerLayout()
        appearrance.cornerRadius = UIStyle.CornerRadius.radius16
        appearrance.borderWidth = 17
        appearrance.borderColor = .white
        view.surfaceView.appearance = appearrance
        view.backdropView.dismissalTapGestureRecognizer.isEnabled = true
        view.isRemovalInteractionEnabled = true
        view.delegate = self

        return view
    }()
    let userType: UserType = UserType(rawValue: Storage.User.getUserType()) ?? .kid

    // MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func setupViews() {
        super.setupViews()

        switch userType {
        case .kid:
            descriptionLabel.text = L10n.welcomeKidDescription
        case .parent:
            descriptionLabel.text = L10n.welcomeParentDescription
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        agreementFloatingPanel.dismiss(animated: true, completion: nil)
    }

    // MARK: - Helpers

}

// MARK: - Actions
extension Self {

    @objc private func loginTapped() {
        goToLogin()
        clickLoginEventTracking()
    }

    @objc private func registerTapped() {
        goToRegister()
        clickRegisterEventTracking()
    }

    @objc private func guestButtonTapped() {
        gotoHomeScreen()
    }

    @objc private func parentConsentTapped() {
        goToParentConsent(view: agreementFloatingPanel)
        clickParentalConsentEventTracking()
    }

    @objc private func termsAndConditionsTapped() {
        goToTermsConditions(view: agreementFloatingPanel)
        clickTermsAgreementEventTracking()
    }

    @objc private func privacyPolicyTapped() {
        goToPrivacyPolicy(view: agreementFloatingPanel)
        clickPrivacyPolicyEventTracking()
    }
}

// MARK: - FLoating Panel Controller delegate
extension Self: FloatingPanelControllerDelegate {
    func floatingPanel(
        _ fpc: FloatingPanelController,
        shouldRemoveAt location: CGPoint,
        with velocity: CGVector
    ) -> Bool {
        let threshold: CGFloat = 2.0
        switch fpc.layout.position {
        case .top:
            return (velocity.dy <= -threshold)
        case .left:
            return (velocity.dx <= -threshold)
        case .bottom:
            return (velocity.dy >= threshold)
        case .right:
            return (velocity.dx >= threshold)
        }
    }
}
