//
//  OTPViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.

import UIKit
import ActiveLabel
import PromiseKit
import MessageUI

private typealias `Self` = OTPViewController

class OTPViewController: BaseViewController, DismissableKeyboard, AdaptableKeyboard, OTPNavigator {

    // MARK: - Configs

    override var shouldAttachScrollView: Bool { return true }

    // MARK: - Properties

    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: L10n.otpCode,
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var errorLabel: UILabel = {
        let view = UILabel(
            text: "",
            font: .medium12,
            textColor: Asset.Color.red.color,
            textAlignment: .left, numberOfLines: 2)

        return view
    }()
    lazy var otpLabel: UILabel = {
        let label = UILabel(
            text: L10n.otp,
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var resendCodeInstructionLabel: UILabel = {
       let label = UILabel(
        text: "",
        font: .medium14,
        textColor: Asset.Color.black.color,
        textAlignment: .left,
        numberOfLines: 0
       )

        return label
    }()
    lazy var resendButtonLabel: ActiveLabel = {
        let resendType = L10n.resendCode
        let label = ActiveLabel(
            text: "",
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 0
        )
        label.lineBreakMode = .byWordWrapping

        let resend = ActiveType.custom(pattern: resendType)
        label.enabledTypes.append(resend)

        label.customize { (label) in
            label.text = L10n.haveNotReceiveCode
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping

            label.configureLinkAttribute = { (type, attributes, isSelected) in
                var atts = attributes
                switch type {
                case resend:
                    atts[NSAttributedString.Key.font] = UIFont.heavy14
                    atts[NSAttributedString.Key.foregroundColor] = Asset.Color.blue.color
                default: ()
                }

                return atts
            }

            label.handleCustomTap(for: resend) { [weak self] _ in
                guard let self = self else { return }
                self.resendTapped()
            }
        }

        return label
    }()
    lazy var contactUsLabel: UILabel = {
        let label = UILabel(
            text: L10n.contactUsIfNeedHelp,
            font: .medium14,
            textColor: Asset.Color.blackAlpha50.color,
            textAlignment: .center,
            numberOfLines: 0
        )

        return label
    }()
    lazy var sendEmailButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(L10n.sendEmail, for: .normal)
        button.titleLabel?.font = .medium14
        button.setTitleColor(Asset.Color.blue.color, for: .normal)
        button.setImage(Asset.General.email.image, for: .normal)
        button.contentHorizontalAlignment = .center
        button.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: UIStyle.Inset.inset8,
            bottom: 0,
            right: 0
        )
        button.addTarget(self, action: #selector(emailButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var sendInstagramButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(L10n.dmInstagram, for: .normal)
        button.titleLabel?.font = .medium14
        button.setTitleColor(Asset.Color.blue.color, for: .normal)
        button.setImage(Asset.General.instagram.image, for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: UIStyle.Inset.inset8,
            bottom: 0,
            right: 0
        )
        button.addTarget(self, action: #selector(instagramButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var contactUsWrapperView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Color.whiteAlpha70.color
        view.layer.cornerRadius = UIStyle.CornerRadius.radius8

        return view
    }()
    let textFields: [EnterCodeTextField] = [
        EnterCodeTextField(),
        EnterCodeTextField(),
        EnterCodeTextField(),
        EnterCodeTextField()
    ]
    var codeSentTo: String = "" {
        didSet {
            if codeSentTo.prefix(1) == "0" {
                codeSentTo = String(codeSentTo.dropFirst())
            }
        }
    }
    var otpType: OTPType = .register
    var userType: UserType = UserType(rawValue: Storage.User.getUserType()) ?? .kid
    var username: String = ""
    var password: String = ""
    var isKidSocialMedia: Bool = false
    var isKidForgotWithParentPhone: Bool = false
    var timer: Timer?
    var timeElapsedSeconds: Int = 0
    let textFieldBorderActive: CGColor = Asset.Color.blue.color.cgColor
    let textFieldBorderIdle: CGColor = Asset.Color.blackAlpha20.color.cgColor
    let textFieldFillActive: CGColor = Asset.Color.skyBlueAlpha3.color.cgColor
    let textFieldFillIdle: CGColor = Asset.Color.white.color.cgColor

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        enableAdaptableKeyboard()
        enableDismissableKeyboard()

        runTimer()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.isHidden = false
        textFields.first?.becomeFirstResponder()
    }

    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)

        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }

    override func setupViews() {
        super.setupViews()

        descriptionLabel.attributedText = NSMutableAttributedString()
            .normal(L10n.otpDesccriptions)
            .bold("+62\(codeSentTo)", boldFont: .heavy16)
        for index in textFields.indices {
            let textField = textFields[index]
            textField.textAlignment = .center
            textField.backgroundColor = .white
            textField.font = .medium16
            textField.adjustsFontSizeToFitWidth = true
            textField.keyboardType = .numberPad
            textField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
            textField.delegate = self
            textField.enterCodeTextFieldDelegate = self
            textField.tag = index
            textField.layer.borderWidth = 1
            textField.layer.borderColor = Asset.Color.blackAlpha20.color.cgColor
            textField.layer.cornerRadius = UIStyle.Button.cornerRadius
            textField.layer.masksToBounds = true
        }
    }

    deinit {
        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }
}

// MARK: - Helpers
extension Self {
    private func getCode() -> String {
        return textFields.compactMap({ $0.text }).joined()
    }

    private func runTimer() {
        stopTimer()
        timer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(updateTimer),
            userInfo: nil,
            repeats: true
        )
        resendCodeInstructionLabel.isHidden = false
        resendButtonLabel.isHidden = true
        errorLabel.text = " "
        timeElapsedSeconds = SettingOTP.otpTimeInSeconds
        resendCodeInstructionLabel.attributedText = NSMutableAttributedString()
            .normal(L10n.resendCodeIn)
            .bold(TimeInterval(timeElapsedSeconds).timeString, boldFont: .heavy14)
    }

    private func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    // swiftlint:disable function_body_length
    // swiftlint:disable cyclomatic_complexity
    private func verifyOTP(username: String? = nil) {
        hideKeyboard()
        let otp = getCode()
        let phone = self.codeSentTo
        let otpType = self.otpType
        let userType = self.userType
        firstly {
            self.view.showLoading()
        }.then { _ in
        APIService.Authentication.verifyOTP(
                phone: phone,
                otp: otp,
                type: otpType.rawValue,
                username: username)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            switch otpType {
            case .forgotPassword:
                switch userType {
                case .kid:
                    if let username = username {
                        self.goToNewPasswordKidParentPhone(
                            phone: phone,
                            token: data.token,
                            username: username)
                    } else {
                        self.goToMakeNewPassword(phone: phone, token: data.token)
                    }
                case .parent:
                    self.goToMakeNewPassword(phone: phone, token: data.token)
                }
                self.forgotPasswordSuccessVerificationOTPEventTracking(otp: otp)
            case .register:
                switch userType {
                case .kid:
                    UserDataManager.shared.setOTP(otp: otp)
                    self.goToParentPhoneNumber(isKidSocialMedia: self.isKidSocialMedia)
                case .parent:
                    UserDataManager.shared.setOTP(otp: otp)
                    self.goToCreateKidProfile(isSocialMediaSignIn: false)
                    self.parentAddKidProfileOpenOnboarding1EventTracking()
                }
                self.registerSuccessVerificationOTPEventTracking()
            default:
                break
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
            self.textFields.forEach { (textfield) in
                textfield.layer.borderColor = self.textFieldBorderIdle
                textfield.layer.backgroundColor = self.textFieldFillIdle
                textfield.text = ""
            }
            self.textFields[0].becomeFirstResponder()
        }
    }

    private func parentUpdatePhoneNumber() {
        let phone = self.codeSentTo
        let otp = getCode()
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Parent.updatePhoneNumber(phone: phone, otp: otp)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            self.goToCreateKidProfile(isSocialMediaSignIn: true)
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
}

// MARK: - Enter Code Text Field Delegate
extension Self: EnterCodeTextFieldDelegate {
    func enterCodeTextField(willDelete textField: EnterCodeTextField) {
        let isEmptyText = textField.text?.isEmpty ?? true
        if textField.isFirstResponder && isEmptyText {
            let index = max(textField.tag - 1, 0)
            textFields[index].text = ""
            textFields[index].becomeFirstResponder()
        }
        textField.layer.borderColor = textFieldBorderIdle
        textField.layer.backgroundColor = textFieldFillIdle
        errorLabel.text = ""
    }

    func enterCodeTextField(didDelete textField: EnterCodeTextField) {
        updateButtonState()
    }
}

// MARK: - Text Field Delegate
extension Self: UITextFieldDelegate {
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        textField.layer.borderColor = textFieldBorderActive
        textField.layer.backgroundColor = textFieldFillActive

        return newLength <= 1
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = textFieldBorderActive
        textField.layer.backgroundColor = textFieldFillIdle
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.isEmpty ?? true {
            textField.layer.borderColor = textFieldBorderIdle
        }
    }
}

// MARK: - MF Mail Composer View Controller Delegate

extension Self: MFMailComposeViewControllerDelegate {
    func mailComposeController(
        _ controller: MFMailComposeViewController,
        didFinishWith result: MFMailComposeResult,
        error: Error?
    ) {
        if let error = error {
            controller.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                self.handleError(error: error)
            }
            return
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Actions
// swiftlint:disable file_length
extension Self {

    @objc private func textFieldChanged(_ textField: UITextField) {
        let textEmpty = textField.text?.isEmpty ?? true
        if textEmpty {
            return
        }

        let index = min(textField.tag + 1, textFields.count - 1)
        textFields[index].becomeFirstResponder()

        let latest = textFields.count-1
        let isLatestEmpty = textFields[latest].text?.isEmpty ?? true
        if textFields[latest].isFirstResponder && !isLatestEmpty {
            textFields[index].resignFirstResponder()
        }
        if getCode().count == textFields.count {
            switch otpType {
            case .forgotPassword:
                switch userType {
                case .parent:
                    verifyOTP()
                case .kid:
                    if isKidForgotWithParentPhone {
                        verifyOTP(username: self.username)
                    } else {
                        verifyOTP()
                    }
                }
                forgotPasswordInputOTPEventTracking(otp: getCode())
            case .register:
                verifyOTP()
                registerInputOTPEventTracking()
            case .updatePhone:
                parentUpdatePhoneNumber()
            }
        }
    }

    @objc private func updateTimer() {
        timeElapsedSeconds -= 1
        resendCodeInstructionLabel.attributedText = NSMutableAttributedString()
            .normal(L10n.resendCodeIn)
            .bold(TimeInterval(timeElapsedSeconds).timeString, boldFont: .heavy14)
        if timeElapsedSeconds <= 0 {
            stopTimer()
            resendCodeInstructionLabel.isHidden = true
            resendButtonLabel.isHidden = false
            errorLabel.text = " "
        }
    }

    @objc private func resendTapped() {
        hideKeyboard()
        let phone = self.codeSentTo
        let type = otpType.rawValue
        self.forgotPasswordResendOTPEventTracking(otp: self.getCode())
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.requestOTP(phone: phone, type: type)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            switch self.otpType {
            case .forgotPassword:
                self.textFields.forEach { (textfield) in
                    textfield.layer.borderColor = self.textFieldBorderIdle
                    textfield.layer.backgroundColor = self.textFieldFillIdle
                    textfield.text = ""
                    self.errorLabel.text = ""
                }
                self.runTimer()
            case .register, .updatePhone:
                self.textFields.forEach { (textfield) in
                    textfield.layer.borderColor = self.textFieldBorderIdle
                    textfield.layer.backgroundColor = self.textFieldFillIdle
                    textfield.text = ""
                    self.errorLabel.text = ""
                }
                self.runTimer()
                self.registerResendOTPEventTracking()
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    @objc private func instagramButtonTapped() {
        openInstagram(username: QlipAccount.instagram)
    }
    @objc private func emailButtonTapped() {
        let phone = self.codeSentTo
        let recipients: String = QlipAccount.emailSupport
        let subject: String = L10n.otpEmailSubject
        let messageBody: String = L10n.otpEmailBody(phone)

        guard MFMailComposeViewController.canSendMail() else {
            if let emailUrl = OTPViewController.createEmailUrl(
                to: recipients,
                subject: subject,
                body: messageBody
            ) {
                UIApplication.shared.open(emailUrl)
            }
            return
        }
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients([recipients])
        mail.setSubject(subject)
        mail.setMessageBody(messageBody, isHTML: false)

        present(mail, animated: true)
    }
}
