//
//  LoadingSetupScreenViewController.swift
//  qlip
//
//  Created by Rahman Bramantya on 31/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import PromiseKit
struct InterestData {
    let chosenSkill: [String]
    let interest: String
}
private typealias `Self` = LoadingSetupScreenViewController
class LoadingSetupScreenViewController: BaseViewController {
    // MARK: - Properties

    lazy var imageView: UIImageView = {
        let image = UIImageView(image: Asset.General.loadingSetup.image)
        image.withSize(.init(width: 96, height: 96))
        image.contentMode = .scaleAspectFill

        return image
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: "Tunggu sebentar..\nQlip sedang menyiapkan video & konten seru,\nkhusus buat kamu!",
            font: .heavy14,
            textColor: Asset.Color.black.color,
            textAlignment: .center,
            numberOfLines: 0
        )
        label.lineBreakMode = .byWordWrapping

        return label
    }()
    var interestData: InterestData?
    var isSkipped: Bool = true
    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        if !isSkipped {
//            firstly {
//                self.view.showLoading()
//            }.then { _ in
//                APIService.SkillInterest.assignInterest(
//                    kidId: KidRealmModel.getId(),
//                    interestId: self.interestData!.chosenSkill,
//                    interestName: self.interestData?.interest ?? ""
//                )
//            }.compactMap {
//                return $0
//            }.done { [weak self] _ in
//                guard let self = self else { return }
//                self.showSuccessToast(message: "success kid register")
//            }.ensure { [weak self] in
//                guard let self = self else { return }
//                self.view.hideLoading()
//            }.catch { [weak self] error in
//                guard let self = self else { return }
//                self.handleError(error: error)
//            }
        } else {
            self.showTodoPopup(message: "gotohomescreen")
        }
    }
}
