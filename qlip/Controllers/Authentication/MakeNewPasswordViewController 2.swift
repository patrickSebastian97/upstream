//
//  MakeNewPasswordViewController.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import Foundation
import UIKit

private typealias `Self` = MakeNewPasswordViewController

class MakeNewPasswordViewController: BaseViewController {
    lazy var popupSuccess: UIImageView = {
        let view = UIImageView(
            image: Asset.General.newPasswordSuccess.image,
            contentMode: .scaleAspectFit
        )
        view.withWidth(200)
        view.withHeight(50)
        view.isHidden = true
        return view
    }()
    lazy var popupFail: UIImageView = {
        let view = UIImageView(
            image: Asset.General.newPasswordFail.image,
            contentMode: .scaleAspectFit
        )
        view.withWidth(200)
        view.withHeight(50)
        view.isHidden = true
        return view
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: L10n.makeNewPassword,
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: L10n.makeNewPassDesc,
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var insertNewPassword: UILabel = {
        let label = UILabel(
            text: L10n.makeNewPassword,
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var newPasswordField: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .line
        return textfield
    }()
    lazy var passwordMinLength: UILabel = {
        let label = UILabel(
            text: L10n.passwordMinimumLength,
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var passwordNeedCapAndNumber: UILabel = {
        let label = UILabel(
            text: L10n.passNeedCapitalNumber,
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var radioButton: UIImageView = {
        let view = UIImageView(
            image: Asset.General.passwordRadioBtn.image,
            contentMode: .scaleAspectFit
        )
        view.withWidth(12)
        view.withHeight(12)
        return view
    }()
    lazy var radioButton2: UIImageView = {
        let view = UIImageView(
            image: Asset.General.passwordRadioBtn.image,
            contentMode: .scaleAspectFit
        )
        view.withWidth(12)
        view.withHeight(12)
        return view
    }()
    lazy var createPasswordBtn: Button = {
        let button = Button(title: "Lanjutkan", type: .yellow)
        button.addTarget(self, action: #selector(createPasswordTapped), for: .touchUpInside)
        return button
    }()
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}

extension Self {
    @objc func createPasswordTapped () {
        validateNewPassword()
    }
    func validateNewPassword() {
        if newPasswordField.text!.count <= 6 {
            popupFail.isHidden = false
            popupSuccess.isHidden = true
        } else {
            popupSuccess.isHidden = false
            popupFail.isHidden = true
        }
    }
}
