//
//  OTPLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension OTPViewController {

    // MARK: - Initialize UI
    //swiftlint:disable function_body_length

    override func initializeUI() {
        super.initializeUI()

        let textFieldsView = UIStackView()
        textFieldsView.axis = .horizontal
        textFieldsView.spacing = UIStyle.Inset.inset8
        textFieldsView.distribution = .fillEqually
        textFields.forEach({
            let textField = $0
            textFieldsView.addArrangedSubview(textField)
            textField.snp.makeConstraints { maker in
                maker.height.equalTo(UIStyle.Button.height)
            }
        })

        let buttonWrapper = UIView()
        buttonWrapper.hstack(
            sendEmailButton,
            sendInstagramButton,
            spacing: UIStyle.Inset.inset16,
            distribution: .fillEqually
        )

        contactUsWrapperView.addSubview(contactUsLabel)
        contactUsWrapperView.addSubview(buttonWrapper)

        contactUsLabel.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().inset(UIStyle.Inset.inset16)
            maker.leading.trailing.equalToSuperview().inset(UIStyle.Inset.inset8)
        }
        buttonWrapper.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalToSuperview().inset(UIStyle.Inset.inset8)
            maker.top.equalTo(contactUsLabel.snp.bottom).inset(-UIStyle.Inset.inset8)
            maker.bottom.equalToSuperview().inset(UIStyle.Inset.inset16)
        }

        let view = scrollView.contentView

        view.vstack(
            view.hstack(titleLabel),
            view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(otpLabel).padTop(UIStyle.Inset.inset24),
            view.hstack(textFieldsView).padTop(UIStyle.Inset.inset8),
            view.hstack(errorLabel).padTop(UIStyle.Inset.inset8),
            view.hstack(resendCodeInstructionLabel, resendButtonLabel).padTop(UIStyle.Inset.inset32)
        ).withMargins(
            .init(
                top: UIStyle.Inset.defaultTop,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )

        self.view.addSubViews(contactUsWrapperView)
        contactUsWrapperView.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().inset(UIStyle.Inset.left)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.right)
            maker.bottom.equalTo(contactUsWrapperView.superview!.safeAreaLayoutGuide.snp.bottom)
                .inset(UIStyle.Inset.inset40)
        }
    }
}
