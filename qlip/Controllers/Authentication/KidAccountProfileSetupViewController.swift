//
//  KidAccountProfileSetup.swift
//  qlip
//
//  Created by Rahman Bramantya on 21/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import PromiseKit

private typealias `Self` = KidAccountProfileSetupViewController

class KidAccountProfileSetupViewController: BaseViewController, KidProfileNavigator {

    // MARK: - Configs

    override var shouldAttachScrollView: Bool { return true }

    // MARK: - Properties

    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: L10n.chooseYourProfile,
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: L10n.chooseYourRegisteredParent,
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var kidAvatarCollectionController: KidAccountProfileSetupCollectionViewController = {
        let controller = KidAccountProfileSetupCollectionViewController()
        controller.kidDelegate = self

        return controller
    }()
    var kidProfileData: [KidRealmModel] = [] {
        didSet {
            kidAvatarCollectionController.items = kidProfileData.map({
                    ChooseKidProfileViewModel(item: $0)
                })
        }
    }
    var isKidSocialMedia: Bool = false
    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        kidRegistrationChooseProfileEventTracking()
    }

    // MARK: - Helpers
    private func registerAsKid(userId: String, phone: String? = nil) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.registerAsKid(userId: userId, phone: phone)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.setKidUser(user: data)
            if Storage.EventTracking.getFirstKidRegistered() == nil {
                Storage.EventTracking.saveFirstKidRegistered(date: Date())
            }
            self.kidSuccessRegistrationEventTracking()
            self.goToSelectInterest(kidName: data.firstName, kidId: data.id)
            self.kidAddProfileOpenOnboarding1EventTracking()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func registerAsKidSocialMedia(userId: String, phone: String? = nil) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.registerAsKidBySocialMedia(userId: userId, phone: phone)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.setKidUser(user: data)
            if Storage.EventTracking.getFirstKidRegistered() == nil {
                Storage.EventTracking.saveFirstKidRegistered(date: Date())
            }
            self.kidSuccessRegistrationEventTracking()
            self.goToSelectInterest(kidName: data.firstName, kidId: data.id)
            self.kidAddProfileOpenOnboarding1EventTracking()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
}

// MARK: - Kid Account Profile Setup List Controller Delegate

extension  Self: KidAccountProfileSetupListControllerDelegate {
    func didSelect(_ viewController: KidAccountProfileSetupCollectionViewController, index: Int) {
        let phone = UserDataManager.shared.getData()?.phone
        let userId = self.kidProfileData[index].id
        if isKidSocialMedia {
            registerAsKidSocialMedia(userId: userId, phone: phone)
        } else {
            registerAsKid(userId: userId, phone: phone)
        }
    }
}
