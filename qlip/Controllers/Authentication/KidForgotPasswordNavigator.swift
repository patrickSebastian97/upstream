//
//  KidForgotPasswordNavigator.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 03/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

protocol KidForgotPasswordNavigator {
    func goToKidParentPhoneOTP(phone: String, otpType: OTPType, username: String)
}

extension KidForgotPasswordNavigator where Self: UIViewController {
    func goToKidParentPhoneOTP(phone: String, otpType: OTPType, username: String) {
        let controller = OTPViewController()
        controller.hidesBottomBarWhenPushed = true
        controller.codeSentTo = phone
        controller.username = username
        controller.otpType = otpType
        controller.isKidForgotWithParentPhone = true
        navigationController?.pushViewController(controller, animated: true)
    }
}
