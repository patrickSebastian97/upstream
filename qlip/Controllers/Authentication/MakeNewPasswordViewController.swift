//
//  MakeNewPasswordViewController.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.

import Foundation
import UIKit
import PromiseKit

private typealias `Self` = MakeNewPasswordViewController

class MakeNewPasswordViewController: BaseViewController {

    // MARK: - Properties
    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: L10n.makeNewPassword,
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: L10n.makeNewPassDesc,
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var newPasswordTextField: TextFieldView = {
        let view = TextFieldView(
            title: L10n.password,
            placeholder: L10n.makeNewPassword,
            keyboardType: .password
        )
        view.textFieldDelegate = self
        view.isUsingValidationView = true

        return view
    }()
    lazy var createPasswordBtn: BottomButtonView = {
        let button = BottomButtonView(
            title: L10n.lanjutkan,
            type: .yellow
        )
        button.actionButton.isStyleEnabled = false
        button.actionButton.addTarget(self, action: #selector(validateNewPassword), for: .touchUpInside)

        return button
    }()
    var kidUsername: String = ""
    var phone: String = ""
    var token: String = ""
    var userType: UserType = UserType(rawValue: Storage.User.getUserType()) ?? .kid

    // MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: - Helpers
    override func updateButtonState() {
        let textFields = [newPasswordTextField]
        let isEmpty = !textFields.filter({ $0.isEmpty() }).isEmpty
        let isValid = textFields.filter({ $0.hasError() }).isEmpty
        let password = newPasswordTextField.text?.trim ?? ""
        let min = SettingValidation.Password.minLength
        let lengthValid = newPasswordTextField.validate(
            type: .minLength(length: min),
            textToValidate: password
        )
        let capitalValid = newPasswordTextField.validate(type: .password, textToValidate: password)
        newPasswordTextField.setPasswordValidate(minLengthValid: lengthValid, capitalValid: capitalValid)
        createPasswordBtn.actionButton.isStyleEnabled = isValid && !isEmpty && lengthValid && capitalValid
    }

    // MARK: - Actions
    func parentMakeNewPassword(phone: String, token: String, password: String, confirmPassword: String) {
        firstly {
            self.view.showLoading()
        }.then {_ in
            APIService.Authentication.createNewPassword(
                phone: phone, token: token, password:
                password, confirmPassword: confirmPassword)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else {return}
            self.showSuccessToast(message: L10n.newPasswordSuccess)
            self.parentForgotPasswordSuccessEventTracking()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
            }
        }
    func kidMakeNewPassword(
        username: String? = nil,
        phone: String? = nil,
        token: String,
        password: String,
        confirmPassword: String
    ) {
        firstly {
            self.view.showLoading()
        }.then {_ in
        APIService.Authentication.createNewPasswordKid(
            username: username,
            phone: phone,
            token: token,
            password: password,
            confirmPassword: confirmPassword
        )
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else {return}
            self.showSuccessToast(message: L10n.newPasswordSuccess)
            self.kidForgotPasswordSuccessEventTracking()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
            }
        }
    }

extension Self {
    @objc func validateNewPassword() {
        let password = newPasswordTextField.text?.trim ?? ""
        let token = self.token
        let phone = self.phone
        let kidUsername = self.kidUsername
        switch userType {
        case .parent :
            parentMakeNewPassword(
                phone: phone,
                token: token,
                password: password,
                confirmPassword: password
            )
        case .kid :
            if kidUsername.isEmpty {
                kidMakeNewPassword(
                    phone: phone,
                    token: token,
                    password: password,
                    confirmPassword: password
                )
            } else {
                kidMakeNewPassword(
                    username: kidUsername,
                    token: token,
                    password: password,
                    confirmPassword: password
                )
            }
        }
        forgotPasswordSubmitNewPasswordEventTracking()
    }
}

// MARK: - Textfield View Delegate
extension Self: TextFieldDelegate {
    func textField(didValidate textField: TextFieldView) {
        updateButtonState()
    }
    func textField(shouldBeginEditing textField: TextFieldView) -> Bool {
        forgotPasswordFillNewEventTracking()
        return true
    }
}
