//
//  CreateKidProfileLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension CreateKidProfileViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        let infoImageWrapper = UIView()
        infoImageWrapper.addSubview(infoImageView)
        infoImageView.snp.makeConstraints { (maker) in
            maker.centerX.equalToSuperview()
        }
        let view = scrollView.contentView
        view.vstack(
            view.hstack(titleLabel),
            view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(kidNameTextField).padTop(UIStyle.Inset.inset24),
            view.hstack(kidDobTextField).padTop(UIStyle.Inset.inset16),
            view.hstack(bottomButtonView),
            view.hstack(
                infoImageWrapper,
                infoLabel,
                spacing: UIStyle.Inset.inset16,
                distribution: .fillProportionally
            )
        ).withMargins(
            .init(
                top: UIStyle.Inset.defaultTop,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )
    }
}
