//
//  KidForgotPasswordLayout.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 03/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit

extension KidForgotPasswordViewController {
    override func initializeUI() {
        super.initializeUI()
        let view = scrollView.contentView
        view.vstack(
        view.hstack(titleLabel),
        view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
        view.hstack(usernameTextField).padTop(UIStyle.Inset.inset24),
        view.vstack(phoneNumberTextField).padTop(UIStyle.Inset.inset16),
        view.vstack(nextButton).padTop(UIStyle.Inset.inset24)
        ).withMargins(.init(
                    top: UIStyle.Inset.defaultTop,
                    left: UIStyle.Inset.left,
                    bottom: UIStyle.Inset.bottom,
                    right: UIStyle.Inset.right
                )
            )
    }
}
