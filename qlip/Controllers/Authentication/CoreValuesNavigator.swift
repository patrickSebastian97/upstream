//
//  CoreValuesNavigator.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.

import UIKit

protocol CoreValuesNavigator {
    func goToWelcomeScreenAsParent()
    func goToWelcomeScreenAsKid()
}

extension CoreValuesNavigator where Self: UIViewController {
    func goToWelcomeScreenAsParent() {
        Storage.User.setUserType(type: UserType.parent.rawValue)
        let controller = WelcomePageViewController()
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToWelcomeScreenAsKid() {
        Storage.User.setUserType(type: UserType.kid.rawValue)
        let controller = WelcomePageViewController()
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
}
