//
//  LoginAndRegisterViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 15/12/20.
//  Copyright © 2020 Qlip. All rights reserved.

import UIKit
import PromiseKit
import AuthenticationServices
import Firebase
import GoogleSignIn
import FacebookLogin

private typealias `Self` = LoginAndRegisterViewController

// swiftlint:disable type_body_length
class LoginAndRegisterViewController: BaseViewController, AdaptableKeyboard, DismissableKeyboard,
LoginAndRegisterNavigator {

    // MARK: - Properties

    enum ScreenType {
        case login
        case register
    }

    enum SocialMediaType: String {
        case apple
        case facebook
        case google
    }

    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var usernameTextField: TextFieldView = {
        let view = TextFieldView(
            title: "",
            placeholder: "",
            keyboardType: .normal
        )
        view.setValidations(
            validations: [
                (.whitespace, message: L10n.whitespaceNotAllowed),
                (.username, message: L10n.specialCharNotAllowrd),
                (.minLength(
                    length: SettingValidation.Username.minLength),
                 message: "min. \(SettingValidation.Username.minLength)"),
                (.maxLength(
                    length: SettingValidation.Username.maxLength),
                 message: "max. \(SettingValidation.Username.maxLength)"),
                (.required, message: "")
                ].map(TextFieldView.Validation.init)
        )
        view.textFieldDelegate = self

        return view
    }()
    lazy var passwordTextField: TextFieldView = {
        let view = TextFieldView(
            title: "",
            placeholder: "",
            keyboardType: .password
        )
        switch screenType {
        case .login:
            let min = SettingValidation.Password.minLength
            view.setValidations(
                validations: [
                    (.whitespace, message: "Spasi tidak diperbolehkan"),
                    (.minLength(length: min), message: L10n.passwordLengthValidation(min)),
                    (.password, message: L10n.passwordCapitalValidation),
                    (.required, message: "")
                    ].map(TextFieldView.Validation.init)
            )
        case .register:
            view.isUsingValidationView = true
        }
        view.textFieldDelegate = self

        return view
    }()
    lazy var forgotPasswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(L10n.forgotPasswordQuestion, for: .normal)
        button.setTitleColor(Asset.Color.blue.color, for: .normal)
        button.titleLabel?.font = .medium14
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(forgotPasswordTapped), for: .touchUpInside)

        return button
    }()
    lazy var bottomButtonView: BottomButtonView = {
        let button = BottomButtonView(
            title: "",
            type: .yellow
        )
        button.actionButton.isStyleEnabled = false
        button.actionButton.addTarget(self, action: #selector(bottomButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var orLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .center,
            numberOfLines: 1
        )

        return label
    }()
    lazy var facebookButton: Button = {
        let button = Button(
            title: "",
            type: .white
        )
        button.setImage(
            Asset.Authentication.facebookLogo.image.withRenderingMode(
                .alwaysOriginal),
            for: .normal
        )
        button.withHeight(UIStyle.Button.height)
        button.addTarget(self, action: #selector(facebookSignInTapped), for: .touchUpInside)

        return button
    }()
    lazy var googleButton: Button = {
        let button = Button(
            title: "",
            type: .white)
        button.setImage(
            Asset.Authentication.googleLogo.image.withRenderingMode(
                .alwaysOriginal
            ),
            for: .normal
        )
        button.withHeight(UIStyle.Button.height)
        button.addTarget(self, action: #selector(googleSignInTapped), for: .touchUpInside)

        return button
    }()
    lazy var appleButton: Button = {
        let button = Button(title: "", type: .white)
        button.withHeight(UIStyle.Button.height)
        button.setImage(
            Asset.Authentication.appleLogoBlack
                .image.withRenderingMode(
                .alwaysOriginal),
            for: .normal
        )
        button.addTarget(self, action: #selector(appleSignInTapped), for: .touchUpInside)

        return button
    }()

    var screenType: ScreenType = .login
    var userType: UserType = UserType(rawValue: Storage.User.getUserType()) ?? .kid

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        enableDismissableKeyboard()
        enableAdaptableKeyboard()
        setupGoogleSignInView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateButtonState()
    }

    override func setupViews() {
        super.setupViews()

        switch userType {
        case .kid:
            descriptionLabel.text = L10n.loginKidDescription
        case .parent:
            descriptionLabel.text = L10n.loginParentDescription
        }

        switch screenType {
        case .login:
            usernameTextField.titleLabel.text = L10n.phoneOrUsername
            usernameTextField.placeholder = L10n.phoneOrUsernamePlaceholder
            passwordTextField.titleLabel.text = L10n.password
            passwordTextField.placeholder = L10n.passwordLoginPlaceholder
            titleLabel.text = L10n.loginTitle
            bottomButtonView.actionButton.setTitle(L10n.login, for: .normal)
            orLabel.text = L10n.orLoginWith
        case .register:
            usernameTextField.titleLabel.text = L10n.accountName
            usernameTextField.placeholder = L10n.accountNamePlaceholder
            passwordTextField.titleLabel.text = L10n.password
            passwordTextField.placeholder = L10n.passwordRegisterPlaceholder
            titleLabel.text = L10n.registerTitle
            descriptionLabel.text = L10n.registerDescription
            bottomButtonView.actionButton.setTitle(L10n.registration, for: .normal)
            forgotPasswordButton.isHidden = true
            orLabel.text = L10n.orRegistrationWith
        }
    }

    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)

        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }

    deinit {
        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }

    // MARK: - Helpers
    override func updateButtonState() {
        let textFields = [usernameTextField, passwordTextField]
        let isEmpty = !textFields.filter({ $0.isEmpty() }).isEmpty
        let isValid = textFields.filter({ $0.hasError() }).isEmpty
        let password = passwordTextField.text ?? ""
        let min = SettingValidation.Password.minLength
        let lengthValid = passwordTextField.validate(type: .minLength(length: min), textToValidate: password)
        let capitalValid = passwordTextField.validate(type: .password, textToValidate: password)
        passwordTextField.setPasswordValidate(minLengthValid: lengthValid, capitalValid: capitalValid)
        bottomButtonView.actionButton.isStyleEnabled = isValid && !isEmpty && lengthValid && capitalValid
    }

    private func parentLogin(id: String, password: String, token: String) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.loginParent(
                id: id,
                password: password,
                token: token
            )
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            let phone = data.phone
            let uncompleteRegistered = !data.isCompleteRegister
            if uncompleteRegistered {
                let token = TokenModel(
                    tokenType: data.tokenType ?? "",
                    expiresIn: data.expiresIn ?? 0,
                    accessToken: data.accessToken ?? ""
                )
                TokenModel.Storage.delete()
                TokenModel.Storage.save(tokenModel: token)
                Storage.ParentData.save(data: data)
                if phone.isEmpty {
                    self.goToInsertPhoneNumber(otpType: .updatePhone)
                } else {
                    self.goToCreateKidProfile(isParentDontHaveKid: true)
                }
            } else {
                self.setParentUser(user: data)
                self.gotoHomeScreen()
                self.parentLoginSuccessEventTracking()
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func kidLogin(id: String, password: String, token: String) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.loginKid(
                id: id,
                password: password,
                token: token
            )
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            let isRegistered = (data.isRegistered ?? true)
            if !isRegistered {
                self.goToInsertPhoneNumber(otpType: .updatePhone)
            } else {
                self.setKidUser(user: data)
                self.gotoHomeScreen()
                self.kidLoginSuccessEventTracking()
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func parentSocialMediaSignIn(
        socialMediaType: SocialMediaType,
        providerToken: String,
        firstname: String? = nil,
        lastname: String? = nil
    ) {
        let token = Firebase.shared.getFCMToken() ?? ""
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.parentSocialMediaSignIn(
                providerType: socialMediaType.rawValue,
                providerToken: providerToken,
                registrationId: token,
                firstname: firstname,
                lastname: lastname
            )
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            let phone = data.phone
            let uncompleteRegistered = !data.isCompleteRegister
            if uncompleteRegistered {
                let token = TokenModel(
                    tokenType: data.tokenType ?? "",
                    expiresIn: data.expiresIn ?? 0,
                    accessToken: data.accessToken ?? ""
                )
                TokenModel.Storage.delete()
                TokenModel.Storage.save(tokenModel: token)
                Storage.ParentData.save(data: data)
                if phone.isEmpty {
                    self.goToInsertPhoneNumber(otpType: .updatePhone)
                } else {
                    self.goToCreateKidProfile(isSocialMediaSignIn: true)
                }
                self.regisSocialMediaEventTracking(socialMediaType: socialMediaType)
            } else {
                self.setParentUser(user: data)
                self.gotoHomeScreen()
                self.loginSocialMediaEventTracking(socialMediaType: socialMediaType)
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func kidSocialMediaSignIn(
        socialMediaType: SocialMediaType,
        providerToken: String,
        firstname: String? = nil,
        lastname: String? = nil
    ) {
        let token = Firebase.shared.getFCMToken() ?? ""
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.kidSocialMediaSignIn(
                providerType: socialMediaType.rawValue,
                providerToken: providerToken,
                registrationId: token,
                firstname: firstname,
                lastname: lastname
            )
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            let isRegistered = data.isRegistered ?? true
            if !isRegistered {
                UserDataManager.shared.setProviderType(data.providerType ?? "")
                UserDataManager.shared.setProviderToken(data.providerToken ?? "")
                UserDataManager.shared.setProviderId(data.providerId ?? "")
                UserDataManager.shared.setUsername(username: data.username ?? "")
                self.goToInserPhoneNumberKidSocialMedia(otpType: .register)
                self.regisSocialMediaEventTracking(socialMediaType: socialMediaType)
            } else {
                self.setKidUser(user: data)
                self.gotoHomeScreen()
                self.loginSocialMediaEventTracking(socialMediaType: socialMediaType)
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func setupGoogleSignInView() {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }

}

// MARK: - Textfield View Delegate
extension Self: TextFieldDelegate {
    func textField(didValidate textField: TextFieldView) {
        updateButtonState()
    }
    func textField(shouldBeginEditing textField: TextFieldView) -> Bool {
        if screenType == .register {
            if textField == usernameTextField {
            regisFillNameEventTracking()
            }
            if textField == passwordTextField {
            regisFillPasswordEventTracking()
            }
        }
        if screenType == .login {
            if textField == usernameTextField {
            loginFillPhoneNumberEventTracking()
            }
            if textField == passwordTextField {
            loginFillPasswordEventTracking()
            }
        }
    return true
    }
}

// MARK: - ASAuthorization Controller Delegate
extension Self: ASAuthorizationControllerDelegate {

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        guard let error = error as? ASAuthorizationError else {return}
        switch error.code {
        case .canceled:
            Log.debug(error)
        case .unknown:
            Log.debug(error)
        case .invalidResponse:
            Log.debug(error)
        case .notHandled:
            Log.debug(error)
        case .failed:
            Log.debug(error)
        @unknown default:
            handleError(error: error)
        }
    }

    func authorizationController(
        controller: ASAuthorizationController,
        didCompleteWithAuthorization authorization: ASAuthorization
    ) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            // Create an account in your system.
            let firstName = appleIDCredential.fullName?.givenName
            let lastName = appleIDCredential.fullName?.familyName
            guard let token = appleIDCredential.identityToken else { return }
            guard let providerToken = String(bytes: token, encoding: .utf8) else { return }
            switch userType {
            case .kid:
                kidSocialMediaSignIn(
                    socialMediaType: .apple,
                    providerToken: providerToken,
                    firstname: firstName,
                    lastname: lastName
                )
            case .parent:
                parentSocialMediaSignIn(
                    socialMediaType: .apple,
                    providerToken: providerToken,
                    firstname: firstName,
                    lastname: lastName
                )
            }
        default:
            break
        }
    }
}

// MARK: - ASAuthorization Presentation Context Providing
extension Self: ASAuthorizationControllerPresentationContextProviding {

    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

// MARK: Google Sign In Delegate
extension Self: GIDSignInDelegate {

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
          if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
            print("The user has not signed in before or they have since signed out.")
          } else {
            Log.debug("\(error.localizedDescription)")
          }
          return
        }
        guard let token = user.authentication.idToken else { return }
        switch userType {
        case .kid:
            kidSocialMediaSignIn(socialMediaType: .google, providerToken: token)
        case .parent:
            parentSocialMediaSignIn(socialMediaType: .google, providerToken: token)
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            Log.debug(error.localizedDescription)
        }
    }
}

// MARK: - Actions
// swiftlint:disable file_length
extension Self {

    @objc private func forgotPasswordTapped() {
        goToResetPassword()
        loginForgotPasswordEventTracking()
    }

    @objc private func bottomButtonTapped() {
        switch screenType {
        case .login:
            hideKeyboard()
            let id = usernameTextField.text?.trim ?? ""
            let password = passwordTextField.text?.trim ?? ""
            let token = Firebase.shared.getFCMToken() ?? ""
            switch userType {
            case .parent:
                parentLogin(id: id, password: password, token: token)
            case .kid:
                kidLogin(id: id, password: password, token: token)
            }
            loginClickLoginEventTracking()
        case .register:
            hideKeyboard()
            let username = usernameTextField.text?.trim ?? ""
            let password = passwordTextField.text?.trim ?? ""
            self.bottomButtonView.actionButton.isStyleEnabled = false
            firstly {
                self.view.showLoading()
            }.then { _ in
                APIService.Authentication.checkUsernamePassword(username: username, password: password)
            }.compactMap {
                return $0
            }.done { [weak self] _ in
                guard let self = self else { return }
                UserDataManager.shared.setUsername(username: username)
                UserDataManager.shared.setPassword(password: password)
                self.goToInsertPhoneNumber(otpType: .register)
            }.ensure { [weak self] in
                guard let self = self else { return }
                self.view.hideLoading()
            }.catch { [weak self] error in
                guard let self = self else { return }
                self.handleError(error: error)
            }
        }
    }

    @objc private func appleSignInTapped() {
        hideKeyboard()
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName]

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }

    @objc private func googleSignInTapped() {
        GIDSignIn.sharedInstance()?.signIn()
    }

    @objc private func facebookSignInTapped() {
        let fbLoginManager = LoginManager()
        fbLoginManager.logIn(permissions: [], from: self) { (result, error) in
            if error != nil {
                Log.debug(error!)
                return
            } else if result?.isCancelled ?? false {
                Log.debug("Facebook Sign in Cancelled")
            } else {
                if let token = AccessToken.current?.tokenString {
                    switch self.userType {
                    case .kid:
                        self.kidSocialMediaSignIn(socialMediaType: .facebook, providerToken: token)
                    case .parent:
                        self.parentSocialMediaSignIn(socialMediaType: .facebook, providerToken: token)
                    }
                }
            }
        }
    }
}
