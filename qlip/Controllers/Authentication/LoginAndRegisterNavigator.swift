//
//  LoginAndRegisterNavigator.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

protocol LoginAndRegisterNavigator {
    func goToInsertPhoneNumber(otpType: OTPType)
    func goToInserPhoneNumberKidSocialMedia(otpType: OTPType)
    func goToCreateKidProfile(isSocialMediaSignIn: Bool)
    func goToCreateKidProfile(isParentDontHaveKid: Bool)
}

extension LoginAndRegisterNavigator where Self: UIViewController {
    func goToInsertPhoneNumber(otpType: OTPType) {
        let controller = InsertPhoneNumberViewController()
        controller.screenType = .register
        controller.otpType = otpType
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToCreateKidProfile(isSocialMediaSignIn: Bool) {
        let controller = CreateKidProfileViewController()
        controller.isSocialMediaSignIn = isSocialMediaSignIn
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToCreateKidProfile(isParentDontHaveKid: Bool) {
        let controller = CreateKidProfileViewController()
        controller.isParentDontHaveKid = isParentDontHaveKid
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToResetPassword() {
        let controller = InsertPhoneNumberViewController()
        controller.screenType = .forgotPassword
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToInserPhoneNumberKidSocialMedia(otpType: OTPType) {
        let controller = InsertPhoneNumberViewController()
        controller.screenType = .register
        controller.otpType = otpType
        controller.isKidSocialMedia = true
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
}
