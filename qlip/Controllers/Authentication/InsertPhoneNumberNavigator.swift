//
//  InsertPhoneNumberNavigator.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

protocol InsertPhoneNumberNavigator {
    func goToRegisterOTP(phone: String, otpType: OTPType, isKidSocialMedia: Bool)
    func goToParentPhoneNumber(isKidSocialMedia: Bool)
    func goToKidAccountProfileSetup(kidProfileData: [KidRealmModel])
    func goToKidAccountProfileWithSocialMedia(kidProfileData: [KidRealmModel])
    func goToSelectInterest(kidName: String, kidId: String)
    func goToResetPasswordOTP(phone: String, otpType: OTPType)
    func goToResetPasswordKid(phone: String, otpType: OTPType)
    func goToKidNoPhoneNumber()
}

extension InsertPhoneNumberNavigator where Self: UIViewController {
    func goToRegisterOTP(phone: String, otpType: OTPType, isKidSocialMedia: Bool) {
        let controller = OTPViewController()
        controller.codeSentTo = phone
        controller.isKidSocialMedia = isKidSocialMedia
        controller.otpType = otpType
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToParentPhoneNumber(isKidSocialMedia: Bool) {
        let controller = InsertPhoneNumberViewController()
        controller.hidesBottomBarWhenPushed = true
        controller.screenType = .parentPhoneNumber
        controller.isKidSocialMedia = isKidSocialMedia
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToKidAccountProfileSetup(kidProfileData: [KidRealmModel]) {
        let controller = KidAccountProfileSetupViewController()
        controller.kidProfileData = kidProfileData
    }
    func goToResetPasswordOTP(phone: String, otpType: OTPType) {
        let controller = OTPViewController()
        controller.hidesBottomBarWhenPushed = true
        controller.codeSentTo = phone
        controller.otpType = otpType
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToKidAccountProfileWithSocialMedia(kidProfileData: [KidRealmModel]) {
        let controller = KidAccountProfileSetupViewController()
        controller.kidProfileData = kidProfileData
        controller.isKidSocialMedia = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToSelectInterest(kidName: String, kidId: String) {
        let controller = SelectInterestSkillViewController()
        controller.onboardingType = .selectInterest
        controller.kidName = kidName
        controller.kidId = kidId
        self.navigationController?.pushViewController(controller, animated: true)
        }
    func goToResetPasswordKid(phone: String, otpType: OTPType) {
        let controller = OTPViewController()
        controller.hidesBottomBarWhenPushed = true
        controller.codeSentTo = phone
        controller.otpType = otpType
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToKidNoPhoneNumber() {
        let controller = KidForgotPasswordViewController()
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
}
