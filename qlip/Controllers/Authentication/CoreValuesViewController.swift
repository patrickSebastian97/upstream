//
//  CoreValuesViewController.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 14/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import AVFoundation
import LBTATools
import CoreMedia

private typealias `Self` = CoreValuesViewController

class CoreValuesViewController: BaseViewController, CoreValuesNavigator, AppNotificationCenter {

    // MARK: - Properties

    lazy var asParentButton: Button = {
        let button = Button(title: L10n.enterAsParent, type: .clear)
        button.withWidth(250)
        button.withHeight(UIStyle.Button.height)
        button.layer.cornerRadius = UIStyle.Button.cornerRadius
        button.isHidden = true
        button.addTarget(self, action: #selector(enterAsParent), for: .touchUpInside)
        return button
    }()

    lazy var asKidButton: Button = {
        let button = Button(title: L10n.enterAsKid, type: .clear)
        button.withWidth(250)
        button.withHeight(UIStyle.Button.height)
        button.layer.cornerRadius = UIStyle.Button.cornerRadius
        button.isHidden = true
        button.addTarget(self, action: #selector(enterAsKid), for: .touchUpInside)
        return button
    }()

    lazy var logoImageView: UIImageView = {
        let view = UIImageView(image: Asset.General.logoWhite.image)
        view.withHeight(45)
        view.withWidth(70)
        view.isHidden = true
        return view
    }()

    lazy var logoContainer: UIView = {
        let view = UIView()
        view.isHidden = true
        return view
    }()

    var player: AVPlayer?
    var delayTime = Double()
    var videoDuration = Double()

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()
        playBackgroundVideo()
        checkFirstLaunch()
        delayUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        guard let player = player else { return }
        player.play()

        openOnboardingEventTracking()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        guard let player = player else { return }
        player.pause()
        player.seek(to: .zero)
    }

    // MARK: - Helpers
    func delayUI() {
        DispatchQueue.main.asyncAfter(deadline: .now() + delayTime) {
            self.logoImageView.isHidden = false
            self.asParentButton.isHidden = false
            self.asKidButton.isHidden = false
            self.logoContainer.isHidden = false
        }
    }
    @objc func playerItemDidReachEnd() {
        player!.seek(to: CMTime.zero) }
    func checkFirstLaunch() {
        if Storage.UserFirstLogin.getUserFirstLogin == true {
            Storage.UserFirstLogin.setUserFirstLogin()
            delayTime = 0
        } else {
            Storage.UserFirstLogin.setUserFirstLogin()
            delayTime = videoDuration
        }
    }

    func playBackgroundVideo() {
        let path = Bundle.main.path(forResource: "PlaceholderVideo", ofType: ".mp4")
        player = AVPlayer(url: URL(fileURLWithPath: path ?? ""))
        player!.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.view.layer.insertSublayer(playerLayer, at: 0)
        notifyInternalVideoLooper(player: player)
        player!.seek(to: CMTime.zero)
        player!.play()
        self.player?.isMuted = true
        let vidDuration = player?.currentItem?.asset.duration
        let convertedDuration = Double(CMTimeGetSeconds(
        vidDuration ?? CMTime(seconds: 1, preferredTimescale: 100)))
        videoDuration = convertedDuration
    }
}

// MARK: - Actions

extension Self {

    @objc func enterAsParent() {
        goToWelcomeScreenAsParent()
        parentStartEventTracking()
    }

    @objc func enterAsKid() {
        goToWelcomeScreenAsKid()
        kidStartEventTracking()
    }
}
