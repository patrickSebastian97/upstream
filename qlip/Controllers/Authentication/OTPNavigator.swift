//
//  OTPNavigator.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

protocol OTPNavigator {
    func goToCreateKidProfile(isSocialMediaSignIn: Bool)
    func goToMakeNewPassword(phone: String, token: String)
    func goToNewPasswordKidParentPhone(phone: String, token: String, username: String)
    func goToParentPhoneNumber(isKidSocialMedia: Bool)
}

extension OTPNavigator where Self: UIViewController {
    func goToCreateKidProfile(isSocialMediaSignIn: Bool) {
        let controller = CreateKidProfileViewController()
        controller.isSocialMediaSignIn = isSocialMediaSignIn
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToParentPhoneNumber(isKidSocialMedia: Bool) {
        let controller = InsertPhoneNumberViewController()
        controller.screenType = .parentPhoneNumber
        controller.isKidSocialMedia = isKidSocialMedia
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToMakeNewPassword(phone: String, token: String) {
        let controller = MakeNewPasswordViewController()
        controller.phone = phone
        controller.token = token
        navigationController?.pushViewController(controller, animated: true)
    }
    func goToNewPasswordKidParentPhone(phone: String, token: String, username: String) {
        let controller = MakeNewPasswordViewController()
        controller.token = token
        controller.kidUsername = username
        controller.phone = phone
        navigationController?.pushViewController(controller, animated: true)
    }
}
