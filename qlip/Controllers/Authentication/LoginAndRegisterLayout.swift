//
//  LoginAndRegisterLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 15/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension LoginAndRegisterViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        let view = scrollView.contentView

        view.vstack(
            view.hstack(titleLabel),
            view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(usernameTextField).padTop(UIStyle.Inset.inset24),
            view.hstack(passwordTextField).padTop(UIStyle.Inset.inset16),
            view.hstack(forgotPasswordButton,
                        UIView(),
                        distribution: .equalCentering
                        ),
            view.hstack(bottomButtonView),
            view.hstack(orLabel),
            view.hstack(appleButton,
                        facebookButton,
                        googleButton,
                        spacing: UIStyle.Inset.inset8,
                        distribution: .fillEqually
            ).padTop(UIStyle.Inset.inset16)
        ).withMargins(
            .init(
                top: UIStyle.Inset.defaultTop,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )
    }
}
