//
//  KidForgotPasswordViewController.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 03/01/21.
//  Copyright © 2021 Qlip. All rights reserved.

import UIKit
import PromiseKit

private typealias `Self` = KidForgotPasswordViewController

class KidForgotPasswordViewController: BaseViewController, KidForgotPasswordNavigator {

    // MARK: - Properties
    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: L10n.forgotPassword,
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: L10n.kidForgotPasswordDesc,
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var usernameTextField: TextFieldView = {
        let view = TextFieldView(
            title: "Nama akun kamu",
            placeholder: "",
            keyboardType: .normal
        )
        view.setValidations(
            validations: [
                (.whitespace, message: L10n.whitespaceNotAllowed),
                (.username, message: L10n.specialCharNotAllowrd),
                (.minLength(length: SettingValidation.Username.minLength), message: "min. 5"),
                (.maxLength(length: SettingValidation.Username.maxLength), message: "max. 20"),
                (.required, message: "")
                ].map(TextFieldView.Validation.init)
        )
        view.textFieldDelegate = self

        return view
    }()
    lazy var phoneNumberTextField: TextFieldView = {
        let view = TextFieldView(
            title: L10n.kidInsertParentPhone,
            placeholder: nil,
            keyboardType: .phonePad
        )
        view.setValidations(
            validations: [
                (.phonenumber, message: "invalid phone number"),
                (.minLength(length: SettingValidation.PhoneNumber.minLength), message: "min. 8"),
                (.maxLength(length: SettingValidation.PhoneNumber.maxLength), message: "max. 12"),
                (.required, message: "")
                ].map(TextFieldView.Validation.init)
        )
        view.textFieldDelegate = self

        return view
    }()
    lazy var nextButton: BottomButtonView = {
        let button = BottomButtonView(
            title: L10n.lanjutkan,
            type: .yellow
        )
        button.actionButton.isStyleEnabled = false
        button.actionButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)

        return button
    }()
    // MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func setupViews() {
        super.setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.updateButtonState()
    }

    // MARK: - Helpers
    override func updateButtonState() {
        let textFields = [phoneNumberTextField]
        let isEmpty = !textFields.filter({ $0.isEmpty() }).isEmpty
        let isValid = textFields.filter({ $0.hasError() }).isEmpty
        nextButton.actionButton.isStyleEnabled = isValid && !isEmpty
    }

    private func requestOTP(phone: String, type: OTPType, username: String) {
        hideKeyboard()
        nextButton.actionButton.isStyleEnabled = false
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.requestOTP(phone: phone, type: type.rawValue, username: username)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            self.goToKidParentPhoneOTP(phone: phone, otpType: .forgotPassword, username: username)
            self.kidForgotPasswordSubmitParentPhoneEventTracking()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
}

// MARK: - Textfield View Delegate
extension Self: TextFieldDelegate {
    func textField(didValidate textField: TextFieldView) {
        self.updateButtonState()
    }
    func textField(shouldBeginEditing textField: TextFieldView) -> Bool {
        kidForgotPasswordFillParentPhoneEventTracking()
    return true
    }
}

// MARK: - Actions
extension Self {
    @objc private func nextButtonTapped() {
    let kidUsername = usernameTextField.text?.trim ?? ""
    let parentPhone = phoneNumberTextField.text?.trim ?? ""
    requestOTP(phone: parentPhone, type: OTPType.forgotPassword, username: kidUsername)
    }
}
