//
//  MakeNewPasswordLayout.swift
//  qlip
//
//  Created by Patrick Sebastian Lie on 18/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension MakeNewPasswordViewController {

    override func initializeUI() {
        super.initializeUI()
        let view = scrollView.contentView
        view.vstack(
        view.hstack(titleLabel),
        view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
        view.hstack(newPasswordTextField).padTop(UIStyle.Inset.inset24),
        view.vstack(createPasswordBtn).padTop(UIStyle.Inset.inset24)
        ).withMargins(.init(
                    top: UIStyle.Inset.defaultTop,
                    left: UIStyle.Inset.left,
                    bottom: UIStyle.Inset.bottom,
                    right: UIStyle.Inset.right
                )
            )
    }
}
