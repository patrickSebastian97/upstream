//
//  InsertPhoneNumberViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import PromiseKit

private typealias `Self` = InsertPhoneNumberViewController

class InsertPhoneNumberViewController: BaseViewController, DismissableKeyboard, AdaptableKeyboard,
InsertPhoneNumberNavigator {

    // MARK: - Properties

    enum ScreenType {
        case forgotPassword
        case register
        case parentPhoneNumber
    }

    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var insertPhoneNumberLabel: UILabel = {
        let label = UILabel(
            text: L10n.insertPhoneNumber,
            font: .medium14,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 1
        )

        return label
    }()
    lazy var phoneNumberTextField: TextFieldView = {
        let view = TextFieldView(
            title: nil,
            placeholder: nil,
            keyboardType: .phonePad
        )
        view.setValidations(
            validations: [
                (.phonenumber, message: "Nomor HP. Tidak valid"),
                (.minLength(
                    length: SettingValidation.PhoneNumber.minLength),
                 message: "min. \(SettingValidation.PhoneNumber.minLength)"),
                (.maxLength(
                    length: SettingValidation.PhoneNumber.maxLength),
                 message: "max. \(SettingValidation.PhoneNumber.maxLength)"),
                (.required, message: "")
                ].map(TextFieldView.Validation.init)
        )
        view.textFieldDelegate = self

        return view
    }()
    lazy var nextButton: Button = {
        let button = Button(
            title: "",
            type: .circularYellow
        )
        button.isStyleEnabled = false
        button.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var skipPhoneNumberButton: UIButton = {
        let button = UIButton()
        button.setTitle(L10n.skipDontHavePhone, for: .normal)
        button.setTitleColor(Asset.Color.blue.color, for: .normal)
        button.titleLabel?.font = .medium14
        button.contentHorizontalAlignment = .left
        button.setImage(Asset.Authentication.arrowBlue.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Asset.Color.blue.color
        button.titleLabel?.numberOfLines = 0
        button.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: UIStyle.Inset.inset8,
            bottom: 0,
            right: 0
        )
        button.addTarget(self, action: #selector(skipPhoneTapped), for: .touchUpInside)

        return button
    }()

    var isKidSocialMedia: Bool = false
    var screenType: ScreenType = .forgotPassword
    var userType: UserType = UserType(rawValue: Storage.User.getUserType()) ?? .kid
    var otpType: OTPType = .forgotPassword

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        enableDismissableKeyboard()
        enableAdaptableKeyboard()
    }

    override func setupViews() {
        super.setupViews()

        switch userType {
        case .kid:
            skipPhoneNumberButton.isHidden = false
            descriptionLabel.text = L10n.insertPhoneKidDescription
            switch screenType {
            case .forgotPassword:
                titleLabel.text = L10n.forgotPassword
                descriptionLabel.text = L10n.forgotPassDescKid
                insertPhoneNumberLabel.text = L10n.insertPhoneNumberKid
                skipPhoneNumberButton.isHidden = false
            case .register:
                titleLabel.text = L10n.phoneNumber
            case .parentPhoneNumber:
                titleLabel.text = L10n.parentPhoneNumber
                descriptionLabel.text = L10n.insertYourParentNumberDescription
                skipPhoneNumberButton.isHidden = true

            }
        case.parent:
            skipPhoneNumberButton.isHidden = true
            descriptionLabel.text = L10n.insertPhoneParentDescription
            switch screenType {
            case .forgotPassword:
                titleLabel.text = L10n.forgotPassword
                descriptionLabel.text = L10n.forgotPassDesc
                skipPhoneNumberButton.isHidden = true
            case .register:
                titleLabel.text = L10n.phoneNumber
            case .parentPhoneNumber:
                titleLabel.text = L10n.parentPhoneNumber
                descriptionLabel.text = L10n.insertYourParentNumberDescription
                skipPhoneNumberButton.isHidden = true
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.updateButtonState()
    }

    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)

        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }

    deinit {
        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }

    // MARK: - Helpers
    override func updateButtonState() {
        let textFields = [phoneNumberTextField]
        let isEmpty = !textFields.filter({ $0.isEmpty() }).isEmpty
        let isValid = textFields.filter({ $0.hasError() }).isEmpty
        nextButton.isStyleEnabled = isValid && !isEmpty
    }

    private func requestOTP(phone: String, otpType: OTPType) {
        hideKeyboard()
        nextButton.isStyleEnabled = false
        let screenType = self.screenType
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.requestOTP(phone: phone, type: otpType.rawValue)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            switch screenType {
            case .register:
                UserDataManager.shared.setPhone(phone: phone)
                self.goToRegisterOTP(phone: phone, otpType: otpType, isKidSocialMedia: self.isKidSocialMedia)
                self.registerGetOTPEvenetTracking()
            case .forgotPassword:
                switch self.userType {
                case .parent:
                    self.goToResetPasswordOTP(phone: phone, otpType: .forgotPassword)
                case .kid:
                    self.goToResetPasswordKid(phone: phone, otpType: .forgotPassword)
                }
                self.forgotPasswordGetOTPEventTracking()
            case .parentPhoneNumber:
                self.goToRegisterOTP(phone: phone, otpType: otpType, isKidSocialMedia: self.isKidSocialMedia)
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func checkParentNumber(phone: String) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.checkParentPhoneNumber(parentPhone: phone)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            if data.count == 1 {
                if self.isKidSocialMedia {
                    let phone = UserDataManager.shared.getData()?.phone
                    self.registerAsKidSocialMedia(userId: data[0].id, phone: phone)
                } else {
                    let phone = UserDataManager.shared.getData()?.phone
                    self.registerAsKid(userId: data[0].id, phone: phone)
                }
            } else {
                if self.isKidSocialMedia {
                    self.goToKidAccountProfileWithSocialMedia(kidProfileData: data)
                } else {
                    self.goToKidAccountProfileSetup(kidProfileData: data)
                }
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func checkKidNumber(phone: String) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.checkKidPhoneNumber(kidPhone: phone)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            UserDataManager.shared.setPhone(phone: phone)
            self.goToRegisterOTP(
                phone: phone,
                otpType: .register,
                isKidSocialMedia: self.isKidSocialMedia
            )
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func registerAsKid(userId: String, phone: String? = nil) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.registerAsKid(userId: userId, phone: phone)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.setKidUser(user: data)
            if Storage.EventTracking.getFirstKidRegistered() == nil {
                Storage.EventTracking.saveFirstKidRegistered(date: Date())
            }
            self.kidSuccessRegistrationEventTracking()
            self.goToSelectInterest(kidName: data.firstName, kidId: data.id)
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func registerAsKidSocialMedia(userId: String, phone: String? = nil) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.registerAsKidBySocialMedia(userId: userId, phone: phone)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.setKidUser(user: data)
            if Storage.EventTracking.getFirstKidRegistered() == nil {
                Storage.EventTracking.saveFirstKidRegistered(date: Date())
            }
            self.kidSuccessRegistrationEventTracking()
            self.goToSelectInterest(kidName: data.firstName, kidId: data.id)
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
}

// MARK: - Textfield View Delegate
extension Self: TextFieldDelegate {
    func textField(didValidate textField: TextFieldView) {
        self.updateButtonState()
    }
    func textField(shouldBeginEditing textField: TextFieldView) -> Bool {
        if screenType == .forgotPassword {
            forgotPasswordFillPhoneNumberEventTracking()
        }
        if screenType == .register {
            registerFillPhoneNumberEventTracking()
        }
        if screenType == .parentPhoneNumber {
            kidRegistrationFillParentPhoneNumberEventTracking()
        }
        return true
    }
}

// MARK: - Actions
extension Self {

    @objc private func nextButtonTapped() {
        hideKeyboard()
        switch screenType {
        case .forgotPassword:
            let phone = phoneNumberTextField.text?.trim ?? ""
            requestOTP(phone: phone, otpType: .forgotPassword)
            self.forgotPasswordSubmitPhoneEventTracking()
        case .register:
            let phone = phoneNumberTextField.text?.trim ?? ""
            switch otpType {
            case .register:
                switch userType {
                case .kid:
                    self.checkKidNumber(phone: phone)
                case .parent:
                    self.requestOTP(phone: phone, otpType: .register)
                }
                self.registerSubmitPhoneNumberEventTracking()
            case .updatePhone:
                self.requestOTP(phone: phone, otpType: .updatePhone)
                self.registerSubmitPhoneNumberEventTracking()
            default:
                break
            }
        case .parentPhoneNumber:
            let phone = phoneNumberTextField.text?.trim ?? ""
            checkParentNumber(phone: phone)
        }
    }

    @objc private func skipPhoneTapped() {
        switch screenType {
        case .forgotPassword:
            self.goToKidNoPhoneNumber()
        case .register:
            self.goToParentPhoneNumber(isKidSocialMedia: isKidSocialMedia)
        default:
            break
        }
    }
}
