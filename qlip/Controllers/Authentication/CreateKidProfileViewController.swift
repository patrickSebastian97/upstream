//
//  CreateKidProfileViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 22/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit
import PromiseKit

private typealias `Self` = CreateKidProfileViewController

class CreateKidProfileViewController: BaseViewController, AdaptableKeyboard, DismissableKeyboard,
                                      CreateKidProfileNavigator {

    // MARK: - Configs

    override var backImage: UIImage? { return nil }

    // MARK: - Properties

    lazy var backButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(Asset.General.backButton.image.withRenderingMode(.alwaysOriginal), for: .normal)
        view.contentEdgeInsets = backInset ?? .zero
        view.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)

        return view
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: L10n.createKidProfileTitle,
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: L10n.createKidProfileDescription,
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var kidNameTextField: TextFieldView = {
        let textfield = TextFieldView(
            title: L10n.kidName,
            placeholder: L10n.insertOneOfYourKid,
            keyboardType: .normal
        )
        textfield.setValidations(
            validations: [
                (.alphabet, message: "Nama tidak valid"),
                (.required, message: "")
                ].map(TextFieldView.Validation.init)
        )
        textfield.textFieldDelegate = self

        return textfield
    }()
    lazy var kidDobTextField: TextFieldView = {
        let textfield = TextFieldView(
            title: L10n.kidDob,
            placeholder: L10n.chooseDate,
            keyboardType: .normal
        )
        textfield.leftImage = Asset.Authentication.calendar.image
        textfield.textFieldDelegate = self

        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        let doneButton = UIBarButtonItem(
            title: L10n.save,
            style: .done,
            target: self,
            action: #selector(doneDatePickerTapped))
        let spaceButton = UIBarButtonItem(
            barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
            target: nil,
            action: nil)

        toolbar.setItems([spaceButton, doneButton], animated: true)

        textfield.datePicker = datePicker
        textfield.toolbar = toolbar
        return textfield
    }()
    lazy var bottomButtonView: BottomButtonView = {
        let view = BottomButtonView(
            title: L10n.createKidProfile,
            type: .yellow,
            backgroundColor: .clear
        )
        view.actionButton.isStyleEnabled = false
        view.actionButton.addTarget(self, action: #selector(bottomButtonTapped), for: .touchUpInside)

        return view
    }()
    lazy var infoImageView: UIImageView = {
        let image = UIImageView(image: Asset.General.info.image.withRenderingMode(.alwaysOriginal))

        return image
    }()
    lazy var infoLabel: UILabel = {
        let label = UILabel(
            text: L10n.infoCreateKid,
            font: .book12,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    private var kidDob: String = ""
    private var kidId: String = ""
    private var kidName: String = ""
    var isSocialMediaSignIn: Bool = false
    var isParentDontHaveKid: Bool = false

    // MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        enableDismissableKeyboard()
        enableAdaptableKeyboard()
    }

    override func setupViews() {
        super.setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.isHidden = false
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton)]
    }

    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)

        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }

    deinit {
        disableAdaptableKeyboard()
        disableDismissableKeyboard()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    // MARK: - Helpers

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }

    override func updateButtonState() {
        let textFields = [kidNameTextField, kidDobTextField]
        let isEmpty = !textFields.filter({ $0.isEmpty() }).isEmpty
        let isValid = textFields.filter({ $0.hasError() }).isEmpty
        bottomButtonView.actionButton.isStyleEnabled = isValid && !isEmpty
    }

    private func validateDate() {
        let dob = kidDobTextField.datePicker?.date.isoDate ?? ""
        let dobComponents = dob.components(separatedBy: "/")
        let day = Int(dobComponents[0])
        let month = Int(dobComponents[1])
        let year =  Int(dobComponents[2])
        let calendar = Calendar.current
        let startDate = calendar.date(from: DateComponents(year: year, month: month, day: day))!
        self.kidDob = "\(dobComponents[2])-\(dobComponents[1])-\(dobComponents[0])"
        let endDate = Date()
        let dateComponents = calendar.dateComponents([.year, .month, .day],
                                                     from: startDate, to: endDate)
        guard let age = dateComponents.year else { return }
        if age < KidAge.minimum || age > KidAge.maximum {
            kidDobTextField.text = dob
            kidDobTextField.errorMessage = L10n.kidAgeValidation(KidAge.minimum, KidAge.maximum)
            kidDobTextField.borderColor = Asset.Color.red.color.cgColor
            kidDobTextField.backgroundTextColor = Asset.Color.redAlpha3.color
        } else {
            kidDobTextField.text = dob
        }
    }

    private func splitFullname(fullName: String) -> [String] {
        var splitedNames: [String] = []
        var components = fullName.components(separatedBy: " ")
        if !components.isEmpty {
            let firstName = components.removeFirst()
            let lastName = components.joined(separator: " ")
            splitedNames.append(firstName)
            splitedNames.append(lastName)
        }
        return splitedNames
    }

    private func createParentProfile() {
        let parentData = UserDataManager.shared.getData()
        let token = Firebase.shared.getFCMToken() ?? ""
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Authentication.registerParent(parentData: parentData, token: token)
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.setParentUser(user: data)
            self.createKidProfile()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    private func createKidProfile() {
        let fullNames = splitFullname(fullName: kidNameTextField.text?.trim ?? "")
        let firstName = fullNames.first ?? ""
        let lastName = fullNames.last ?? ""
        let kidDob = self.kidDob
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Parent.createKid(
                firstName: firstName,
                lastName: lastName,
                dob: kidDob
            )
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.kidName = data.firstName
            self.kidId = data.id
            if self.isSocialMediaSignIn || self.isParentDontHaveKid {
                guard let parent = Storage.ParentData.getParent() else { return }
                self.setParentUser(user: parent)
                Storage.ParentData.delete()
                if Storage.EventTracking.getFirstParentRegistered() == nil {
                    Storage.EventTracking.saveFirstParentRegistered(date: Date())
                }
                if let dob = data.dob {
                    Storage.EventTracking.saveKidDob(dob: dob)
                }
            }
            self.parentSuccessRegistrationEventTracking()
            self.parentAddKidProfileOpenOnboarding2EventTracking()
            self.goToSelectInterest(kidName: self.kidName, kidId: self.kidId)
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
}

// MARK: - Text Field View Delegate
extension Self: TextFieldDelegate {
    func textField(didValidate textField: TextFieldView) {
        if textField == kidDobTextField && !kidDobTextField.isEmpty() {
            validateDate()
        }
        if textField == kidNameTextField {
            let hasWhiteSpace = kidNameTextField.text?.hasPrefix(" ") ?? false
            if hasWhiteSpace {
                kidNameTextField.errorMessage = "hanya dapat ditulis dengan alfabet"
                kidNameTextField.borderColor = Asset.Color.red.color.cgColor
                kidNameTextField.backgroundTextColor = Asset.Color.redAlpha3.color
            }
        }
        self.updateButtonState()
    }
    func textField(shouldBeginEditing textField: TextFieldView) -> Bool {
        if textField == kidNameTextField {
            self.parentAddKidProfileFillKidNameEventTracking()
        }
        if textField == kidDobTextField {
            self.parentAddKidProfileFillBirthDateEventTracking()
        }
        return true
    }
}

// MARK: - Actions

extension Self {

    @objc private func doneDatePickerTapped() {
        let dob = kidDobTextField.datePicker?.date.isoDate ?? ""
        kidDobTextField.text = dob
        self.view.endEditing(true)
    }

    @objc private func bottomButtonTapped() {
        if isSocialMediaSignIn || isParentDontHaveKid {
            createKidProfile()
        } else {
            createParentProfile()
        }
        self.parentClickCreateKidAccountEventTracking()
    }

    @objc private func backButtonTapped() {
        showGeneralInfoPopup(
            image: nil,
            title: "Apakah anda ingin keluar ?",
            message: "data yang dimasukan akan hilang",
            actionButtonTitle: "Tidak",
            skipButtonTitle: "Ya, saya ingin keluar",
            clickAnywhereToDismiss: true,
            action: { [weak self] in
                guard let self = self else { return }
                self.dismiss(animated: true, completion: nil)
            }, skip: { [weak self] in
                guard let self = self else { return }
                if self.isSocialMediaSignIn {
                    self.popToViewControllerOfType(
                        controller: LoginAndRegisterViewController.self,
                        animated: true
                    )
                } else if self.isParentDontHaveKid {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.popToViewControllerOfType(
                        controller: InsertPhoneNumberViewController.self,
                        animated: true
                    )
                }
        })
    }
}
