//
//  InsertPhoneNumberLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 16/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension InsertPhoneNumberViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        let view = scrollView.contentView

        let buttonWrapper = UIView()
        buttonWrapper.withSize(.init(
            width: UIStyle.Button.height,
            height: 80
        ))
        buttonWrapper.addSubview(nextButton)
        nextButton.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().inset(UIStyle.Inset.inset8)
        }

        view.vstack(
            view.hstack(titleLabel),
            view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(insertPhoneNumberLabel).padTop(UIStyle.Inset.inset24),
            view.hstack(phoneNumberTextField,
                        buttonWrapper,
                        spacing: UIStyle.Inset.inset8
            ),
            view.hstack(skipPhoneNumberButton).padTop(UIStyle.Inset.inset32)
        ).withMargins(
            .init(
                top: UIStyle.Inset.defaultTop,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )
    }
}
