//
//  WelcomePageLayout.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 14/12/20.
//  Copyright © 2020 Qlip. All rights reserved.
//

import UIKit

extension WelcomePageViewController {

    // MARK: - Initialize UI

    override func initializeUI() {
        super.initializeUI()

        let view = scrollView.contentView

        let imageWrapper = UIView()
        imageWrapper.addSubview(imageView)
        imageView.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview()
            maker.top.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        self.view.addSubViews(termPrivacyLabel)
        termPrivacyLabel.snp.makeConstraints { (maker) in
            maker.leading.equalToSuperview().inset(UIStyle.Inset.left)
            maker.trailing.equalToSuperview().inset(UIStyle.Inset.right)
            maker.bottom.equalTo(termPrivacyLabel.superview!.safeAreaLayoutGuide.snp.bottom)
                .inset(UIStyle.Inset.inset40)
        }

        view.vstack(
            imageWrapper,
            view.hstack(titleLabel).padTop(UIStyle.Inset.inset32),
            view.hstack(descriptionLabel).padTop(UIStyle.Inset.inset16),
            view.hstack(loginButton,
                        registerButton,
                        spacing: UIStyle.Inset.inset16,
                        distribution: .fillEqually
                        ).padTop(UIStyle.Inset.inset24),
            view.hstack(guestButton).padTop(UIStyle.Inset.inset24)
        ).withMargins(
            .init(
                top: UIStyle.Inset.defaultTop,
                left: UIStyle.Inset.left,
                bottom: UIStyle.Inset.bottom,
                right: UIStyle.Inset.right
            )
        )
    }
}
