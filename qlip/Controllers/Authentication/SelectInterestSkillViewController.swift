//
//  SelectInterestSkillViewController.swift
//  qlip
//
//  Created by Georgius Yoga Dewantama on 02/01/21.
//  Copyright © 2021 Qlip. All rights reserved.
//

import UIKit
import PromiseKit

private typealias `Self` = SelectInterestSkillViewController

class SelectInterestSkillViewController: BaseViewController, AdaptableKeyboard, DismissableKeyboard,
                                         SelectInterestSkillNavigator {

    override var shouldAttachScrollView: Bool { return true }

    // MARK: - Properties

    lazy var skipButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(L10n.skip, for: .normal)
        button.titleLabel?.font = .medium14
        button.tintColor = Asset.Color.blackAlpha30.color
        button.titleLabel?.textAlignment = .right
        button.addTarget(self, action: #selector(skipButtonTapped), for: .touchUpInside)

        return button
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .heavy24,
            textColor: Asset.Color.black.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var descriptionLabel: UILabel = {
        let label = UILabel(
            text: "",
            font: .book16,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 0
        )

        return label
    }()
    lazy var selectInterestSkillListController: SelectInterestSkillListController = {
        let controller = SelectInterestSkillListController()
        controller.selectInterestSkillDelegate = self

        return controller
    }()
    lazy var otherTextField: TextFieldView = {
        let textfield = TextFieldView(title: nil, placeholder: "", keyboardType: .normal)
        textfield.textFieldDelegate = self
        textfield.setValidations(
            validations: [
                (.separetedByComma, message: "hanya huruf abjad")
                ].map(TextFieldView.Validation.init)
        )

        return textfield
    }()
    lazy var useCommaLabel: UILabel = {
        let label = UILabel(
            text: L10n.useComaDescription,
            font: .book12,
            textColor: Asset.Color.blackAlpha70.color,
            textAlignment: .left,
            numberOfLines: 2
        )

        return label
    }()
    lazy var bottomButtonView: BottomButtonView = {
        let button = BottomButtonView(
            title: L10n.continue,
            type: .yellow
        )
        button.actionButton.isStyleEnabled = false
        button.actionButton.addTarget(self, action: #selector(bottomButtonTapped), for: .touchUpInside)

        return button
    }()
    private var interestSkillList: [InterestSkillTagModel] = []
    private var selectedTags: [String] = []
    var onboardingType: OnboardingType = .selectInterest
    var kidName: String = ""
    var kidId: String = ""
    var userType: UserType = UserType(rawValue: Storage.User.getUserType()) ?? .kid

    // MARK: - Life Cycles

    override func viewDidLoad() {
        super.viewDidLoad()

        enableDismissableKeyboard()
        enableAdaptableKeyboard()
        switch onboardingType {
        case .selectInterest:
            fetchInterest()
        case .selectSkill:
            fetchSkill()
        }
    }

    override func setupViews() {
        super.setupViews()

        switch userType {
        case .kid:
            switch onboardingType {
            case .selectInterest:
                titleLabel.text = L10n.selectInterestFromKidTitle(kidName)
                descriptionLabel.text = L10n.selectInterestFromParentDesc
                otherTextField.placeholder = L10n.otherActivity
            case .selectSkill:
                titleLabel.text = L10n.selectSkillFromKidTitle
                descriptionLabel.text = L10n.selectSkillFromKidDesc
                otherTextField.placeholder = L10n.otherSkill
            }
        case .parent:
            switch onboardingType {
            case .selectInterest:
                titleLabel.text = L10n.selectInterestFromParentTitle(kidName)
                descriptionLabel.text = L10n.selectInterestFromParentDesc
                otherTextField.placeholder = L10n.otherActivity
            case .selectSkill:
                titleLabel.text = L10n.selectSkillFromParentTitle(kidName)
                descriptionLabel.text = L10n.selectSkillFromParentDesc
                otherTextField.placeholder = L10n.otherSkill
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.updateButtonState()
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: skipButton)]
    }

    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)

        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }

    deinit {
        disableAdaptableKeyboard()
        disableDismissableKeyboard()
    }

    // MARK: - Helpers

    private func fetchInterest() {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Interest.getAll()
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.interestSkillList = data
            self.selectInterestSkillListController.items = self.interestSkillList.map({
                InterestSkillTagViewModel(item: $0)
            })
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func fetchSkill() {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Skill.getAll()
        }.compactMap {
            return $0.dataModel()
        }.done { [weak self] data in
            guard let self = self else { return }
            self.interestSkillList = data
            self.selectInterestSkillListController.items = self.interestSkillList.map({
                InterestSkillTagViewModel(item: $0)
            })
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func assignInterest(interests: [String], customInterest: String? = nil) {
        let kidId = self.kidId
        let kidName = self.kidName
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Interest.assignInterest(
                kidId: kidId,
                interests: interests,
                customInterest: customInterest
            )
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            self.goToSelectSkill(kidName: kidName, kidId: kidId)
            switch self.userType {
            case.kid:
                self.kidAddProfileOpenOnboarding2EventTracking()
            case .parent:
                self.parentAddKidProfileOpenOnboarding3EventTracking()
            }
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func assignSkill(skills: [String], customSkill: String? = nil) {
        let kidId = self.kidId
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Skill.assignSkill(kidId: kidId, skills: skills, customSkill: customSkill)
        }.compactMap {
            return $0
        }.done { [weak self] _ in
            guard let self = self else { return }
            self.gotoHomeScreen()
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func addInterest(interest: String) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Interest.addInterest(name: interest)
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }
    private func addSkill(skill: String) {
        firstly {
            self.view.showLoading()
        }.then { _ in
            APIService.Skill.addSkill(name: skill)
        }.ensure { [weak self] in
            guard let self = self else { return }
            self.view.hideLoading()
        }.catch { [weak self] error in
            guard let self = self else { return }
            self.handleError(error: error)
        }
    }

    override func updateButtonState() {
        let textFields = [otherTextField]
        let isValid = textFields.filter({ $0.hasError() }).isEmpty
        let min = SettingValidation.InterestSkillTag.minimum
        let max = SettingValidation.InterestSkillTag.maximum
        let isSelectedTagValid = selectedTags.count <= max && selectedTags.count >= min ? true : false
        bottomButtonView.actionButton.isStyleEnabled = isValid && isSelectedTagValid
    }
}

// MARK: - Textfield View Delegate
extension Self: TextFieldDelegate {
    func textField(didValidate textField: TextFieldView) {
        updateButtonState()
    }
}

// MARK: - Select Intereset Skill List Controller Delegate
extension Self: SelectInterestSkillListControllerDelegate {
    func selectInterestSKillListController(
        _ selectInteresSkillListController: SelectInterestSkillListController,
        didSelectAt index: Int
    ) {
        guard let id = self.interestSkillList[index].id else { return }
        if let index = selectedTags.firstIndex(of: id) {
            selectedTags.remove(at: index)
        } else {
            selectedTags.append(id)
        }
        self.updateButtonState()
    }
}

// MARK: - Actions

extension Self {
    @objc private func skipButtonTapped() {
        let message = userType == .kid ? L10n.kidWillSkipPersonalization : L10n.parentWillSkipPersonalization

        showGeneralInfoPopup(
            image: nil,
            title: L10n.areYouSureToSkip,
            message: message,
            actionButtonTitle: L10n.back,
            skipButtonTitle: L10n.skipDontNeedYet,
            clickAnywhereToDismiss: true,
            action: { [weak self] in
                guard let self = self else { return }
                self.dismiss(animated: true, completion: nil)
            },
            skip: { [weak self] in
                guard let self = self else { return }
                switch self.onboardingType {
                case .selectInterest:
                    self.goToSelectSkill(kidName: self.kidName, kidId: self.kidId)
                case .selectSkill:
                    self.gotoHomeScreen()
                }
            })
    }
    @objc private func bottomButtonTapped() {
        let customTags = otherTextField.text?.trim
        switch onboardingType {
        case .selectInterest:
            assignInterest(interests: selectedTags, customInterest: customTags)
            self.kidProfileSubmitActivityEventTracking(activity: selectedTags)
        case .selectSkill:
            assignSkill(skills: selectedTags, customSkill: customTags)
            self.kidProfileSubmitSkillEventTracking(skill: selectedTags)
        }
    }
}
